# - Try to find Catch
# Once done this will define
#  CATCH_FOUND - System has Catch
#  CATCH_INCLUDE_DIRS - The Catch include directories
#  CATCH_SOURCE_FILE - The single source file that has a #define to create a main

find_path(  CATCH_INCLUDE_DIRS
			Catch/Catch.hpp
			PATHS
			${CMAKE_CURRENT_SOURCE_DIR}/../lib/Catch
			${CMAKE_CURRENT_SOURCE_DIR}/lib/Catch
			PATH_SUFFIXES include)

find_file(  CATCH_SOURCE_FILE
			Catch/Catch.cpp
			PATHS
			${CMAKE_CURRENT_SOURCE_DIR}/../lib/Catch
			${CMAKE_CURRENT_SOURCE_DIR}/lib/Catch
			PATH_SUFFIXES src)

# handle the QUIETLY and REQUIRED arguments and set STB_IMAGE_FOUND to TRUE
# if all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Catch DEFAULT_MSG CATCH_INCLUDE_DIRS CATCH_SOURCE_FILE)
mark_as_advanced(CATCH_INCLUDE_DIRS)
mark_as_advanced(CATCH_SOURCE_FILE)