# Copyright 2018 Thomas Cassimon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# - Try to find RapidXML
# Once done this will define
#  RAPIDXML_FOUND - System has RapidXML
#  RapidXML_INCLUDE_DIR - The RapidXML include directories

find_path(  RapidXML_INCLUDE_DIRS
			rapidxml/rapidxml.hpp
			PATHS
			"${CMAKE_CURRENT_SOURCE_DIR}/lib/rapidxml"
			"${CMAKE_CURRENT_SOURCE_DIR}/../lib/rapidxml"
			PATH_SUFFIXES include)

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set RAPIDXML_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(RapidXML DEFAULT_MSG RapidXML_INCLUDE_DIRS)
mark_as_advanced(RapidXML_INCLUDE_DIRS)
