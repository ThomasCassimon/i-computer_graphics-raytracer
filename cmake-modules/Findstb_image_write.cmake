# - Try to find stb_image
# Once done this will define
#  STB_IMAGE_WRITE_FOUND - System has stb_image
#  STB_IMAGE_WRITE_SOURCE_FILE - The .cpp file used to make sure stb_image Properly compiles
#  STB_IMAGE_WRITE_INCLUDE_DIR - The stb_image include directories

find_path(  STB_IMAGE_WRITE_INCLUDE_DIR
			stb_image_write/stb_image_write.h
			PATHS
			"${CMAKE_CURRENT_SOURCE_DIR}/lib/stb_image_write"
			"${CMAKE_CURRENT_SOURCE_DIR}/../lib/stb_image_write"
			PATH_SUFFIXES include)

find_file(  STB_IMAGE_WRITE_SOURCE_FILE
			stb_image_write/stb_image_write.c
			PATHS
			"${CMAKE_CURRENT_SOURCE_DIR}/lib/stb_image_write"
			"${CMAKE_CURRENT_SOURCE_DIR}/../lib/stb_image_write"
			PATH_SUFFIXES src)

# handle the QUIETLY and REQUIRED arguments and set STB_IMAGE_WRITE_FOUND to TRUE
# if all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(stb_image_write DEFAULT_MSG STB_IMAGE_WRITE_INCLUDE_DIR STB_IMAGE_WRITE_SOURCE_FILE)
mark_as_advanced(STB_IMAGE_WRITE_INCLUDE_DIR)
mark_as_advanced(STB_IMAGE_WRITE_SOURCE_FILE)