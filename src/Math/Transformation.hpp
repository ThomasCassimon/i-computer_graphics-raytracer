/*
 * @author thomas
 * @date   01/11/18
 * @file   Transformation.hpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#ifndef RAY_TRACER_TRANSFORMATION_HPP
#define RAY_TRACER_TRANSFORMATION_HPP

#include "mat4.hpp"

namespace RayTracer::Math
{
	class Transformation
	{
		private:
			Math::mat4 forwardTransform;
			Math::mat4 inverseTransform;

		public:
			Transformation() noexcept;

			Transformation(const Math::mat4& forward, const Math::mat4& inverse) noexcept;

			Math::vec4 forward (const Math::vec4& vec) const noexcept;

			Math::vec4 inverse (const Math::vec4& vec) const noexcept;

			friend Transformation operator* (const Transformation& lhs, const Transformation& rhs) noexcept;

			Transformation& operator*= (const Transformation& other) noexcept;

			friend std::ostream& operator<< (std::ostream& stream, const Transformation& transform);
	};
}

#endif //RAY_TRACER_TRANSFORMATION_HPP
