/*
 * @author thomas
 * @date   19/10/18
 * @file   TransformFactory.hpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#ifndef RAY_TRACER_TRANSFORMFACTORY_HPP
#define RAY_TRACER_TRANSFORMFACTORY_HPP

#include "Math/vec3.hpp"
#include "Math/mat4.hpp"
#include "Math/Transformation.hpp"

namespace RayTracer::Math
{
	class TransformFactory
	{
		private:
			constexpr static std::string_view TYPE_KEY = "type";
			constexpr static std::string_view VALUE_KEY = "val";
			constexpr static std::string_view ANGLE_KEY = "angle";
			constexpr static std::string_view AXIS_KEY = "axis";
			constexpr static std::string_view TRANSLATION_TYPE = "trans";
			constexpr static std::string_view SCALE_TYPE = "scale";
			constexpr static std::string_view ROTATE_TYPE = "rot";

			/**
			 * Translation is given as an x,y,z vector, each component representing the translation in its own dimension
			 * @param delta
			 * @return
			 */
			Math::mat4 generateTranslation (const Math::vec3& delta) const noexcept;

			/**
			 * Scale is given as an x,y,z vector, each component representing the translation in its own dimension
			 * @param delta
			 * @return
			 */
			Math::mat4 generateScale (const Math::vec3& scale) const noexcept;

			/**
			 * Rotation is given as a quaternion, x,y,z make up the rotation axis, w is the rotation angle in radians
			 * @param rotate
			 * @return
			 */
			Math::mat4 generateRotation (Math::vec3 axis, float angle) const noexcept;

		public:
			TransformFactory() noexcept = default;

			/**
			 * Translation is given as an x,y,z vector, each component representing the translation in its own dimension
			 * @param delta
			 * @return A pair of matrices, the forward transform in the first element, the inverse transform in the second
			 */
			Math::Transformation translate (const Math::vec3& delta) const noexcept;

			/**
			 * Scale is given as an x,y,z vector, each component representing the translation in its own dimension
			 * @param delta
			 * @return A pair of matrices, the forward transform in the first element, the inverse transform in the second
			 */
			Math::Transformation scale (const Math::vec3& scale) const noexcept;

			/**
			 * Rotation is given as an axis and an angle.
			 * @param rotate
			 * @return A pair of matrices, the forward transform in the first element, the inverse transform in the second
			 */
			Math::Transformation rotate (Math::vec3 axis, float angle) const noexcept;

			Math::Transformation parse (const Json::Value& transformJson);
	};
}

#endif //RAY_TRACER_TRANSFORMFACTORY_HPP
