/*
 * @author thomas
 * @date   23/10/18
 * @file   vec2.hpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#ifndef RAY_TRACER_VEC2_HPP
#define RAY_TRACER_VEC2_HPP

#ifdef USE_SIMD
	#include "Math/unrolled/vec2.hpp"
	#include "Math/simd/vector.hpp"
#else
	#include "Math/unrolled/vec2.hpp"
#endif

namespace RayTracer::Math
{
	#ifdef USE_SIMD
		using vec2 = simd_vec2;
		//todo: Write SIMD version
		using ivec2 = vector2<int>;
	#else
		using vec2 = vector2<float>;
		using ivec2 = vector2<int>;
	#endif


}

#endif //RAY_TRACER_VEC2_HPP
