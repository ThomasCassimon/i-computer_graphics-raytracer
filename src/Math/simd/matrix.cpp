/*
 * @author  thomas
 * @date    25/11/18
 * Copyright (c) 2018 thomas
 */

#include "matrix.hpp"

namespace RayTracer::Math
{
	simd_matrix::simd_matrix(__m256 ymm0, __m256 ymm1) noexcept: ymm0(ymm0), ymm1(ymm1)
	{
	}

	simd_matrix::simd_matrix() noexcept: ymm0(_mm256_set1_ps(0.0f)), ymm1(_mm256_set1_ps(0.0f))
	{
	}

	simd_matrix::simd_matrix(const simd_matrix& other) noexcept: ymm0(other.ymm0), ymm1(other.ymm1)
	{
	}

	simd_matrix::simd_matrix(simd_matrix&& other) noexcept: ymm0(other.ymm0), ymm1(other.ymm1)
	{
	}

	simd_matrix& simd_matrix::operator=(simd_matrix other) noexcept
	{
		this->ymm0 = other.ymm0;
		this->ymm1 = other.ymm1;
		return *this;
	}

	const float& simd_matrix::operator()(int row, int col) const noexcept
	{
		return this->data[this->flattenIndex(row, col)];
	}

	float& simd_matrix::operator()(int row, int col) noexcept
	{
		return this->data[this->flattenIndex(row, col)];
	}

	std::ostream& operator<<(std::ostream& stream, const simd_matrix& simd_matrix)
	{
		stream << "[";

		for (int i = 0; i < 4; i++)
		{
			if (i != 0)
			{
				stream << " ";
			}

			stream << "[";

			for (int j = 0; j < 4; j++)
			{
				stream << std::fixed << std::setprecision(simd_matrix::PRINT_PRECISION) << std::setfill(' ') << simd_matrix(i,j);

				if ((j + 1) !=  4)
				{
					stream << ", ";
				}
			}

			stream << "]";

			if ((i + 1) == 4)
			{
				stream << "]";
			}

			stream << "\r\n";
		}

		return stream;
	}

	simd_matrix operator+(const simd_matrix& lhs, const simd_matrix& rhs) noexcept
	{
		__m256 lo = _mm256_add_ps(lhs.ymm0, rhs.ymm0);
		__m256 hi = _mm256_add_ps(lhs.ymm1, rhs.ymm1);
		return simd_matrix(lo, hi);
	}

	vec4 operator*(const simd_matrix& lhs, const vec4& rhs) noexcept
	{
																			// ymm0    = [        A                 B                 C                 D        ][        E                 F                 G                 H        ]
																			// ymm1    = [        I                 J                 K                 L        ][        M                 N                 O                 P        ]
		__m256 broadcast = _mm256_set_m128(rhs.xmm, rhs.xmm);               // Vec     = [         x                 y                 z                 w       ][         x                 y                 z                 w       ]
																			//-------------------------------------------------------------------------------------------------------------------------------------------------------------
		__m256 xy_m256 = _mm256_mul_ps(lhs.ymm0, broadcast);                // xy_m256 = [        Ax                By                Cz                Dw       ][        Ex                Fy                Gz                Hw       ]
		__m256 zw_m256 = _mm256_mul_ps(lhs.ymm1, broadcast);                // zw_m256 = [        Ix                Jy                Kz                Lw       ][        Mx                Ny                Oz                Pw       ]
																			//-------------------------------------------------------------------------------------------------------------------------------------------------------------
		__m256   acc0  = _mm256_hadd_ps(xy_m256, zw_m256);                  // acc0    = [     Ix + Jy           Kz + Lw           Ax + By           Cz + Dw     ][     Mx + Ny           Oz + Pw           Ex + Fy           Gz + Hw     ]
		__m256   acc1  = _mm256_hadd_ps(zw_m256, xy_m256);                  // acc1    = [     Ax + By           Cz + Dw           Ix + Jy           Kz + Lw     ][     Ex + Fy           Gz + Hw           Mx + Ny           Oz + Pw     ]
				 acc1  = _mm256_permute2f128_ps(acc1, acc1, 0x01);		    // acc1    = [     Ex + Fy           Gz + Hw           Mx + Ny           Oz + Pw     ][     Ax + By           Cz + Dw           Ix + Jy           Kz + Lw     ]
		__m256 merged  = _mm256_hadd_ps(acc0, acc1);                        // merged  = [Ex + Fy + Gz + Hw Mx + Ny + Oz + Pw Ix + Jy + Kz + Lw Ax + By + Cz + Dw][Ax + By + Cz + Dw Ix + Jy + Kz + Lw Mx + Ny + Oz + Pw Ex + Fy + Gz + Hw]
		__m128    vec  = _mm256_extractf128_ps(merged, 0);                  //  vec    =                                                                          [Ax + By + Cz + Dw Ix + Jy + Kz + Lw Mx + Ny + Oz + Pw Ex + Fy + Gz + Hw]
				  vec  = _mm_shuffle_ps(vec, vec, _MM_SHUFFLE(2, 1, 3, 0)); //  vec    =                                                                          [Mx + Ny + Oz + Pw Ix + Jy + Kz + Lw Ex + Fy + Gz + Hw Ax + By + Cz + Dw]

		return vec4(vec);
	}

	simd_matrix operator*(const simd_matrix& lhs, const simd_matrix& rhs) noexcept
	{
		const __m256 L_Row_01 = lhs.ymm0;
		const __m256 L_Row_23 = lhs.ymm1;

		// Holds the simd_matrix
		float rhs_data [16] = {0.0f};

		// Store the simd_matrix in row-major order
		_mm256_store_ps(&rhs_data[0], rhs.ymm1);
		_mm256_store_ps(&rhs_data[8], rhs.ymm0);

		// Retrieve the simd_matrix in column-major order
		const __m256 R_Col_01 = _mm256_set_ps(rhs_data[5], rhs_data[1], rhs_data[13],  rhs_data[9], rhs_data[4], rhs_data[0], rhs_data[12], rhs_data[8]);
		const __m256 R_Col_23 = _mm256_set_ps(rhs_data[7], rhs_data[3], rhs_data[15], rhs_data[11], rhs_data[6], rhs_data[2], rhs_data[14], rhs_data[10]);

		const __m256 R_Col_10 = _mm256_permute2f128_ps(R_Col_01, R_Col_01, 0x01);
		const __m256 R_Col_32 = _mm256_permute2f128_ps(R_Col_23, R_Col_23, 0x01);

		const __m256 Main_Diag_0 = _mm256_mul_ps(L_Row_01, R_Col_01);
		const __m256 Main_Diag_1 = _mm256_mul_ps(L_Row_23, R_Col_23);

		const __m256 Int1_0 = _mm256_mul_ps(L_Row_01, R_Col_10);
		const __m256 Int1_1 = _mm256_mul_ps(L_Row_23, R_Col_32);

		const __m256 Int2_0 = _mm256_mul_ps(L_Row_01, R_Col_23);
		const __m256 Int2_1 = _mm256_mul_ps(L_Row_23, R_Col_01);

		const __m256 Int3_0 = _mm256_mul_ps(L_Row_01, R_Col_32);
		const __m256 Int3_1 = _mm256_mul_ps(L_Row_23, R_Col_10);

		const __m256 Int4 = _mm256_hadd_ps(Main_Diag_0, Int1_0);

		const __m256 Int5 = _mm256_hadd_ps(Main_Diag_1, Int1_1);

		const __m256 Int6 = _mm256_hadd_ps(Int3_0, Int2_0);

		const __m256 Int7 = _mm256_hadd_ps(Int3_1, Int2_1);

		__m256 res_ymm0 = _mm256_hadd_ps(Int6, Int4);
		__m256 res_ymm1 = _mm256_hadd_ps(Int5, Int7);

		int test [4];
		test[0] = 0;
		test[1] = 1;
		test[2] = 2;
		test[3] = 3;

		__m128 row0 = _mm256_extractf128_ps(res_ymm0, 0);
		__m128 row1 = _mm256_extractf128_ps(res_ymm0, 1);
		__m128 row2 = _mm256_extractf128_ps(res_ymm1, 0);
		__m128 row3 = _mm256_extractf128_ps(res_ymm1, 1);

		row0 = _mm_permute_ps(row0, 0x1E);
		row1 = _mm_permute_ps(row1, 0x4B);
		row2 = _mm_permute_ps(row2, 0x4B);
		row3 = _mm_permute_ps(row3, 0x1E);

		res_ymm0 = _mm256_set_m128(row1, row0);
		res_ymm1 = _mm256_set_m128(row3, row2);

		return simd_matrix(res_ymm0, res_ymm1);
	}

	simd_matrix identity() noexcept
	{
		simd_matrix result;

		for (int i = 0; i < 4; i++)
		{
			result(i,i) = 1.0f;
		}

		return result;
	}
}