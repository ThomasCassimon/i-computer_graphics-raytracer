/*
 * @author thomas
 * @date   22/10/18
 * @file   vector.cpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#include "vector.hpp"


namespace RayTracer::Math
{
	template<>
	vector<2>::vector(float x, float y) noexcept: xmm(_mm_set_ps(0.0f, 0.0f, y, x))
	{
	}

	template<>
	vector<3>::vector(float x, float y, float z) noexcept: xmm(_mm_set_ps(0.0f, z, y, x))
	{
	}

	template<int N>
	vector<N>::vector(float x, float y, float z, float w) noexcept: xmm(_mm_set_ps(w, z, y, x))
	{
	}

	template <>
	vector<3>::vector(const vector<2>& vec, float z) noexcept: xmm(_mm_set_ps(0.0f, z, vec.y, vec.x))
	{
	}

	template <>
	vector<4>::vector(const vector<3>& vec, float w) noexcept: xmm(_mm_set_ps(w, vec.z, vec.y, vec.x))
	{
	}

	template<int N>
	vector<N>::vector(const Json::Value& jsonVector)
	{
		if (!jsonVector.isArray())
		{
			std::stringstream stream;
			stream << "JSON vector wasn't an array.\n" << jsonVector;
			throw std::runtime_error(stream.str());
		}

		if (jsonVector.size() != N)
		{
			std::stringstream stream;
			stream <<  "JSON vector had " << jsonVector.size() << " elements, expected " << N << " elements.\n" << jsonVector;
			throw std::runtime_error(stream.str());
		}

		if constexpr (N == 2)
		{
			this->x = jsonVector[0].asFloat();
			this->y = jsonVector[1].asFloat();
			this->z = 0.0f;
			this->w = 0.0f;
		}
		else if constexpr (N == 3)
		{
			this->x = jsonVector[0].asFloat();
			this->y = jsonVector[1].asFloat();
			this->z = jsonVector[2].asFloat();
			this->w = 0.0f;
		}
		else if constexpr (N == 4)
		{
			this->x = jsonVector[0].asFloat();
			this->y = jsonVector[1].asFloat();
			this->z = jsonVector[2].asFloat();
			this->w = jsonVector[3].asFloat();
		}
	}

	template<int N>
	vector<N>::vector(vector<N>&& other) noexcept: xmm(std::move(other.xmm))
	{
	}

	template<int N>
	vector<N>& vector<N>::operator=(vector<N> other) noexcept
	{
		std::swap(other.xmm, this->xmm);
		return *this;
	}

	template<int N>
	float vector<N>::operator()(int element) const
	{
		alignas(16) std::array<float, 4> data = {};
		_mm_store_ps(data.data(), this->xmm);

		if (element < N)
		{
			return data[element];
		}
		else
		{
			throw std::out_of_range("Tried to access element " + std::to_string(element) + " in a " + std::to_string(N) + "-dimensional vector.");
		}
	}

	template<int N>
	vector<N>::operator __m128() noexcept
	{
		return this->xmm;
	}

	template<int N>
	float vector<N>::length() const noexcept
	{
		__m128 square = _mm_mul_ps(this->xmm, this->xmm);
		return std::sqrt(_mm_hacc(square));
	}

	template<int N>
	vector<N> vector<N>::normalize() const noexcept
	{
		float length = this->length();
		length = 1.0f / length;
		__m128 norm = _mm_mul_ps(this->xmm, _mm_broadcast_ss(&length)); // _mm_mul_ps has a throughput of 2 Instr's / Cyle, _mm_div_ps has 0.25 Instr's / Cycle
		return vector(norm);
	}

	template<int N>
	vector<N>& vector<N>::swap(int idx1, int idx2) noexcept
	{
		switch(idx1)
		{
			case 0:
				switch(idx2)
				{
					case 1:
						this->xmm = _mm_shuffle_ps(this->xmm, this->xmm, _MM_SHUFFLE(3, 2, 0, 1));
						break;
					case 2:
						this->xmm = _mm_shuffle_ps(this->xmm, this->xmm, _MM_SHUFFLE(3, 0, 1, 2));
						break;
					case 3:
						this->xmm = _mm_shuffle_ps(this->xmm, this->xmm, _MM_SHUFFLE(0, 2, 1, 3));
						break;
					default:
						break;
				}
				break;

			case 1:
				switch(idx2)
				{
					case 0:
						this->xmm = _mm_shuffle_ps(this->xmm, this->xmm, _MM_SHUFFLE(3, 2, 0, 1));
						break;
					case 2:
						this->xmm = _mm_shuffle_ps(this->xmm, this->xmm, _MM_SHUFFLE(3, 1, 2, 0));
						break;
					case 3:
						this->xmm = _mm_shuffle_ps(this->xmm, this->xmm, _MM_SHUFFLE(1, 2, 3, 0));
						break;
					default:
						break;
				}
				break;

			case 2:
				switch(idx2)
				{
					case 0:
						this->xmm = _mm_shuffle_ps(this->xmm, this->xmm, _MM_SHUFFLE(3, 0, 1, 2));
						break;
					case 1:
						this->xmm = _mm_shuffle_ps(this->xmm, this->xmm, _MM_SHUFFLE(3, 1, 2, 0));
						break;
					case 3:
						this->xmm = _mm_shuffle_ps(this->xmm, this->xmm, _MM_SHUFFLE(2, 3, 1, 0));
						break;
					default:
						break;
				}
				break;

			case 3:
				switch(idx2)
				{
					case 0:
						this->xmm = _mm_shuffle_ps(this->xmm, this->xmm, _MM_SHUFFLE(0, 2, 1, 3));
						break;
					case 1:
						this->xmm = _mm_shuffle_ps(this->xmm, this->xmm, _MM_SHUFFLE(1, 2, 3, 0));
						break;
					case 2:
						this->xmm = _mm_shuffle_ps(this->xmm, this->xmm, _MM_SHUFFLE(2, 3, 1, 0));
						break;
					default:
						break;
				}
				break;

			default:
				break;
		}

		return *this;
	}

	template <int N>
	vector<N> operator+(const vector<N>& lhs, const vector<N>& rhs) noexcept
	{
		return vector<N>(std::move(_mm_add_ps(lhs.xmm, rhs.xmm)));
	}

	template <int N>
	vector<N>& operator+=(vector<N>& lhs, const vector<N>& rhs) noexcept
	{
		lhs.xmm = _mm_add_ps(lhs.xmm, rhs.xmm);
		return lhs;
	}

	template <int N>
	vector<N>& operator+= (vector<N>& vector, float scalar) noexcept
	{
		__m128 scalarVector = _mm_broadcast_ss(&scalar);
		vector.xmm = _mm_add_ps(vector.xmm, scalarVector);
		return vector;
	}

	template <int N>
	vector<N> operator-(const vector<N>& lhs, const vector<N>& rhs) noexcept
	{
		return vector<N>(_mm_sub_ps(lhs.xmm, rhs.xmm));
	}

	template <int N>
	vector<N> operator-(const vector<N>& vec) noexcept
	{
		constexpr static float ZERO = 0.0f;
		return vector<N>(_mm_sub_ps(_mm_broadcast_ss(&ZERO), vec.xmm));
	}

	template <int N>
	vector<N> operator*(const vector<N>& vec, float coef) noexcept
	{
		return vector<N>(_mm_mul_ps(vec.xmm, _mm_broadcast_ss(&coef)));
	}

	template <int N>
	vector<N> operator*(float coef, const vector<N>& vec) noexcept
	{
		return vector<N>(_mm_mul_ps(vec.xmm, _mm_broadcast_ss(&coef)));
	}

	template <int N>
	vector<N> operator*(const vector<N>& lhs, const vector<N>& rhs) noexcept
	{
		return vector<N>(_mm_mul_ps(lhs.xmm, rhs.xmm));
	}

	template <int N>
	vector<N>& operator*=(vector<N>& lhs, const vector<N>& rhs) noexcept
	{
		lhs =  vector<N>(_mm_mul_ps(lhs.xmm, rhs.xmm));
		return lhs;
	}

	template <int N>
	vector<N> operator/ (const vector<N>& lhs, const vector<N>& rhs) noexcept
	{
		return vector<N>(_mm_div_ps(lhs.xmm, rhs.xmm)); // _mm_mul_ps has a throughput of 2 Instr's / Cyle, _mm_div_ps has 0.25 Instr's / Cycle
	}

	template <int N>
	vector<N> operator/ (const vector<N>& vec, float coef) noexcept
	{
		return vector<N>(_mm_mul_ps(vec.xmm, _mm_set1_ps(1.0f / coef))); // _mm_mul_ps has a throughput of 2 Instr's / Cyle, _mm_div_ps has 0.25 Instr's / Cycle
	}

	template<int N>
	vector<N> operator/ (float coef, const vector<N>& vec) noexcept
	{
		return vector<N>(_mm_div_ps(_mm_set1_ps(coef), vec.xmm));
	}

	template <int N>
	vector<N>& operator/= (vector<N>& vec, float scalar) noexcept
	{
		scalar = 1.0f / scalar;
		vec.xmm = _mm_mul_ps(vec.xmm, _mm_set1_ps(scalar));       // _mm_mul_ps has a throughput of 2 Instr's / Cyle, _mm_div_ps has 0.25 Instr's / Cycle
		return vec;
	}

	template <int N>
	bool operator!= (const vector<N>& lhs, const vector<N>& rhs) noexcept
	{
		constexpr static float EPSILON = std::numeric_limits<float>::epsilon();
		__m128 diff = _mm_sub_ps(lhs.xmm, rhs.xmm);
		__m128 epsilon = _mm_broadcast_ss(&EPSILON);
		__m128 cmp = _mm_cmp_ps(diff, epsilon, 0x0C);
		return _mm_hacc(cmp) > 0.0f;
	}

	template <int N>
	bool operator== (const vector<N>& lhs, const vector<N>& rhs) noexcept
	{
		constexpr static float EPSILON = std::numeric_limits<float>::epsilon();
		__m128 diff = _mm_sub_ps(lhs.xmm, rhs.xmm);
		__m128 epsilon = _mm_broadcast_ss(&EPSILON);
		__m128 cmp = _mm_cmp_ps(diff, epsilon, 0x00);
		return _mm_hacc(cmp) > 0.0f;
	}

	template <int N>
	float dot (const vector<N>& lhs, const vector<N>& rhs) noexcept
	{
		__m128 prod = _mm_mul_ps(lhs.xmm, rhs.xmm);
		return _mm_hacc(prod);
	}

	template <int N>
	vector<N> normalize (const vector<N>& vec) noexcept
	{
		return vec.normalize();
	}

	vector<3> cross (const vector<3>& lhs, const vector<3>& rhs) noexcept
	{
		__m128 e = _mm_shuffle_ps(lhs.xmm, lhs.xmm, _MM_SHUFFLE(3, 0, 2, 1));     // [    Wl   |           Xl          |            Zl         |           Yl          ] = E
		__m128 f = _mm_shuffle_ps(rhs.xmm, rhs.xmm, _MM_SHUFFLE(3, 1, 0, 2));     // [    Wr   |           Yr          |            Xr         |           Zr          ] = F

		__m128 g = _mm_shuffle_ps(lhs.xmm, lhs.xmm, _MM_SHUFFLE(3, 1, 0, 2));     // [    Wl   |           Yl          |            Xl         |           Zl          ] = G
		__m128 h = _mm_shuffle_ps(rhs.xmm, rhs.xmm, _MM_SHUFFLE(3, 0, 2, 1));     // [    Wr   |           Xr          |            Zr         |           Yr          ] = H

		__m128 c = _mm_mul_ps(e, f);                                              // [ Wl * Wr |        Xl * Yr        |        Zl * Xr        |        Yl * Zr        ] = C = E * F
		__m128 d = _mm_mul_ps(g, h);                                              // [ Wl * Wr |        Yl * Xr        |        Xl * Zr        |        Zl * Yr        ] = D = G * H

		return vector<3>(_mm_sub_ps(c, d));                                       // [    0    | (Xl * Yr) - (Yl * Xr) | (Zl * Xr) - (Xl * Zr) | (Yl * Zr) - (Zl * Yr) ] = C - D
	}

	template <int N>
	vector<N> abs (const vector<N>& vec) noexcept
	{
		return vector<N>(_mm_abs_ps(vec.xmm));
	}

	template <int N>
	float max (const vector<N>& vec) noexcept
	{
		__m128 shuf = _mm_shuffle_ps(vec.xmm, vec.xmm, _MM_SHUFFLE(2, 3, 0, 1));
		__m128 max = _mm_max_ps(vec.xmm, shuf);
		shuf = _mm_shuffle_ps(max, max, _MM_SHUFFLE(0, 1, 2, 3));
		max = _mm_max_ps(shuf, max);
		return _mm_cvtss_f32(max);
	}

	template <int N>
	float min (const vector<N>& vec) noexcept
	{
		__m128 shuf = _mm_shuffle_ps(vec.xmm, vec.xmm, _MM_SHUFFLE(2, 3, 0, 1));
		__m128 min = _mm_min_ps(vec.xmm, shuf);
		shuf = _mm_shuffle_ps(min, min, _MM_SHUFFLE(0, 1, 2, 3));
		min = _mm_min_ps(shuf, min);
		return _mm_cvtss_f32(min);
	}

	template <int N>
	vector<N> max (const vector<N>& lhs, const vector<N>& rhs) noexcept
	{
		return vector<N>(_mm_max_ps(lhs.xmm, rhs.xmm));
	}

	template <int N>
	vector<N> min (const vector<N>& lhs, const vector<N>& rhs) noexcept
	{
		return vector<N>(_mm_min_ps(lhs.xmm, rhs.xmm));
	}

	template <int N>
	std::ostream& operator<<(std::ostream& stream, const vector<N>& vector) noexcept
	{
		stream << '(';

		for (int i = 0; i < N; i++)
		{
			stream << vector(i);

			if ((i + 1) != N)
			{
				stream << ',' << ' ';
			}
		}

		stream << ')';
		return stream;
	}

	template <int N>
	vector<N> reflect(const vector<N>& incident, const vector<N>& normal) noexcept
	{
		const __m128 two =          _mm_set1_ps(2.0f);
		const __m128 twod =         _mm_mul_ps(two, incident.xmm);
		const float nDotn =         _mm_hacc(_mm_mul_ps(normal.xmm, normal.xmm));
		const float twodDotn =      _mm_hacc(_mm_mul_ps(normal.xmm, twod));
		const __m128 fraction =     _mm_set1_ps(twodDotn / nDotn);
		const __m128 right =        _mm_mul_ps(fraction, normal.xmm);
		const __m128 reflection =   _mm_sub_ps(incident.xmm, right);

		return vector<N>(reflection);
	}

	template <int N>
	vector<N> clamp(const vector<N>& vec, float min, float max) noexcept
	{
		const __m128 xmmax = _mm_set1_ps(max);
		const __m128 xmmin = _mm_set1_ps(min);
		const __m128 minified = _mm_max_ps(xmmin, vec.xmm);
		const __m128 maxified = _mm_min_ps(xmmax, minified);
		return vector<N>(maxified);
	}

	template <int N>
	float angle (const vector<N>& a, const vector<N>& b) noexcept
	{
		const float adotb = Math::dot(a, b);
		const float alen = a.length();
		const float blen = b.length();
		const float  len = alen * blen;
		const float cosa = adotb / len;
		return std::acos(cosa);
	}

	template <int N>
	vector<N> sqrt(const vector<N>& vec) noexcept
	{
		const __m128 root = _mm_sqrt_ps(vec.xmm);
		return vector<N>(root);
	}

	// Instantiate Classes themselves
	template class vector<2>;
	template class vector<3>;
	template class vector<4>;

	// Instantiate functions
	template vector<2> operator+(const vector<2>& lhs, const vector<2>& rhs) noexcept;
	template vector<3> operator+(const vector<3>& lhs, const vector<3>& rhs) noexcept;
	template vector<4> operator+(const vector<4>& lhs, const vector<4>& rhs) noexcept;

	template vector<2>& operator+= (vector<2>& lhs, const vector<2>& rhs) noexcept;
	template vector<3>& operator+= (vector<3>& lhs, const vector<3>& rhs) noexcept;
	template vector<4>& operator+= (vector<4>& lhs, const vector<4>& rhs) noexcept;

	template vector<2>& operator+= (vector<2>& vector, float scalar) noexcept;
	template vector<3>& operator+= (vector<3>& vector, float scalar) noexcept;
	template vector<4>& operator+= (vector<4>& vector, float scalar) noexcept;

	template vector<2> operator- (const vector<2>& lhs, const vector<2>& rhs) noexcept;
	template vector<3> operator- (const vector<3>& lhs, const vector<3>& rhs) noexcept;
	template vector<4> operator- (const vector<4>& lhs, const vector<4>& rhs) noexcept;

	template vector<2> operator- (const vector<2>& vec) noexcept;
	template vector<3> operator- (const vector<3>& vec) noexcept;
	template vector<4> operator- (const vector<4>& vec) noexcept;

	template vector<2> operator* (const vector<2>& vec, float coef) noexcept;
	template vector<3> operator* (const vector<3>& vec, float coef) noexcept;
	template vector<4> operator* (const vector<4>& vec, float coef) noexcept;

	template vector<2> operator* (float coef, const vector<2>& vec) noexcept;
	template vector<3> operator* (float coef, const vector<3>& vec) noexcept;
	template vector<4> operator* (float coef, const vector<4>& vec) noexcept;

	template vector<2> operator* (const vector<2>& lhs, const vector<2>& rhs) noexcept;
	template vector<3> operator* (const vector<3>& lhs, const vector<3>& rhs) noexcept;
	template vector<4> operator* (const vector<4>& lhs, const vector<4>& rhs) noexcept;

	template vector<2>& operator*= (vector<2>& lhs, const vector<2>& rhs) noexcept;
	template vector<3>& operator*= (vector<3>& lhs, const vector<3>& rhs) noexcept;
	template vector<4>& operator*= (vector<4>& lhs, const vector<4>& rhs) noexcept;

	template vector<2> operator/ (const vector<2>& lhs, const vector<2>& rhs) noexcept;
	template vector<3> operator/ (const vector<3>& lhs, const vector<3>& rhs) noexcept;
	template vector<4> operator/ (const vector<4>& lhs, const vector<4>& rhs) noexcept;

	template vector<2> operator/ (const vector<2>& vec, float coef) noexcept;
	template vector<3> operator/ (const vector<3>& vec, float coef) noexcept;
	template vector<4> operator/ (const vector<4>& vec, float coef) noexcept;

	template vector<2> operator/ (float coef, const vector<2>& vec) noexcept;
	template vector<3> operator/ (float coef, const vector<3>& vec) noexcept;
	template vector<4> operator/ (float coef, const vector<4>& vec) noexcept;

	template vector<2>& operator/= (vector<2>& vec, float scalar) noexcept;
	template vector<3>& operator/= (vector<3>& vec, float scalar) noexcept;
	template vector<4>& operator/= (vector<4>& vec, float scalar) noexcept;

	template bool operator!= (const vector<2>& lhs, const vector<2>& rhs) noexcept;
	template bool operator!= (const vector<3>& lhs, const vector<3>& rhs) noexcept;
	template bool operator!= (const vector<4>& lhs, const vector<4>& rhs) noexcept;

	template bool operator== (const vector<2>& lhs, const vector<2>& rhs) noexcept;
	template bool operator== (const vector<3>& lhs, const vector<3>& rhs) noexcept;
	template bool operator== (const vector<4>& lhs, const vector<4>& rhs) noexcept;

	template float dot (const vector<2>& lhs, const vector<2>& rhs) noexcept;
	template float dot (const vector<3>& lhs, const vector<3>& rhs) noexcept;
	template float dot (const vector<4>& lhs, const vector<4>& rhs) noexcept;

	template vector<2> normalize (const vector<2>& vec) noexcept;
	template vector<3> normalize (const vector<3>& vec) noexcept;
	template vector<4> normalize (const vector<4>& vec) noexcept;

	template std::ostream& operator<< (std::ostream& stream, const vector<2>& vector) noexcept;
	template std::ostream& operator<< (std::ostream& stream, const vector<3>& vector) noexcept;
	template std::ostream& operator<< (std::ostream& stream, const vector<4>& vector) noexcept;

	template vector<2> reflect(const vector<2>& incident, const vector<2>& normal) noexcept;
	template vector<3> reflect(const vector<3>& incident, const vector<3>& normal) noexcept;
	template vector<4> reflect(const vector<4>& incident, const vector<4>& normal) noexcept;

	template vector<2> clamp(const vector<2>& vec, float min, float max) noexcept;
	template vector<3> clamp(const vector<3>& vec, float min, float max) noexcept;
	template vector<4> clamp(const vector<4>& vec, float min, float max) noexcept;

	template float angle (const vector<3>& a, const vector<3>& b) noexcept;

	template vector<3> abs(const vector<3>& a) noexcept;

	template float max(const vector<4>& a) noexcept;
	template float min(const vector<4>& a) noexcept;

	template vector<4> max(const vector<4>& a, const vector<4>& b) noexcept;
	template vector<4> min(const vector<4>& a, const vector<4>& b) noexcept;

	template vector<2> sqrt(const vector<2>& vec) noexcept;
	template vector<3> sqrt(const vector<3>& vec) noexcept;
	template vector<4> sqrt(const vector<4>& vec) noexcept;
}