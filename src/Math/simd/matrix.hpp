/*
 * @author  thomas
 * @date    25/11/18
 * Copyright (c) 2018 thomas
 */

#ifndef RAY_TRACER_MATRIX_HPP
#define RAY_TRACER_MATRIX_HPP

#include <immintrin.h>
#include <iostream>

#include "Math/vec4.hpp"

namespace RayTracer::Math
{
	class alignas(16 * sizeof(float)) simd_matrix
	{
		private:
			constexpr static int PRINT_PRECISION = 4;

			union
			{
				struct
				{
					__m256 ymm0;
					__m256 ymm1;
				};

				float data [16];
			};

			inline constexpr int flattenIndex (int row, int col) const noexcept
			{
				return (row * 4) + col;
			}

			simd_matrix(__m256 lo, __m256 hi) noexcept;

		public:
			simd_matrix() noexcept;

			simd_matrix(const simd_matrix& other) noexcept;

			simd_matrix(simd_matrix&& other) noexcept;

			simd_matrix& operator= (simd_matrix other) noexcept;

			const float& operator() (int row, int col) const noexcept;

			float& operator() (int row, int col) noexcept;

			friend std::ostream& operator<< (std::ostream& stream, const simd_matrix& simd_matrix);

			friend simd_matrix operator+ (const simd_matrix& lhs, const simd_matrix& rhs) noexcept;

			friend vec4 operator* (const simd_matrix& lhs, const vec4& rhs) noexcept;

			friend simd_matrix operator* (const simd_matrix& lhs, const simd_matrix& rhs) noexcept;
	};

	simd_matrix identity() noexcept;
}

#endif //RAY_TRACER_MATRIX_HPP
