/*
 * @author thomas
 * @date   23/10/18
 * @file   simdintrinh.hpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#ifndef RAY_TRACER_SIMDINTRINH_HPP
#define RAY_TRACER_SIMDINTRINH_HPP

#include <immintrin.h>

namespace RayTracer::Math
{
	/*
	 * Horizontal Accumulation of a __m128
	 * xmm  = [   D   |   C   |   B   |       A       ]
	 * shuf = [   C   |   D   |   A   |       B       ]
	 * sums = [ D + C | C + D | B + A |     A + B     ]
	 * shuf = [   C   |   D   | D + C |     C + D     ]
	 * sums = [ D + C | C + D | B + A | A + B + C + D ]
	 *
	 * Source: SSE 1 Version: https://stackoverflow.com/a/35270026
	 */
	inline float _mm_hacc (__m128 xmm) noexcept
	{
		__m128 shuf = _mm_shuffle_ps(xmm, xmm, _MM_SHUFFLE(2, 3, 0, 1));
		__m128 sums = _mm_add_ps(xmm, shuf);
		shuf        = _mm_movehl_ps(shuf, sums);
		sums        = _mm_add_ss(sums, shuf);
		return _mm_cvtss_f32(sums);
	}

	inline __m128 _mm_abs_ps (__m128 xmm) noexcept
	{
		const static __m128 ZERO_XMM = _mm_set1_ps(0.0f);
		__m128 negative = _mm_sub_ps(ZERO_XMM, xmm);
		return _mm_max_ps(negative, xmm);
	}
}

#endif //RAY_TRACER_SIMDINTRINH_HPP
