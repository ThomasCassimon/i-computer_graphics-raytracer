/*
 * @author thomas
 * @date   22/10/18
 * @file   SimdVec.hpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#ifndef RAY_TRACER_SIMDVEC_HPP
#define RAY_TRACER_SIMDVEC_HPP

#include <array>
#include <iomanip>
#include <iostream>
#include <sstream>

#include <cmath>
#include <immintrin.h>

#include <json/json.h>

#include "Math/simd/simdintrin.hpp"

namespace RayTracer::Math
{
	template<int N>
	class alignas(4 * sizeof(float)) vector
	{
		public:
			union
			{
				__m128 xmm;

				struct
				{
					union
					{
						float x;
						float r;
					};

					union
					{
						float y;
						float g;
					};

					union
					{
						float z;
						float b;
					};

					union
					{
						float w;
						float a;
					};
				};
			};

			constexpr vector() noexcept: x(0.0f), y(0.0f), z(0.0f), w(0.0f)
			{
			}

			explicit constexpr vector(float value) noexcept: x(value), y(value), z(value), w(value)
			{
			}

			explicit constexpr vector(__m128 vector) noexcept: xmm(vector)
			{
			}

			vector(float x, float y) noexcept;

			vector(float x, float y, float z) noexcept;

			vector(float x, float y, float z, float w) noexcept;

			vector(const vector<2>& vec, float scal) noexcept;

			vector(const vector<3>& vec, float scal) noexcept;

			explicit vector(const Json::Value& jsonVector);

			vector(vector<N>&& other) noexcept;

			vector(const vector<N>& other) noexcept = default;

			vector& operator= (vector<N> other) noexcept;

			float operator()(int element) const;

			explicit operator __m128 () noexcept;

			float length() const noexcept;

			vector normalize() const noexcept;

			vector<N>& swap (int idx1, int idx2) noexcept;
	};

	using simd_vec2 = vector<2>;
	using simd_vec3 = vector<3>;
	using simd_vec4 = vector<4>;

	template <int N>
	vector<N> operator+(const vector<N>& lhs, const vector<N>& rhs) noexcept;

	template <int N>
	vector<N>& operator+=(vector<N>& lhs, const vector<N>& rhs) noexcept;

	template <int N>
	vector<N>& operator+= (vector<N>& vector, float scalar) noexcept;

	template <int N>
	vector<N> operator-(const vector<N>& lhs, const vector<N>& rhs) noexcept;

	template <int N>
	vector<N> operator-(const vector<N>& vec) noexcept;

	template <int N>
	vector<N> operator*(const vector<N>& vec, float coef) noexcept;

	template <int N>
	vector<N> operator*(float coef, const vector<N>& vec) noexcept;

	template <int N>
	vector<N> operator*(const vector<N>& lhs, const vector<N>& rhs) noexcept;

	template <int N>
	vector<N>& operator*=(vector<N>& lhs, const vector<N>& rhs) noexcept;

	template <int N>
	vector<N> operator/ (const vector<N>& vec, float coef) noexcept;

	template <int N>
	vector<N> operator/ (const vector<N>& lhs, const vector<N>& rhs) noexcept;

	template<int N>
	vector<N> operator/ (float coef, const vector<N>& vec) noexcept;

	template <int N>
	vector<N>& operator/= (vector<N>& vec, float scalar) noexcept;

	template <int N>
	bool operator!= (const vector<N>& lhs, const vector<N>& rhs) noexcept;

	template <int N>
	bool operator== (const vector<N>& lhs, const vector<N>& rhs) noexcept;

	template <int N>
	float dot (const vector<N>& lhs, const vector<N>& rhs) noexcept;

	template <int N>
	vector<N> normalize (const vector<N>& vec) noexcept;

	/**
	 * Computes the cross product between 2 SIMD vectors
	 * @param lhs
	 * @param rhs
	 * @return
	 */
	vector<3> cross (const vector<3>& lhs, const vector<3>& rhs) noexcept;

	template <int N>
	vector<N> abs (const vector<N>& vec) noexcept;

	template <int N>
	float max (const vector<N>& vec) noexcept;

	template <int N>
	float min (const vector<N>& vec) noexcept;

	template <int N>
	vector<N> max (const vector<N>& lhs, const vector<N>& rhs) noexcept;

	template <int N>
	vector<N> min (const vector<N>& lhs, const vector<N>& rhs) noexcept;

	template <int N>
	std::ostream& operator<<(std::ostream& stream, const vector<N>& vector) noexcept;

	// Vector reflection as described in https://math.stackexchange.com/a/13266
	// Optimized for SIMD processing
	template <int N>
	vector<N> reflect(const vector<N>& incident, const vector<N>& normal) noexcept;

	//todo: Unit Test
	template <int N>
	vector<N> clamp(const vector<N>& vec, float min, float max) noexcept;

	//todo: Unit Test
	template <int N>
	float angle (const vector<N>& a, const vector<N>& b) noexcept;

	//todo: Unit Test
	template <int N>
	vector<N> sqrt(const vector<N>& vec) noexcept;
}

#endif //RAY_TRACER_SIMDVEC_HPP
