/*
 * @author thomas
 * @date   05/10/18
 * @file   Util.cpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#include "Util.hpp"

namespace RayTracer::Math
{
	float dist (const Math::vec3& v1, const Math::vec3& v2) noexcept
	{
		return (v2 - v1).length();
	}

	float degreesToRadians(float deg)
	{
		return (deg / 360.0f) * 2.0f * PI;
	}

	float beckmann (float delta, float m) noexcept
	{
		float cosdelta = std::cos(delta);
		return std::exp(-std::pow(std::tan(delta) / m, 2.0f)) / (4 * m * m * cosdelta * cosdelta * cosdelta * cosdelta);
	}
}