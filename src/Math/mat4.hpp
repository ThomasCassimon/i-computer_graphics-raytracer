/*
 * @author thomas
 * @date   23/10/18
 * @file   mat4.hpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#ifndef RAY_TRACER_MAT4_HPP
#define RAY_TRACER_MAT4_HPP

#ifdef USE_SIMD
	#include "Math/simd/matrix.hpp"
#else
	#include "Math/unrolled/mat4.hpp"
#endif
namespace RayTracer::Math
{
	#ifdef USE_SIMD
		using mat4 = simd_matrix;
	#else
		using mat4 = matrix;
	#endif
}

#endif //RAY_TRACER_MAT4_HPP
