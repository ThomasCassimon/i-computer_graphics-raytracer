/*
 * @author thomas
 * @date   23/10/18
 * @file   vec3.hpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#ifndef RAY_TRACER_VEC3_HPP
#define RAY_TRACER_VEC3_HPP

#ifdef USE_SIMD
	#include "Math/unrolled/vec3.hpp"
	#include "Math/simd/vector.hpp"
#else
	#include "Math/unrolled/vec3.hpp"
#endif

namespace RayTracer::Math
{
	#ifdef USE_SIMD
		using vec3 = simd_vec3;
		//todo: Write SIMD version
		using ivec3 = vector3<int>;
	#else
		using vec3 = vector3<float>;
		using ivec3 = vector3<int>;
	#endif
}

#endif //RAY_TRACER_VEC3_HPP
