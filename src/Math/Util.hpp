/*
 * @author thomas
 * @date   05/10/18
 * @file   Util.hpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#ifndef RAY_TRACER_UTIL_HPP
#define RAY_TRACER_UTIL_HPP

#include <Math/vec3.hpp>

namespace RayTracer::Math
{
	constexpr float PI = 3.14159265359f;

	float dist (const Math::vec3& v1, const Math::vec3& v2) noexcept;

	float degreesToRadians(float deg);

	float beckmann (float delta, float m) noexcept;
}

#endif //RAY_TRACER_UTIL_HPP
