/*
 * @author thomas
 * @date   23/10/18
 * @file   vec4.hpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#ifndef RAY_TRACER_VEC4_HPP
#define RAY_TRACER_VEC4_HPP

#ifdef USE_SIMD
	#include "Math/unrolled/vec4.hpp"
	#include "Math/simd/vector.hpp"
#else
	#include "Math/unrolled/vec4.hpp"
#endif

namespace RayTracer::Math
{
	#ifdef USE_SIMD
		using vec4 = simd_vec4;
		//todo: Write SIMD version
		using ivec4 = vector4<int>;
	#else
		using vec4 = vector4<float>;
		using ivec4 = vector4<int>;
	#endif

}

#endif //RAY_TRACER_VEC4_HPP
