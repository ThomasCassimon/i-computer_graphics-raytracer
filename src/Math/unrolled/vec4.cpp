/*
 * Created by thomas on 28/09/18.
 */

#include "vec4.hpp"

namespace RayTracer::Math
{
	template <typename T>
	vector4<T>::vector4() noexcept: x(T{}), y(T{}), z(T{}), w(T{})
	{
	}

	template <typename T>
	vector4<T>::vector4(const T& val) noexcept: x(val), y(val), z(val), w(val)
	{
	}

	template <typename T>
	vector4<T>::vector4(const vector3<T>& vec, const T& w) noexcept: x(vec.x), y(vec.y), z(vec.z), w(w)
	{
	}

	template <typename T>
	vector4<T>::vector4(const T& x, const T& y, const T& z, const T& w) noexcept: x(x), y(y), z(z), w(w)
	{
	}

	template <typename T>
	const T& vector4<T>::operator() (int element) const
	{
		switch(element)
		{
			case 0:
				return this->x;

			case 1:
				return this->y;

			case 2:
				return this->z;

			case 3:
				return this->w;

			default:
				throw std::out_of_range("Tried to access " + std::to_string(element) + "-th element in a 4D vector.");
		}
	}

	template <typename T>
	T& vector4<T>::operator()(int element)
	{
		switch(element)
		{
			case 0:
				return this->x;

			case 1:
				return this->y;

			case 2:
				return this->z;

			case 3:
				return this->w;

			default:
				throw std::out_of_range("Tried to access " + std::to_string(element) + "-th element in a 4D vector.");
		}
	}

	template <typename T>
	T vector4<T>::length() const noexcept
	{
		return std::sqrt(std::pow(this->x, 2.0f) + std::pow(this->y, 2.0f) + std::pow(this->z, 2.0f) + std::pow(this->w, 2.0f));
	}

	template <typename T>
	vector4<T> vector4<T>::normalize() const noexcept
	{
		return (*this) / this->length();
	}

	template class vector4<short>;
	template class vector4<int>;
	template class vector4<long>;
	template class vector4<float>;
	template class vector4<double>;
}