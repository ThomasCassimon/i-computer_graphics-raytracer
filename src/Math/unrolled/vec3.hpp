/*
 * Created by thomas on 28/09/18.
 */

#ifndef RAY_TRACER_UNROLLED_VEC3_HPP
#define RAY_TRACER_UNROLLED_VEC3_HPP

#include <iomanip>
#include <string>
#include <sstream>
#include <variant>

#include <cmath>

namespace RayTracer::Math
{
	template <typename T>
	class vector3
	{
		private:
			constexpr static int PRINT_PRECISION = 4;

		public:
			union
			{
				T x;
				T r;
			};

			union
			{
				T y;
				T g;
			};

			union
			{
				T z;
				T b;
			};

			constexpr vector3() noexcept: x(T{}), y(T{}), z(T{})
			{
			}

			explicit constexpr vector3(const T& val) noexcept: x(val), y(val), z(val)
			{
			}

			constexpr vector3(const T& x, const T& y, const T& z) noexcept: x(x), y(y), z(z)
			{
			}

			const T& operator() (int element) const;

			T& operator() (int element);

			T length() const noexcept;

			vector3 normalize() const noexcept;

			friend std::ostream& operator<< (std::ostream& stream, const vector3<T>& vector) noexcept
			{
				stream << "(" << std::fixed << std::setprecision(vector3<T>::PRINT_PRECISION) << vector.x << ", " << std::fixed << std::setprecision(vector3<T>::PRINT_PRECISION) << vector.y << ", " << std::fixed << std::setprecision(vector3<T>::PRINT_PRECISION) << vector.z << ")";
				return stream;
			}

			/*
			friend std::string to_string (const vector3<T>& vec) noexcept
			{
				std::stringstream stream;
				stream << "(" << std::fixed << std::setprecision(vector3<T>::PRINT_PRECISION) << vec.x << ", " << std::fixed << std::setprecision(vector3<T>::PRINT_PRECISION) << vec.y << ", " << std::fixed << std::setprecision(vector3<T>::PRINT_PRECISION) << vec.z << ")";
				return stream.str();
			}
			*/
	};

	template <typename T>
	vector3<T> operator+(const vector3<T>& v1, const vector3<T>& v2) noexcept
	{
		return vector3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
	}

	template <typename T>
	vector3<T>& operator+=(vector3<T>& v1, const vector3<T>& v2) noexcept
	{
		v1 = vector3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
		return v1;
	}

	template <typename T>
	vector3<T>& operator+=(vector3<T>& v1, const T& coef) noexcept
	{
		v1 = vector3(v1.x + coef, v1.y + coef, v1.z + coef);
		return v1;
	}

	template <typename T>
	vector3<T> operator-(const vector3<T>& v1, const vector3<T>& v2) noexcept
	{
		return vector3(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
	}

	template <typename T>
	vector3<T> operator- (const vector3<T>& v) noexcept
	{
		return vector3(-v.x, -v.y, -v.z);
	}

	template <typename T>
	vector3<T> operator* (const vector3<T>& vec,const T& coef) noexcept
	{
		return vector3(vec.x * coef, vec.y * coef, vec.z * coef);
	}

	template <typename T>
	vector3<T> operator* (const vector3<T>& lhs,const vector3<T>& rhs) noexcept
	{
		return vector3(lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z);
	}

	template <typename T>
	vector3<T> operator* (T coef, const vector3<T>& vec) noexcept
	{
		return vector3<T>(coef * vec.x, coef * vec.y, coef * vec.z);
	}

	template <typename T>
	vector3<T> operator/ (const vector3<T>& vec, const T& coef) noexcept
	{
		return vector3<T>(vec.x / coef, vec.y / coef, vec.z / coef);
	}

	template <typename T>
	vector3<T> operator/ (const T& coef, const vector3<T>& vec) noexcept
	{
		return vector3<T>(coef / vec.x, coef / vec.y, coef / vec.z);
	}

	template <typename T>
	bool operator!= (const vector3<T>& v1, const vector3<T>& v2) noexcept
	{
		return (v1.x != v2.x) || (v1.y != v2.y) || (v1.z != v2.z);
	}

	template <typename T>
	bool operator== (const vector3<T>& v1, const vector3<T>& v2) noexcept
	{
		return (std::fabs(v1.x - v2.x) < std::numeric_limits<T>::epsilon()) && (std::fabs(v1.y - v2.y) < std::numeric_limits<T>::epsilon()) && (std::fabs(v1.z - v2.z) < std::numeric_limits<T>::epsilon());
	}

	template <typename T>
	vector3<T> normalize (const vector3<T>& vec) noexcept
	{
		return vec.normalize();
	}

	template <typename T>
	T dot (const vector3<T>& vec1, const vector3<T>& vec2) noexcept
	{
		return (vec1.x * vec2.x) + (vec1.y * vec2.y) + (vec1.z * vec2.z);
	}

	template <typename T>
	vector3<T> cross(const vector3<T>& v1, const vector3<T>& v2) noexcept
	{
		return vector3<T>((v1.y * v2.z) - (v1.z * v2.y), (v1.z * v2.x) - (v1.x * v2.z), (v1.x * v2.y) - (v1.y * v2.x));
	}

	//todo: Unit Test
	template <typename T>
	vector3<T> abs (const vector3<T>& vec) noexcept
	{
		return vector3<T>(std::abs(vec.x), std::abs(vec.y), std::abs(vec.z));
	}

	// Vector reflection implemented as of https://math.stackexchange.com/a/13266
	//todo: Unit Test
	template <typename T>
	vector3<T> reflect(const vector3<T>& incident, const vector3<T>& normal) noexcept
	{
		return incident - ((Math::dot((2.0f * incident), normal) / Math::dot(normal, normal)) * normal);
	}

	template <typename T>
	vector3<T> clamp(const vector3<T>& vector, const T& min, const T& max) noexcept
	{
		vector3<T> copy = vector;

		for (int i = 0; i < 3; i++)
		{
			copy(i) = std::max(copy(i), min);
			copy(i) = std::min(copy(i), max);
		}

		return copy;
	}
}

#endif //RAY_TRACER_UNROLLED_VEC3_HPP
