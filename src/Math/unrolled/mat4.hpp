/*
 * Created by thomas on 28/09/18.
 */

#ifndef RAY_TRACER_ULNROLLED_MATRIX_HPP
#define RAY_TRACER_ULNROLLED_MATRIX_HPP

#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>

#include <Math/vec4.hpp>

namespace RayTracer::Math
{
	class matrix
	{
		private:
			constexpr static int MATRIX_SIZE = 4;
			constexpr static int PRINT_PRECISION = 4;
			float data [MATRIX_SIZE * MATRIX_SIZE];

			inline int flattenIndex (int row, int col) const noexcept
			{
				return (row * MATRIX_SIZE) + col;
			}

		public:
			matrix() noexcept;

			matrix (const matrix& matrix);

			matrix (matrix&& matrix) noexcept;

			matrix (const vec4& col0, const vec4& col1, const vec4& col2, const vec4& col3) noexcept;

			matrix& operator= (matrix matrix);

			const float& operator() (int row, int col) const noexcept;

			float& operator() (int row, int col) noexcept;

			std::pair<int, int> size() const noexcept;

			friend void swap (matrix& m1, matrix& m2) noexcept;

			friend std::ostream& operator<< (std::ostream& stream, const matrix& matrix);
	};

	matrix operator+ (const matrix& m1, const matrix& m2) noexcept;

	vec4 operator* (const matrix& matrix, const vec4& vector) noexcept;

	matrix operator* (const matrix& m1, const matrix& m2) noexcept;

	matrix identity() noexcept;
}

#endif //RAY_TRACER_ULNROLLED_MATRIX_HPP
