/*
 * @author thomas
 * @date   12/10/18
 * @file   Vec2.cpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#include "vec2.hpp"

#include <cmath>

#include <stdexcept>

namespace RayTracer::Math
{
	template<typename T>
	const T& vector2<T>::operator()(int element) const
	{
		switch(element)
		{
			case 0:
				return this->x;
			case 1:
				return this->y;
			default:
				throw std::out_of_range("Tried to access element at index " + std::to_string(element) +  " in a 2D vector.");
		}
	}

	template<typename T>
	T& vector2<T>::operator()(int element)
	{
		switch(element)
		{
			case 0:
				return this->x;
			case 1:
				return this->y;
			default:
				throw std::out_of_range("Tried to access element at index " + std::to_string(element) +  " in a 2D vector.");
		}
	}

	template<typename T>
	T vector2<T>::length() const noexcept
	{
		return std::sqrt(std::pow(this->x, vector2::two()) + std::pow(this->y, vector2::two()));
	}

	template<typename T>
	vector2<T> vector2<T>::normalize() const noexcept
	{
		return *this / this->length();
	}

	template class vector2<short>;
	template class vector2<int>;
	template class vector2<long>;
	template class vector2<float>;
	template class vector2<double>;
}