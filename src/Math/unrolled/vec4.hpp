/*
 * Created by thomas on 28/09/18.
 */

#ifndef RAY_TRACER_UNROLLED_VEC4_HPP
#define RAY_TRACER_UNROLLED_VEC4_HPP

#include <iomanip>
#include <string>
#include <sstream>

#include <cmath>

#include <Math/vec3.hpp>

namespace RayTracer::Math
{
	template <typename T>
	class vector4
	{
		private:
			constexpr static int PRINT_PRECISION = 4;

		public:
			union
			{
				T x;
				T r;
			};

			union
			{
				T y;
				T g;
			};

			union
			{
				T z;
				T b;
			};

			union
			{
				T w;
				T a;
			};

			vector4() noexcept;

			explicit vector4(const T& val) noexcept;

			vector4(const vector3<T>& vec, const T& w) noexcept;

			vector4(const T& x, const  T& y, const T& z, const T& w) noexcept;

			vector4 (const vector4<T>& other) noexcept = default;

			const T& operator() (int element) const;

			T& operator() (int element);

			T length() const noexcept;

			vector4 normalize() const noexcept;

			friend std::string to_string (const vector4& vec) noexcept
			{
				std::stringstream stream;
				stream << "(" << std::fixed << std::setprecision(vector4<T>::PRINT_PRECISION) << vec.x << ", " << std::fixed << std::setprecision(vector4<T>::PRINT_PRECISION) << vec.y << ", " << std::fixed << std::setprecision(vector4<T>::PRINT_PRECISION) << vec.z << ", " << std::fixed << std::setprecision(vector4<T>::PRINT_PRECISION) << vec.w << ")";
				return stream.str();
			}
	};

	template <typename T>
	vector4<T> operator+(const vector4<T>& vec1, const vector4<T>& vec2) noexcept
	{
		return vector4(vec1.x + vec2.x, vec1.y + vec2.y, vec1.z + vec2.z, vec1.w + vec2.w);
	}

	template <typename T>
	vector4<T> operator-(const vector4<T>& vec1, const vector4<T>& vec2) noexcept
	{
		return vector4(vec1.x - vec2.x, vec1.y - vec2.y, vec1.z - vec2.z, vec1.w - vec2.w);
	}

	template <typename T>
	vector4<T> operator*(const vector4<T>& vec, const T& coef) noexcept
	{
		return vector4(vec.x * coef, vec.y * coef, vec.z * coef, vec.w * coef);
	}

	template <typename T>
	vector4<T> operator*(const T& coef, const vector4<T>& vec) noexcept
	{
		return vector4(coef * vec.x, coef * vec.y, coef * vec.z, coef * vec.w);
	}

	template <typename T>
	vector4<T> operator/(const vector4<T>& vec, const T& coef) noexcept
	{
		return vector4<T>(vec.x / coef, vec.y / coef, vec.z / coef, vec.w / coef);
	}

	template <typename T>
	vector4<T> normalize(const vector4<T>& vec) noexcept
	{
		return vec.normalize();
	}

	template <typename T>
	T dot (const vector4<T>& vec1, const vector4<T>& vec2) noexcept
	{
		return (vec1.x * vec2.x) + (vec1.y * vec2.y) + (vec1.z * vec2.z) + (vec1.w * vec2.w);
	}
}

#endif //RAY_TRACER_UNROLLED_VEC4_HPP
