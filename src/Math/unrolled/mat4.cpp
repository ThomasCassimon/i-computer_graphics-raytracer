/*
 * Created by thomas on 28/09/18.
 */

#include "mat4.hpp"
#include <Math/vec4.hpp>

namespace RayTracer::Math
{
	matrix::matrix() noexcept
	{
		for (float& f : this->data)
		{
			f = 0;
		}
	}

	matrix::matrix(const matrix& matrix)
	{
		for (int i = 0; i < matrix.size().first; i++)
		{
			for (int j = 0; j < matrix.size().second; j++)
			{
				(*this)(i,j) = matrix(i,j);
			}
		}
	}

	//todo: Unit Test
	matrix::matrix(const vec4& col0, const vec4& col1, const vec4& col2, const vec4& col3) noexcept
	{
		for (int i = 0; i < MATRIX_SIZE; i++)
		{
			this->data[this->flattenIndex(i, 0)]  = col0(i);
			this->data[this->flattenIndex(i, 1)]  = col1(i);
			this->data[this->flattenIndex(i, 2)]  = col2(i);
			this->data[this->flattenIndex(i, 3)]  = col3(i);
		}
	}

	matrix::matrix(matrix&& matrix) noexcept
	{
		swap(*this, matrix);
	}

	matrix& matrix::operator=(matrix matrix)
	{
		swap(*this, matrix);
		return *this;
	}

	const float& matrix::operator() (int row, int col) const noexcept
	{
		return this->data[this->flattenIndex(row, col)];
	}

	float& matrix::operator() (int row, int col) noexcept
	{
		return this->data[this->flattenIndex(row, col)];
	}

	std::pair<int, int> matrix::size() const noexcept
	{
		return std::pair<int, int>(matrix::MATRIX_SIZE, matrix::MATRIX_SIZE);
	}

	matrix operator+(const matrix& m1, const matrix& m2) noexcept
	{
		matrix result = m1;

		for (int i = 0; i < result.size().first; i++)
		{
			for (int j = 0; j < result.size().second; j++)
			{
				result(i,j) += m2(i,j);
			}
		}

		return result;
	}

	vec4 operator* (const matrix& matrix, const vec4& vector) noexcept
	{
		//Math::vec4 result;
		std::array<float, 4> result = {};

		for (int i = 0; i < matrix.size().first; i++)
		{
			float sum = 0.0f;

			for (int j = 0; j < matrix.size().second; j++)
			{
				sum += matrix(i, j) * vector(j);
			}

			result[i] = sum;
		}

		return Math::vec4(result[0], result[1], result[2], result[3]);
	}

	matrix operator* (const matrix& m1, const matrix& m2) noexcept
	{
		Math::matrix result;

		for (int i = 0; i < m1.size().first; i++)
		{
			for (int j = 0; j < m2.size().second; j++)
			{
				for (int k = 0; k < m1.size().second; k++)
				{
					result(i,j) += m1(i, k) * m2(k, j);
				}
			}
		}

		return result;
	}

	matrix identity() noexcept
	{
		matrix identity;

		for (int i = 0; i < identity.size().first; i++)
		{
			for (int j = 0; j < identity.size().second; j++)
			{
				if (i == j)
				{
					identity(i,j) = 1.0f;
				}
			}
		}

		return identity;
	}

	void swap (matrix& m1, matrix& m2) noexcept
	{
		for (int i = 0; i < m1.size().first; i++)
		{
			for (int j = 0; j < m2.size().second; j++)
			{
				std::swap(m1.data[m1.flattenIndex(i,j)], m2.data[m2.flattenIndex(i,j)]);
			}
		}
	}

	std::ostream& operator<<(std::ostream& stream, const matrix& matrix)
	{
		stream << "[";

		for (int i = 0; i < matrix.size().first; i++)
		{
			if (i != 0)
			{
				stream << " ";
			}

			stream << "[";

			for (int j = 0; j < matrix.size().second; j++)
			{
				stream << std::fixed << std::setprecision(matrix::PRINT_PRECISION) << std::setfill(' ') << matrix(i,j);

				if ((j + 1) !=  matrix.size().second)
				{
					stream << ", ";
				}
			}

			stream << "]";

			if ((i + 1) == matrix.size().first)
			{
				stream << "]";
			}

			stream << "\r\n";
		}

		return stream;
	}
}