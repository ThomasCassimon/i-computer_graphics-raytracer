/*
 * Created by thomas on 28/09/18.
 */

#include "vec3.hpp"

namespace RayTracer::Math
{
	template <typename T>
	const T& vector3<T>::operator()(int element) const
	{
		switch(element)
		{
			case 0:
				return this->x;

			case 1:
				return this->y;

			case 2:
				return this->z;

			default:
				throw std::out_of_range("Tried to access " + std::to_string(element) + "-th element in a 3D vector.");
		}
	}

	template <typename T>
	T& vector3<T>::operator()(int element)
	{
		switch(element)
		{
			case 0:
				return this->x;

			case 1:
				return this->y;

			case 2:
				return this->z;

			default:
				throw std::out_of_range("Tried to access " + std::to_string(element) + "-th element in a 3D vector.");
		}
	}

	template <typename T>
	T vector3<T>::length() const noexcept
	{
		return std::sqrt(std::pow(this->x, 2.0f) + std::pow(this->y, 2.0f) + std::pow(this->z, 2.0f));
	}

	template <typename T>
	vector3<T> vector3<T>::normalize() const noexcept
	{
		return (*this) / this->length();
	}

	template class vector3<short>;
	template class vector3<int>;
	template class vector3<long>;
	template class vector3<float>;
	template class vector3<double>;
}