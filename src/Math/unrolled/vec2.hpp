/*
 * @author thomas
 * @date   12/10/18
 * @file   Vec2.hpp
 *
 * Copyright 2018 Thomas Cassimon
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef RAY_TRACER_UNROLLED_VEC2_HPP
#define RAY_TRACER_UNROLLED_VEC2_HPP

#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>

#include <cmath>

namespace RayTracer::Math
{
	template <typename T>
	class vector2
	{
		private:
			constexpr static int PRINT_PRECISION = 4;

			static constexpr T two() noexcept
			{
				if constexpr (std::is_same<T, short>::value)
				{
					return static_cast<short>(2);
				}
				else if constexpr (std::is_same<T, int>::value)
				{
					return 2;
				}
				else if constexpr (std::is_same<T, long>::value)
				{
					return 2L;
				}
				else if constexpr (std::is_same<T, float>::value)
				{
					return 2.0f;
				}
				else if constexpr (std::is_same<T, double>::value)
				{
					return 2.0;
				}
			}

		public:
			union
			{
				T x;
				T u;
			};

			union
			{
				T y;
				T v;
			};

			constexpr vector2() noexcept : x(T{}), y(T{})
			{
			}

			explicit constexpr vector2(const T& val) noexcept : x(val), y(val)
			{
			}

			constexpr  vector2(const T& x, const T& y) noexcept : x(x), y(y)
			{
			}

			constexpr vector2(const vector2<T>& other) noexcept : x(other.x), y(other.y)
			{
			}

			const T& operator() (int element) const;

			T& operator() (int element);

			T length() const noexcept;

			vector2<T> normalize() const noexcept;

			friend std::ostream& operator<< (std::ostream& stream, const vector2<T>& vec) noexcept
			{
				stream << "(" << std::fixed << std::setprecision(vector2<T>::PRINT_PRECISION) << vec.x << ", " << std::fixed << std::setprecision(vector2<T>::PRINT_PRECISION) << vec.y << ")";
				return stream;
			}
	};

	template <typename T>
	vector2<T> operator/ (const vector2<T>& vec, const T& scalar) noexcept
	{
		return vector2<T>(vec.x / scalar, vec.y / scalar);
	}

	template <typename T>
	vector2<T>& operator+= (vector2<T>& lhs, const vector2<T>& rhs) noexcept
	{
			lhs.x += rhs.x;
			lhs.y += rhs.y;
			return lhs;
	}

	template <typename T>
	vector2<T> operator+ (const vector2<T>& lhs, const vector2<T>& rhs) noexcept
	{
		Math::vector2<T> vec (lhs);
		vec.x += rhs.x;
		vec.y += rhs.y;
		return vec;
	}

	template <typename T>
	bool operator== (const vector2<T>& lhs, const vector2<T>& rhs) noexcept
	{
		if constexpr (std::is_same<T, float>::value)
		{
			return (std::fabs(lhs.x - rhs.x) < std::numeric_limits<float>::epsilon()) && (std::fabs(lhs.y - rhs.y) < std::numeric_limits<float>::epsilon());
		}
		else if constexpr (std::is_same<T, double>::value)
		{
			return (std::fabs(lhs.x - rhs.x) < std::numeric_limits<double>::epsilon()) && (std::fabs(lhs.y - rhs.y) < std::numeric_limits<double>::epsilon());
		}
		else
		{
			return (lhs.x == rhs.x) && (lhs.y == rhs.y);
		}
	}
}

#endif //RAY_TRACER_UNROLLED_VEC2_HPP
