/*
 * @author thomas
 * @date   19/10/18
 * @file   TransformFactory.cpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#include "TransformFactory.hpp"
#include "Math/Util.hpp"

namespace RayTracer::Math
{
	Math::mat4 TransformFactory::generateTranslation(const RayTracer::Math::vec3& delta) const noexcept
	{
		Math::mat4 matrix;

		for (int i = 0; i < 4; ++i)
		{
			matrix(i,i) = 1.0f;
		}

		matrix(0, 3) = delta.x;
		matrix(1, 3) = delta.y;
		matrix(2, 3) = delta.z;

		return matrix;
	}

	Math::mat4 TransformFactory::generateScale(const Math::vec3& scale) const noexcept
	{
		Math::mat4 matrix;

		for (int i = 0; i < 3; i++)
		{
			matrix(i,i) = scale(i);
		}

		matrix(3,3) = 1.0f;

		return matrix;
	}

	Math::mat4 TransformFactory::generateRotation(Math::vec3 axis, float angle) const noexcept
	{
		axis = Math::normalize(axis);

		Math::mat4 matrix;

		// First Row
		matrix(0,0) = std::cos(angle) + (std::pow(axis.x, 2.0f) * (1 - std::cos(angle)));
		matrix(0,1) = (axis.x * axis.y * (1 - std::cos(angle))) - (axis.z * std::sin(angle));
		matrix(0,2) = (axis.x * axis.z * (1 - std::cos(angle))) + (axis.y * std::sin(angle));

		// Second Row
		matrix(1,0) = (axis.y * axis.x * (1 - std::cos(angle))) + (axis.z * std::sin(angle));
		matrix(1,1) = std::cos(angle) + (std::pow(axis.y, 2.0f) * (1 - std::cos(angle)));
		matrix(1,2) = (axis.y * axis.z * (1 - std::cos(angle))) - (axis.x * std::sin(angle));

		// Third Row
		matrix(2,0) = (axis.z * axis.x * (1 - std::cos(angle))) - (axis.y * std::sin(angle));
		matrix(2,1) = (axis.z * axis.y * (1 - std::cos(angle))) + (axis.x * std::sin(angle));
		matrix(2,2) = std::cos(angle) + (std::pow(axis.z, 2.0f) * (1 - std::cos(angle)));

		matrix(3,3) = 1.0f;

		return matrix;
	}

	Math::Transformation TransformFactory::translate(const Math::vec3& delta) const noexcept
	{
		return Math::Transformation(this->generateTranslation(delta), this->generateTranslation(-delta));
	}

	Math::Transformation TransformFactory::scale(const Math::vec3& scale) const noexcept
	{
		return Math::Transformation(this->generateScale(scale), this->generateScale(1.0f / scale));
	}

	Math::Transformation TransformFactory::rotate(Math::vec3 axis, float angle) const noexcept
	{
		return Math::Transformation(this->generateRotation(axis, Math::degreesToRadians(angle)), this->generateRotation(axis, Math::degreesToRadians(-angle)));
	}

	Math::Transformation TransformFactory::parse(const Json::Value& transformJson)
	{
		const Json::Value& jsonType = transformJson[TYPE_KEY.data()];

		if (jsonType.isNull())
		{
			throw std::runtime_error("Transformation didn't contain type.");
		}

		if (jsonType.asString() == TRANSLATION_TYPE)
		{
			const Json::Value& jsonValue = transformJson[VALUE_KEY.data()];

			if (jsonValue.isNull())
			{
				throw std::runtime_error("Translation didn't contain a value.");
			}

			return this->translate(Math::vec3(jsonValue));
		}
		else if (jsonType.asString() == SCALE_TYPE)
		{
			const Json::Value& jsonValue = transformJson[VALUE_KEY.data()];

			if (jsonValue.isNull())
			{
				throw std::runtime_error("Scale didn't contain a value.");
			}

			return this->scale(Math::vec3(jsonValue));
		}
		else if (jsonType.asString() == ROTATE_TYPE)
		{
			const Json::Value& jsonAngle = transformJson[ANGLE_KEY.data()];
			const Json::Value& jsonAxis = transformJson[AXIS_KEY.data()];

			if (jsonAngle.isNull())
			{
				throw std::runtime_error("Rotation didn't contain a angle.");
			}

			if (jsonAxis.isNull())
			{
				throw std::runtime_error("Rotation didn't contain a axis.");
			}

			return this->rotate(Math::vec3(jsonAxis), jsonAngle.asFloat());
		}
		else
		{
			throw std::runtime_error("Unsupported transformation type: \"" + jsonType.asString() + "\".");
		}
	}
}