/*
 * @author thomas
 * @date   01/11/18
 * @file   Transformation.cpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#include "Transformation.hpp"

namespace RayTracer::Math
{
	Transformation::Transformation() noexcept: forwardTransform(identity()), inverseTransform(identity())
	{
	}

	Transformation::Transformation(const Math::mat4 &forward, const Math::mat4 &inverse) noexcept: forwardTransform(forward), inverseTransform(inverse)
	{
	}

	Math::vec4 Transformation::forward(const Math::vec4& vec) const noexcept
	{
		return this->forwardTransform * vec;
	}

	Math::vec4 Transformation::inverse(const Math::vec4 &vec) const noexcept
	{
		return this->inverseTransform * vec;
	}

	Transformation operator*(const Transformation &lhs, const Transformation &rhs) noexcept
	{
		Math::mat4 forwardTransform = rhs.forwardTransform * lhs.forwardTransform;
		Math::mat4 inverseTransform = lhs.inverseTransform * rhs.inverseTransform;

		return Transformation(forwardTransform, inverseTransform);
	}

	Transformation &Transformation::operator*=(const Transformation &other) noexcept
	{
		*this = (*this) * other;
		return *this;
	}

	std::ostream& operator<<(std::ostream& stream, const Transformation& transform)
	{
		stream << "Forward:\n" << transform.forwardTransform << "\nInverse\n" << transform.inverseTransform;
		return stream;
	}
}