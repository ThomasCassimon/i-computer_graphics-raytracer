/*
 * @author  thomas
 * @date    09/11/18
 * Copyright (c) 2018 thomas
 */

#include "GlobalLight.hpp"

namespace RayTracer::Lights
{
	GlobalLight::GlobalLight(const Json::Value& jsonLight) : Light(jsonLight)
	{
	}

	GlobalLight::GlobalLight(const Graphics::Color& color, float intensity) noexcept: Light(color, intensity)
	{
	}
}