/*
 * @author  thomas
 * @date    12/11/18
 * Copyright (c) 2018 thomas
 */

#ifndef RAY_TRACER_POINTLIGHT_HPP
#define RAY_TRACER_POINTLIGHT_HPP

#include "Math/vec3.hpp"
#include "Lights/Light.hpp"

namespace RayTracer::Lights
{
	class PointLight: public Light
	{
		private:
			constexpr static std::string_view POSITION_KEY = "position";

			Math::vec3 position;

		public:
			explicit PointLight (const Json::Value& jsonLight);

			PointLight(const Graphics::Color& color, float intensity, const Math::vec3& position) noexcept;

			const Math::vec3&  getPosition() const noexcept;
	};
}

#endif //RAY_TRACER_POINTLIGHT_HPP
