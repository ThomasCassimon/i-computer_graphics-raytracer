/*
 * @author  thomas
 * @date    09/11/18
 * Copyright (c) 2018 thomas
 */

#include "Light.hpp"

namespace RayTracer::Lights
{
	Light::Light(const Json::Value& jsonLight)
	{
		const Json::Value jsonColor = jsonLight[std::string(COLOR_KEY)];
		const Json::Value jsonIntensity = jsonLight[std::string(INTENSITY_KEY)];

		if (jsonColor.isNull())
		{
			std::stringstream stream;
			stream << "JSON light didn't contain color.\n" << jsonLight;
			throw std::runtime_error(stream.str());
		}

		if (jsonIntensity.isNull())
		{
			std::stringstream stream;
			stream << "JSON light didn't contain intensity.\n" << jsonLight;
			throw std::runtime_error(stream.str());
		}

		this->color = Graphics::Color(Math::vec3(jsonColor) / 255.0f);
		this->intensity = jsonIntensity.asFloat();
	}

	Light::Light(const Graphics::Color& color, float intensity) noexcept: color(color), intensity(intensity)
	{
	}

	const Graphics::Color& Light::getColor() const noexcept
	{
		return this->color;
	}

	float Light::getIntensity() const noexcept
	{
		return this->intensity;
	}
}