/*
 * @author  thomas
 * @date    09/11/18
 * Copyright (c) 2018 thomas
 */

#ifndef RAY_TRACER_LIGHT_HPP
#define RAY_TRACER_LIGHT_HPP

#include <json/json.h>

#include "Graphics/Color.hpp"
#include "Math/vec3.hpp"

namespace RayTracer::Lights
{
	class Light
	{
		protected:
			constexpr static std::string_view COLOR_KEY = "color";
			constexpr static std::string_view INTENSITY_KEY = "intensity";

			Graphics::Color color;
			float intensity;

			explicit Light(const Json::Value& jsonLight);

			Light (const Graphics::Color& color, float intensity) noexcept;

		public:
			const Graphics::Color& getColor() const noexcept;

			float getIntensity() const noexcept;
	};
}

#endif //RAY_TRACER_LIGHT_HPP
