/*
 * @author  thomas
 * @date    09/11/18
 * Copyright (c) 2018 thomas
 */

#ifndef RAY_TRACER_GLOBALLIGHT_HPP
#define RAY_TRACER_GLOBALLIGHT_HPP

#include <Lights/Light.hpp>

namespace RayTracer::Lights
{
	class GlobalLight: public Light
	{
		private:

		public:
			explicit GlobalLight(const Json::Value& jsonLight);

			GlobalLight(const Graphics::Color& color, float intensity) noexcept;
	};
}

#endif //RAY_TRACER_GLOBALLIGHT_HPP
