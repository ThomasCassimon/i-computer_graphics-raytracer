/*
 * @author  thomas
 * @date    12/11/18
 * Copyright (c) 2018 thomas
 */

#include "PointLight.hpp"

namespace RayTracer::Lights
{
	PointLight::PointLight(const Json::Value& jsonLight): Light(jsonLight)
	{
		const Json::Value jsonPosition = jsonLight[std::string(POSITION_KEY)];

		if (jsonPosition.isNull())
		{
			std::stringstream stream;
			stream << "JSON point light didn't contain position.\n" << jsonLight;
			throw std::runtime_error(stream.str());
		}

		this->position = Math::vec3(jsonPosition);
	}

	PointLight::PointLight(const Graphics::Color& color, float intensity, const Math::vec3& position) noexcept: Light(color, intensity), position(position)
	{
	}

	const Math::vec3& PointLight::getPosition() const noexcept
	{
		return this->position;
	}
}