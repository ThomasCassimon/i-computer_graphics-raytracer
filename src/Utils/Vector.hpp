/*
 * @author thomas
 * @date   12/10/18
 * @file   Vector.hpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#ifndef RAY_TRACER_UTILS_VECTOR_HPP
#define RAY_TRACER_UTILS_VECTOR_HPP

#include <iostream>
#include <vector>

namespace RayTracer::Utils
{
	template <typename T>
	std::vector<T> merge (const std::vector<T>& v1, const std::vector<T>& v2)
	{
		std::vector<T> result = std::vector<T> (v1.size() + v2.size());

		std::copy(v1.begin(), v1.end(), result.begin());
		std::copy(v2.begin(), v2.end(), result.begin() + v1.size());

		return result;
	}

	template <typename T>
	std::vector<std::vector<T>> split (const std::vector<T>& vector, std::size_t parts)
	{
		const std::size_t divisbleSize = (vector.size() - (vector.size() % parts));
		const std::size_t divisionSize = divisbleSize / parts;

		std::vector<std::vector<T>> result = std::vector<std::vector<T>> (parts);

		for (int i = 0; i < (parts - 1); i++)
		{
			result[i] = std::vector<T>(divisionSize);
		}

		result[(parts - 1)] = std::vector<T>(divisionSize + (vector.size() % parts));

		for (std::size_t i = 0; i < parts; i++)
		{
			for (std::size_t j = 0; j < divisionSize; j++)
			{
				const std::size_t index = (i * divisionSize) + j;
				result[i][j] = vector[index];
			}
		}

		const std::size_t remainderOffset = vector.size() - (vector.size() % parts);
		for (std::size_t i = remainderOffset; i < vector.size(); i++)
		{
			result[(parts - 1)][divisionSize + (i - remainderOffset)] = vector[i];
		}

		return result;
	}
}


#endif //RAY_TRACER_UTILS_VECTOR_HPP
