/*
 * Created by thomas on 4/19/18.
 *
 * Copyright 2018 Thomas Cassimon
 */
#ifndef WATSON_STRING_HPP
#define WATSON_STRING_HPP

#include <algorithm>
#include <array>
#include <limits>
#include <stdexcept>
#include <string>
#include <vector>

#include <cmath>

namespace RayTracer::Utils
{
	class String
	{
		public:
			#ifndef WIN32
				constexpr static std::string_view EOL = "\n";
			#else
				constexpr static std::string_view EOL = "\r\n";
			#endif

			static std::vector<std::string> split(const std::string& str, const std::string& delim);

			static std::string trim(const std::string& str, const std::string& whitespace = " \t\n\r\f\v");

			static std::string replace(const std::string& input, const std::string& oldStr, const std::string& newStr);

			static std::string merge (const std::vector<std::string>& strings);

			static std::string toLowerCase (const std::string& string);

			static std::string toUpperCase (const std::string& string);

			static std::pair<double, std::string> getOrderOfMagnitude (double d) noexcept;
	};
}

#endif //WATSON_STRING_HPP
