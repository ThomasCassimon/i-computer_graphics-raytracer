/*
 * Created by thomas on 4/19/18.
 *
 * Copyright 2018 Thomas Cassimon
 */

#include "String.hpp"

#include <iostream>

namespace RayTracer::Utils
{
	std::vector<std::string> String::split(const std::string& str, const std::string& delim)
	{
		std::vector<std::string> tokens = std::vector<std::string>();
		std::string strCopy = str;

		size_t pos = 0;
		std::string token;

		while ((pos = strCopy.find(delim)) != std::string::npos)
		{
			token = strCopy.substr(0, pos);
			strCopy.erase(0, pos + delim.length());

			tokens.push_back(token);
		}

		if (strCopy.length() > 0)
		{
			tokens.push_back(strCopy);
		}

		return tokens;
	}

	std::string String::trim(const std::string& str, const std::string& whitespace)
	{
		std::string copy = str;
		copy.erase(0, copy.find_first_not_of(whitespace));
		copy.erase(copy.find_last_not_of(whitespace) + 1);

		return copy;
	}

	std::string String::replace(const std::string& input, const std::string& oldStr, const std::string& newStr)
	{
		std::string result = input;

		std::size_t pos = 0;

		while ((pos = result.find(oldStr, pos)) != std::string::npos)
		{
			result.replace(pos, oldStr.length(), newStr);
			pos += newStr.length();
		}

		return result;
	}

	std::string String::merge(const std::vector<std::string>& strings)
	{
		std::string result;

		for (std::size_t i = 0; i < strings.size(); i++)
		{
			result += "\"";
			result += strings[i];
			result += "\"";

			if ((i + 1) != strings.size())
			{
				result += ", ";
			}
		}

		return result;
	}

	std::string String::toLowerCase(const std::string& string)
	{
		std::string lowercase = string;

		std::transform(lowercase.begin(), lowercase.end(), lowercase.begin(), ::tolower);

		return lowercase;
	}

	std::string String::toUpperCase(const std::string& string)
	{
		std::string uppercase = string;

		std::transform(uppercase.begin(), uppercase.end(), uppercase.begin(), ::toupper);

		return uppercase;
	}

	std::pair<double, std::string> String::getOrderOfMagnitude(double d) noexcept
	{
		if (std::fabs(d) == std::numeric_limits<double>::infinity())
		{
			return std::make_pair(d, "");
		}

		constexpr std::array<std::string_view, 6> PREPOSITIONS = {"", "kilo", "Mega", "Giga", "Tera", "Peta"};

		int i = 0;

		while (d > 1000.0)
		{
			d /= 1000.0;
			i++;
		}

		return std::make_pair(d, std::string(PREPOSITIONS[i]));
	}
}