/*
 * @author  thomas
 * @date    23/11/18
 * Copyright (c) 2018 thomas
 */

#include "SceneParser.hpp"
#include "Objects/COLLADA/Parser.hpp"
#include "Objects/Cube.hpp"
#include "Objects/Cylinder.hpp"

namespace RayTracer
{
	bool SceneParser::isAmbientEnabled(const Json::Value& jsonGeneral) const
	{
		const std::string AMBIENT_KEY = "ambient";

		const Json::Value jsonAmbient = jsonGeneral[AMBIENT_KEY];

		if (!jsonAmbient.isNull())
		{

			return jsonAmbient.asBool();
		}
		else
		{
			throw std::runtime_error("Failed to determine ambient setting.");
		}
	}

	bool SceneParser::isDiffuseEnabled(const Json::Value& jsonGeneral) const
	{
		const std::string DIFFUSE_KEY = "diffuse";

		const Json::Value jsonDiffuse = jsonGeneral[DIFFUSE_KEY];

		if (!jsonDiffuse.isNull())
		{

			return jsonDiffuse.asBool();
		}
		else
		{
			throw std::runtime_error("Failed to determine ambient setting.");
		}
	}

	bool SceneParser::isSpecularEnabled(const Json::Value& jsonGeneral) const
	{
		const std::string SPECULAR_KEY = "specular";

		const Json::Value jsonSpecular = jsonGeneral[SPECULAR_KEY];

		if (!jsonSpecular.isNull())
		{

			return jsonSpecular.asBool();
		}
		else
		{
			throw std::runtime_error("Failed to determine ambient setting.");
		}
	}

	long SceneParser::getNumThreads(const Json::Value& jsonGeneral) const
	{
		const std::string CONCURRENCY_KEY = "concurrency";

		const Json::Value jsonConcurrency = jsonGeneral[CONCURRENCY_KEY];

		if (!jsonConcurrency.isNull())
		{
			long numThreads = jsonConcurrency.asLargestInt();
			numThreads = numThreads == -1 ? std::thread::hardware_concurrency() : numThreads;

			return numThreads;
		}
		else
		{
			throw std::runtime_error("Failed to determine concurrency.");
		}
	}

	int SceneParser::getReflectionDepth(const Json::Value& jsonGeneral) const
	{
		const std::string MAX_REFLECTIONS_KEY = "max_recursion";

		const Json::Value jsonReflections = jsonGeneral[MAX_REFLECTIONS_KEY];

		if (!jsonReflections.isNull())
		{
			int numReflections = jsonReflections.asInt();
			numReflections = numReflections == -1 ? 4 : numReflections;

			return numReflections;
		}
		else
		{
			throw std::runtime_error("Failed to determine maximum number of reflections.");
		}
	}

	Shaders::ShaderModel SceneParser::getShaderModel(const Json::Value& generalHeader) const
	{
		const std::string LIGHTING_MODEL_KEY = "lighting_model";
		const std::string BLINN_PHONG_MODEL = "Blinn-Phong";
		const std::string COOK_TORRANCE_MODEL = "Cook-Torrance";

		if (!generalHeader[LIGHTING_MODEL_KEY].isNull())
		{
			if (generalHeader[LIGHTING_MODEL_KEY] == BLINN_PHONG_MODEL)
			{
				return Shaders::ShaderModel::BLINN_PHONG;
			}
			else if (generalHeader[LIGHTING_MODEL_KEY] == COOK_TORRANCE_MODEL)
			{
				return Shaders::ShaderModel::COOK_TORRANCE;
			}
			else
			{
				throw std::runtime_error("Failed to parse lighting model, got \"" + generalHeader[LIGHTING_MODEL_KEY].asString() + "\", possible values are [\"" + BLINN_PHONG_MODEL + "\", \"" + COOK_TORRANCE_MODEL + "\"].");
			}
		}
		else
		{
			throw std::runtime_error("Failed to find lighting model, possible values are [\"" + BLINN_PHONG_MODEL + "\", \"" + COOK_TORRANCE_MODEL + "\"].");
		}
	}

	void SceneParser::parseGlobalLights(RayTracer& rayTracer, Json::Value& globalLightList) const
	{
		if (globalLightList.isArray())
		{
			for (Json::ArrayIndex i = 0; i != globalLightList.size(); ++i)
			{
				rayTracer.addGlobalLight(Lights::GlobalLight(globalLightList[i]));
			}
		}
		else
		{
			throw std::runtime_error("\"global_lights\" wasn't an array.");
		}
	}

	void SceneParser::parsePointLights(RayTracer& rayTracer, Json::Value& pointLightList) const
	{
		if (pointLightList.isArray())
		{
			for (Json::ArrayIndex i = 0; i != pointLightList.size(); ++i)
			{
				rayTracer.addPointLight(Lights::PointLight(pointLightList[i]));
			}
		}
		else
		{
			throw std::runtime_error("\"point_lights\" wasn't an array.");
		}
	}

	void SceneParser::parseObjectList(RayTracer& rayTracer, Json::Value& objectList)
	{
		const std::string OBJECT_TYPE_KEY = "type";
		const std::string SPHERE_TYPE = "sphere";
		const std::string TRIANGLE_TYPE = "triangle";
		const std::string QUAD_TYPE = "quad";
		const std::string MESH_TYPE = "mesh";
		const std::string CUBE_TYPE = "cube";
		const std::string CYLINDER_TYPE = "cylinder";

		if (objectList.isArray())
		{
			for (Json::ArrayIndex i = 0; i != objectList.size(); ++i)
			{
				Json::Value jsonObject = objectList[i];
				Json::Value jsonObjectType = jsonObject[OBJECT_TYPE_KEY];

				std::shared_ptr<Objects::Object> objPtr = nullptr;

				if (!jsonObjectType.isNull())
				{
					if (jsonObjectType.asString() == SPHERE_TYPE)
					{
						Objects::Sphere sphere (jsonObject);
						objPtr = std::make_shared<Objects::Sphere>(sphere);
					}
					else if (jsonObjectType.asString() == QUAD_TYPE)
					{
						Objects::Quad quad (jsonObject);
						objPtr = std::make_shared<Objects::Quad>(quad);
					}
					else if (jsonObjectType.asString() == TRIANGLE_TYPE)
					{
						Objects::Triangle triangle (jsonObject);
						objPtr = std::make_shared<Objects::Triangle>(triangle);
					}
					else if (jsonObjectType.asString() == MESH_TYPE)
					{
						Objects::Mesh mesh (jsonObject);
						objPtr = std::make_shared<Objects::Mesh>(mesh);
					}
					else if (jsonObjectType.asString() == CUBE_TYPE)
					{
						Objects::Cube cube (jsonObject);
						objPtr = std::make_shared<Objects::Cube>(cube);
					}
					else if (jsonObjectType.asString() == CYLINDER_TYPE)
					{
						Objects::Cylinder cylinder (jsonObject);
						objPtr = std::make_shared<Objects::Cylinder>(cylinder);
					}
					else
					{
						std::cerr << "Object has unsupported type field \"" + jsonObjectType.asString() + "\", ignoring." << std::endl;
					}

					if (objPtr != nullptr)
					{
						const Json::Value& jsonTransforms = jsonObject["transforms"];

						if (!jsonTransforms.isNull())
						{
							Math::TransformFactory factory;

							for (const Json::Value& jsonTransform : jsonTransforms)
							{
								objPtr->transform(factory.parse(jsonTransform));
							}
						}

						rayTracer.addObject(objPtr);
					}
				}
				else
				{
					std::cerr << "Object didn't have a type field, ignoring." << std::endl;
				}
			}
		}
		else
		{
			throw std::runtime_error("\"objects\" wasn't an array.");
		}
	}

	SceneParser::SceneParser(): GENERAL_HEADER("general"), CAMERA_HEADER("camera"), GLOBAL_LIGHT_HEADER("global_lights"), POINT_LIGHT_HEADER("point_lights"), OBJECT_HEADER("objects")
	{
	}

	RayTracer SceneParser::parse(const std::string& scenefile)
	{
		const std::string BACKGROUND_KEY = "background";

		Json::Value root;

		std::ifstream filestream (scenefile, std::ios::binary);

		if (!filestream.is_open())
		{
			throw std::runtime_error("Failed to open scene file \"" + scenefile + "\".");
		}

		filestream >> root;

		Json::Value jsonGeneral = root[GENERAL_HEADER];
		Json::Value jsonCamera = root[CAMERA_HEADER];
		Json::Value jsonGlobalLights = root[GLOBAL_LIGHT_HEADER];
		Json::Value jsonPointLights = root[POINT_LIGHT_HEADER];
		Json::Value jsonObjects = root[OBJECT_HEADER];

		if (jsonGeneral.isNull())
		{
			throw std::runtime_error("Failed to find \"general\" object.");
		}

		if (jsonCamera.isNull())
		{
			throw std::runtime_error("Failed to find \"camera\" object.");
		}

		Shaders::ShaderModel shaderModel = this->getShaderModel(jsonGeneral);
		int reflectionDepth = this->getReflectionDepth(jsonGeneral);
		long numThreads = this->getNumThreads(jsonGeneral);
		Graphics::Color background (jsonGeneral[BACKGROUND_KEY]);

		RayTracer rayTracer (Camera(jsonCamera), shaderModel, reflectionDepth, background, numThreads);

		if (!this->isAmbientEnabled(jsonGeneral))
		{
			rayTracer.disableAmbient();
		}

		if (!this->isDiffuseEnabled(jsonGeneral))
		{
			rayTracer.disableDiffuse();
		}

		if (!this->isSpecularEnabled(jsonGeneral))
		{
			rayTracer.disableSpecular();
		}

		if (!jsonGlobalLights.isNull())
		{
			this->parseGlobalLights(rayTracer, jsonGlobalLights);
		}

		if (!jsonPointLights.isNull())
		{
			this->parsePointLights(rayTracer, jsonPointLights);
		}

		if (!jsonObjects.isNull())
		{
			this->parseObjectList(rayTracer, jsonObjects);
		}

		return rayTracer;
	}
}