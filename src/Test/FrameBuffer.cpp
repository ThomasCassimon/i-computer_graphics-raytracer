/*
 * @author thomas
 * @date   21/10/18
 * @file   FrameBuffer.cpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#include <Catch/Catch.hpp>
#include <Graphics/FrameBuffer.hpp>
#include <Math/vec2.hpp>

using namespace RayTracer;

TEST_CASE("Setting some pixels to white on a black buffer", "[framebuffer][set]")
{
	Graphics::FrameBuffer buffer = Graphics::FrameBuffer(Math::ivec2(16, 9));

	const std::array<Math::ivec2, 8> INDICES_TO_SET = {
			Math::ivec2(3,3),
			Math::ivec2(11,7),
			Math::ivec2(0,5),
			Math::ivec2(1,2),
			Math::ivec2(8,5),
			Math::ivec2(1,13),
			Math::ivec2(8,15),
			Math::ivec2(7,14),
	};

	for (const Math::ivec2& index: INDICES_TO_SET)
	{
		buffer(index.x, index.y) = Graphics::Color(1.0f, 1.0f, 1.0f, 1.0f);
	}

	for (int i = 0; i < 16; i++)
	{
		for (int j = 0; j < 9; j++)
		{
			const Math::ivec2 index = Math::ivec2(j, i);

			if (std::find(INDICES_TO_SET.begin(), INDICES_TO_SET.end(), index) == INDICES_TO_SET.end())
			{
				CHECK(buffer(index.x, index.y) == Graphics::Color(0.0f, 0.0f, 0.0f, 1.0f));
			}
			else
			{
				CHECK(buffer(index.x, index.y) == Graphics::Color(1.0f, 1.0f, 1.0f, 1.0f));
			}
		}
	}
}