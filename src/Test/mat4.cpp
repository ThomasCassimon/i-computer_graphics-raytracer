/*
 * Created by thomas on 28/09/18.
 */

#include <Catch/Catch.hpp>

#include <Math/mat4.hpp>
#include <Math/vec4.hpp>

using namespace RayTracer;

TEST_CASE("Identity matrix creation", "[mat4][identity][constructor]")
{
	Math::mat4 matrix = Math::identity();

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			if (i == j)
			{
				CHECK(matrix(i,j) == Approx(1.0f));
			}
			else
			{
				CHECK(matrix(i,j) == Approx(0.0f));
			}
		}
	}
}

TEST_CASE("mat4 + mat4", "[mat4][sum][add]")
{
	Math::mat4 mat1;
	Math::mat4 mat2;

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			const int val1 = (i * 4) + j;
			const int val2 = (j * 4) + i;

			mat1(i, j) = val1;
			mat2(i, j) = val2;
		}
	}

	Math::mat4 matrix = mat1 + mat2;

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			const int val1 = (i * 4) + j;
			const int val2 = (j * 4) + i;
			const int sum = val1 + val2;
			CHECK(matrix(i, j) == Approx(sum));
		}
	}
}

TEST_CASE("mat4 * vec4", "[mat4][vec4][multiplication][product]")
{
	Math::mat4 mat;
	Math::vec4 vec;

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			const int val = (i * 4) + j;

			mat(i, j) = val;
		}
	}

	vec.x = 1.0f;
	vec.y = 3.0f;
	vec.z = 2.0f;
	vec.w = 5.0f;

	Math::vec4 result = mat * vec;
	Math::vec4 EXPECTED = Math::vec4(
			(0.0f * 1.0f) + (1.0f * 3.0f) + (2.0f * 2.0f) + (3.0f * 5.0f),
			(4.0f * 1.0f) + (5.0f * 3.0f) + (6.0f * 2.0f) + (7.0f * 5.0f),
			(8.0f * 1.0f) + (9.0f * 3.0f) + (10.0f * 2.0f) + (11.0f * 5.0f),
			(12.0f * 1.0f) + (13.0f * 3.0f) + (14.0f * 2.0f) + (15.0f * 5.0f));

	CHECK(result.x == Approx(EXPECTED.x));
	CHECK(result.y == Approx(EXPECTED.y));
	CHECK(result.z == Approx(EXPECTED.z));
	CHECK(result.w == Approx(EXPECTED.w));
}

TEST_CASE("mat4 * mat4", "[mat4][multiplication][product]")
{
	Math::mat4 mat1;
	Math::mat4 mat2;

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			const int val1 = (i * 4) + j;
			const int val2 = (j * 4) + i;

			mat1(i, j) = val1;
			mat2(i, j) = val2;
		}
	}

	Math::mat4 matrix = mat1 * mat2;
	Math::mat4 EXPECTED;

	EXPECTED(0,0) = 14.0f;
	EXPECTED(0,1) = 38.0f;
	EXPECTED(0,2) = 62.0f;
	EXPECTED(0,3) = 86.0f;

	EXPECTED(1,0) = 38.0f;
	EXPECTED(1,1) = 126.0f;
	EXPECTED(1,2) = 214.0f;
	EXPECTED(1,3) = 302.0f;

	EXPECTED(2,0) = 62.0f;
	EXPECTED(2,1) = 214.0f;
	EXPECTED(2,2) = 366.0f;
	EXPECTED(2,3) = 518.0f;

	EXPECTED(3,0) = 86.0f;
	EXPECTED(3,1) = 302.0f;
	EXPECTED(3,2) = 518.0f;
	EXPECTED(3,3) = 734.0f;

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			CHECK(matrix(i, j) == Approx(EXPECTED(i,j)));
		}
	}
}