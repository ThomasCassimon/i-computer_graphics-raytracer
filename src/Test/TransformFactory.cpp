/*
 * @author thomas
 * @date   23/10/18
 * @file   TransformFactory.cpp
 *
 * Copyright 2018 Thomas Cassimon
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <Catch/Catch.hpp>

#include <Math/TransformFactory.hpp>

using namespace RayTracer;
/*
TEST_CASE("TransformFactory.translate", "[transform][factory][translate]")
{
	Math::vec3 translation (-3.0f, 2.0f, 0.0f);

	Math::TransformFactory factory;

	Math::Transformation transform (factory.translate(translation));
	Math::mat4 transform = transformPair.first;

	Math::mat4 EXPECTED (Math::vec4(1.0f, 0.0f, 0.0f, 0.0f), Math::vec4(0.0f, 1.0f, 0.0f, 0.0f), Math::vec4(0.0f, 0.0f, 1.0f, 0.0f), Math::vec4(translation, 1.0f));

	std::cout << to_string(EXPECTED) << std::endl;

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			CHECK(transform(i, j) == Approx(EXPECTED(i, j)));
		}
	}
}
*/