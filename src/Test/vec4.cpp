/*
 * Created by thomas on 28/09/18.
 */

#include <cmath>

#include <Catch/Catch.hpp>

#include <Math/vec4.hpp>

using namespace RayTracer;

TEST_CASE("vec4 + vec4", "[vec4][sum][add]")
{
	Math::vec4 v1;
	Math::vec4 v2;

	v1.x = 3.0f;
	v1.y = 0.0f;
	v1.z = 2.0f;
	v1.w = 5.0f;

	v2.x = 4.0f;
	v2.y = 1.0f;
	v2.z = 8.0f;
	v2.w = 3.0f;

	Math::vec4 sum = v1 + v2;
	Math::vec4 EXPECTED = Math::vec4(7.0f, 1.0f, 10.0f, 8.0f);

	CHECK(sum.x == Approx(EXPECTED.x));
	CHECK(sum.y == Approx(EXPECTED.y));
	CHECK(sum.z == Approx(EXPECTED.z));
	CHECK(sum.w == Approx(EXPECTED.w));
}

TEST_CASE("vec4 - vec4", "[vec4][difference][sub]")
{
	Math::vec4 v1;
	Math::vec4 v2;

	v1.x = 3.0f;
	v1.y = 0.0f;
	v1.z = 2.0f;
	v1.w = 5.0f;

	v2.x = 4.0f;
	v2.y = 1.0f;
	v2.z = 8.0f;
	v2.w = 3.0f;

	Math::vec4 sum = v1 - v2;
	Math::vec4 EXPECTED = Math::vec4(-1.0f, -1.0f, -6.0f, 2.0f);

	CHECK(sum.x == Approx(EXPECTED.x));
	CHECK(sum.y == Approx(EXPECTED.y));
	CHECK(sum.z == Approx(EXPECTED.z));
	CHECK(sum.w == Approx(EXPECTED.w));
}

TEST_CASE("float * vec4", "[vec4][multiplication][scalar][product]")
{
	Math::vec4 v;
	float coef = 2.0f;

	v.x = 3.0f;
	v.y = 0.0f;
	v.z = 2.0f;
	v.w = 5.0f;

	Math::vec4 product = coef * v;

	Math::vec4 EXPECTED = Math::vec4(6.0f, 0.0f, 4.0f, 10.0f);

	CHECK(product.x == Approx(EXPECTED.x));
	CHECK(product.y == Approx(EXPECTED.y));
	CHECK(product.z == Approx(EXPECTED.z));
	CHECK(product.w == Approx(EXPECTED.w));
}

TEST_CASE("vec4 * float", "[vec4][multiplication][scalar][product]")
{
	Math::vec4 v;
	float coef = 2.0f;

	v.x = 3.0f;
	v.y = 0.0f;
	v.z = 2.0f;
	v.w = 5.0f;

	Math::vec4 product = v * coef;

	Math::vec4 EXPECTED = Math::vec4(6.0f, 0.0f, 4.0f, 10.0f);

	CHECK(product.x == Approx(EXPECTED.x));
	CHECK(product.y == Approx(EXPECTED.y));
	CHECK(product.z == Approx(EXPECTED.z));
	CHECK(product.w == Approx(EXPECTED.w));
}

TEST_CASE("vec4 / float", "[vec4][division][scalar][quotient]")
{
	Math::vec4 v;
	float coef = 2.0f;

	v.x = 6.0f;
	v.y = 0.0f;
	v.z = 2.0f;
	v.w = 10.0f;

	Math::vec4 product = v / coef;

	Math::vec4 EXPECTED = Math::vec4(3.0f, 0.0f, 1.0f, 5.0f);

	CHECK(product.x == Approx(EXPECTED.x));
	CHECK(product.y == Approx(EXPECTED.y));
	CHECK(product.z == Approx(EXPECTED.z));
	CHECK(product.w == Approx(EXPECTED.w));
}

TEST_CASE("vec4.length()", "[vec4][norm][length]")
{
	Math::vec4 v;

	v.x = 6.0f;
	v.y = 0.0f;
	v.z = 2.0f;
	v.w = 10.0f;

	float length = v.length();
	float EXPECTED = std::sqrt(std::pow(6.0f, 2.0f) + std::pow(0.0f, 2.0f)  + std::pow(2.0f, 2.0f) + std::pow(10.0f, 2.0f));

	CHECK(length == Approx(EXPECTED));
}

TEST_CASE("normalize(vec4)", "[vec4][norm][normalize]")
{
	Math::vec4 v;

	v.x = 6.0f;
	v.y = 0.0f;
	v.z = 2.0f;
	v.w = 10.0f;

	Math::vec4 normalized = normalize(v);

	float length = std::sqrt(std::pow(6.0f, 2.0f) + std::pow(0.0f, 2.0f) + std::pow(2.0f, 2.0f) + std::pow(10.0f, 2.0f));
	Math::vec4 EXPECTED = Math::vec4(6.0f / length, 0.0f / length, 2.0f / length, 10.0f / length);

	CHECK(normalized.x == Approx(EXPECTED.x));
	CHECK(normalized.y == Approx(EXPECTED.y));
	CHECK(normalized.z == Approx(EXPECTED.z));
	CHECK(normalized.w == Approx(EXPECTED.w));
}

TEST_CASE("vec4.normalize()", "[vec4][norm][normalize]")
{
	Math::vec4 v;

	v.x = 6.0f;
	v.y = 0.0f;
	v.z = 2.0f;
	v.w = 10.0f;

	Math::vec4 normalized = v.normalize();

	float length = std::sqrt(std::pow(6.0f, 2.0f) + std::pow(0.0f, 2.0f) + std::pow(2.0f, 2.0f) + std::pow(10.0f, 2.0f));
	Math::vec4 EXPECTED = Math::vec4(6.0f / length, 0.0f / length, 2.0f / length, 10.0f / length);

	CHECK(normalized.x == Approx(EXPECTED.x));
	CHECK(normalized.y == Approx(EXPECTED.y));
	CHECK(normalized.z == Approx(EXPECTED.z));
	CHECK(normalized.w == Approx(EXPECTED.w));
}

TEST_CASE("vec4 . vec4", "[vec4][dot][dotproduct][product]")
{
	Math::vec4 v1;
	Math::vec4 v2;

	v1.x = 2.0f;
	v1.y = 3.0f;
	v1.z = 1.0f;
	v1.w = 4.0f;

	v2.x = 0.0f;
	v2.y = 4.0f;
	v2.z = -1.0f;
	v2.w = 1.0f;


	float dotprod = Math::dot(v1, v2);

	float EXPECTED = (2.0f  * 0.0f) + (3.0f * 4.0f) + (1.0f * (-1.0f)) + (4.0f * 1.0f);

	CHECK(dotprod == Approx(EXPECTED));
}

TEST_CASE("vec4(int)", "[vec4][access]")
{
	Math::vec4 v;

	v.x = 2.0f;
	v.y = 3.0f;
	v.z = 1.0f;
	v.w = 4.0f;

	CHECK(v(0) == Approx(2.0f));
	CHECK(v(1) == Approx(3.0f));
	CHECK(v(2) == Approx(1.0f));
	CHECK(v(3) == Approx(4.0f));
}