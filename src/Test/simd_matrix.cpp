/*
 * @author  thomas
 * @date    27/11/18
 * Copyright (c) 2018 thomas
 */

#include <Catch/Catch.hpp>

#define USE_SIMD 1
#include <Math/mat4.hpp>

using namespace RayTracer;

TEST_CASE("matrix + matrix", "[simd_matrix][sum][add]")
{
	Math::mat4 mat1;
	Math::mat4 mat2;

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			const int val1 = (i * 4) + j;
			const int val2 = (j * 4) + i;

			mat1(i, j) = val1;
			mat2(i, j) = val2;
		}
	}

	Math::mat4 result = mat1 + mat2;

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			const int val1 = (i * 4) + j;
			const int val2 = (j * 4) + i;
			const int sum  = val1 + val2;
			CHECK(result(i, j) == Approx(sum));
		}
	}
}

TEST_CASE("matrix * vec4", "[simd_matrix][vec4][multiplication][product]")
{
	Math::mat4 mat;
	Math::simd_vec4 vec;

	// First test case, tested by writing out all operations
	constexpr std::array<float, 16> TEST_CASE_1 =
		                                {0,  1,  2,  3,
		                                 4,  5,  6,  7,
		                                 8,  9,  10, 11,
		                                 12, 13, 14, 15};

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			const int arrayIdx = (i * 4) + j;

			mat(i, j) = TEST_CASE_1[arrayIdx];
		}
	}

	vec = Math::simd_vec4(1.0f, 3.0f, 2.0f, 5.0f);

	Math::simd_vec4 result   = mat * vec;
	Math::simd_vec4 EXPECTED_1 = Math::simd_vec4(
		(0.0f * 1.0f)  + (1.0f * 3.0f)  + (2.0f * 2.0f)  + (3.0f * 5.0f),
		(4.0f * 1.0f)  + (5.0f * 3.0f)  + (6.0f * 2.0f)  + (7.0f * 5.0f),
		(8.0f * 1.0f)  + (9.0f * 3.0f)  + (10.0f * 2.0f) + (11.0f * 5.0f),
		(12.0f * 1.0f) + (13.0f * 3.0f) + (14.0f * 2.0f) + (15.0f * 5.0f));

	CHECK(result.x == Approx(EXPECTED_1.x));
	CHECK(result.y == Approx(EXPECTED_1.y));
	CHECK(result.z == Approx(EXPECTED_1.z));
	CHECK(result.w == Approx(EXPECTED_1.w));

	// Second test case, checked with Wolfram Alpha (https://www.wolframalpha.com/input/?i=%7B%7B1,+7,+23,+-5%7D,%7B0,+-9,+-5,+1%7D,%7B2,+6,+-3,+8%7D,%7B-1,+8,+11,+-5%7D%7D+*+%7B-7,+5,+-3,+1%7D)
	constexpr std::array<float, 16> TEST_CASE_2 =
		                                {1, 7, 23, -5,
		                                 0, -9, -5, 1,
		                                 2, 6, -3, 8,
		                                 -1, 8, 11, -5};

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			const int arrayIdx = (i * 4) + j;
			mat(i, j) = TEST_CASE_2[arrayIdx];
		}
	}

	vec = Math::simd_vec4(-7, 5, -3, 1);
	Math::simd_vec4 EXPECTED_2 = Math::simd_vec4(-46, -29, 33, 9);
	result   = mat * vec;

	CHECK(result.x == Approx(EXPECTED_2.x));
	CHECK(result.y == Approx(EXPECTED_2.y));
	CHECK(result.z == Approx(EXPECTED_2.z));
	CHECK(result.w == Approx(EXPECTED_2.w));
}

TEST_CASE("matrix * matrix", "[simd_matrix][multiplication][product]")
{
	Math::mat4 mat1;
	Math::mat4 mat2;

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			const int val1 = (i * 4) + j;
			const int val2 = (j * 4) + i;

			mat1(i, j) = val1;
			mat2(i, j) = val2;
		}
	}

	Math::simd_matrix matrix = mat1 * mat2;
	Math::simd_matrix EXPECTED;

	EXPECTED(0,0) = 14.0f;
	EXPECTED(0,1) = 38.0f;
	EXPECTED(0,2) = 62.0f;
	EXPECTED(0,3) = 86.0f;

	EXPECTED(1,0) = 38.0f;
	EXPECTED(1,1) = 126.0f;
	EXPECTED(1,2) = 214.0f;
	EXPECTED(1,3) = 302.0f;

	EXPECTED(2,0) = 62.0f;
	EXPECTED(2,1) = 214.0f;
	EXPECTED(2,2) = 366.0f;
	EXPECTED(2,3) = 518.0f;

	EXPECTED(3,0) = 86.0f;
	EXPECTED(3,1) = 302.0f;
	EXPECTED(3,2) = 518.0f;
	EXPECTED(3,3) = 734.0f;

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			CHECK(matrix(i, j) == Approx(EXPECTED(i,j)));
		}
	}
}