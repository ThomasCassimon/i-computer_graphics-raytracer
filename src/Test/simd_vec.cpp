/*
 * @author thomas
 * @date   22/10/18
 * @file   simd_vec.cpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#include <Catch/Catch.hpp>

#include <Math/simd/vector.hpp>

using namespace RayTracer;

TEST_CASE("vector<2>()", "[simd_vec][operator][access]")
{
	Math::vector<2> v (3.0f, 0.0f);

	CHECK(v.x == Approx(3.0f));
	CHECK(v.y == Approx(0.0f));
}

TEST_CASE("vector<3>()", "[simd_vec][operator][access]")
{
	Math::vector<3> v (3.0f, 0.0f, -2.0f);

	CHECK(v.x == Approx(3.0f));
	CHECK(v.y == Approx(0.0f));
	CHECK(v.z == Approx(-2.0f));
}

TEST_CASE("vector<4>()", "[simd_vec][operator][access]")
{
	Math::vector<4> v (3.0f, 0.0f, -2.0f, 1.0f);

	CHECK(v.x == Approx(3.0f));
	CHECK(v.y == Approx(0.0f));
	CHECK(v.z == Approx(-2.0f));
	CHECK(v.w == Approx(1.0f));
}


TEST_CASE("vector<3>.length", "[simd_vec][length][norm]")
{
	Math::vector<3> v (3.0f, 0.0f, 2.0f);

	float length = v.length();
	float EXPECTED = std::sqrt(std::pow(3.0f, 2.0f) + std::pow(0.0f, 2.0f) + std::pow(2.0f, 2.0f));

	CHECK(length == Approx(EXPECTED));
}

TEST_CASE("vector<3>.normalize", "[simd_vec][normalize][norm]")
{
	Math::vector<3> v (3.0f, 0.0f, 2.0f);

	Math::vector<3> normalized = v.normalize();
	float length = std::sqrt(std::pow(3.0f, 2.0f) + std::pow(0.0f, 2.0f) + std::pow(2.0f, 2.0f));
	Math::vector<3> EXPECTED (3.0f / length, 0.0f / length, 2.0f / length);

	CHECK(normalized.x == Approx(EXPECTED.x));
	CHECK(normalized.y == Approx(EXPECTED.y));
	CHECK(normalized.z == Approx(EXPECTED.z));
}

TEST_CASE("vector<3> - vector<3>", "[simd_vec][subtract][difference][sub]")
{
	Math::vector<3> left(10.0f, 5.0f, -2.0f);
	Math::vector<3> right(7.0f, -1.0f, 4.0f);

	Math::vector<3> diff = left - right;
	Math::vector<3> EXPECTED (3.0f, 6.0f, -6.0f);

	CHECK(diff.x == Approx(EXPECTED.x));
	CHECK(diff.y == Approx(EXPECTED.y));
	CHECK(diff.z == Approx(EXPECTED.z));
}


TEST_CASE("- vector<3>", "[simd_vec][subtract][sign]")
{
	Math::vector<3> v (3.0f, 0.0f, 2.0f);

	Math::vector<3> reversed = -v;
	Math::vector<3> EXPECTED = Math::vector<3>(-3.0f, -0.0f, -2.0f);

	CHECK(reversed.x == Approx(EXPECTED.x));
	CHECK(reversed.y == Approx(EXPECTED.y));
	CHECK(reversed.z == Approx(EXPECTED.z));
}

TEST_CASE("vector<3> + vector<3>", "[simd_vec][addition][sum][add]")
{
	Math::vector<3> left(10.0f, 5.0f, -2.0f);
	Math::vector<3> right(7.0f, -1.0f, 4.0f);

	Math::vector<3> diff = left + right;
	Math::vector<3> EXPECTED (17.0f, 4.0f, 2.0f);

	CHECK(diff.x == Approx(EXPECTED.x));
	CHECK(diff.y == Approx(EXPECTED.y));
	CHECK(diff.z == Approx(EXPECTED.z));
}

TEST_CASE("float * vector<3>", "[simd_vec][float][multiplication][mul]")
{
	float factor = 2.0f;
	Math::vector<3> vec(10.0f, 5.0f, -2.0f);

	Math::vector<3> result = factor * vec;
	Math::vector<3> EXPECTED (20.0f, 10.0f, -4.0f);

	CHECK(result.x == Approx(EXPECTED.x));
	CHECK(result.y == Approx(EXPECTED.y));
	CHECK(result.z == Approx(EXPECTED.z));

}

TEST_CASE("vector<3> * float", "[simd_vec][float][multiplication][mul]")
{
	float factor = 2.0f;
	Math::vector<3> vec(10.0f, 5.0f, -2.0f);

	Math::vector<3> result = vec * factor;
	Math::vector<3> EXPECTED (20.0f, 10.0f, -4.0f);

	CHECK(result.x == Approx(EXPECTED.x));
	CHECK(result.y == Approx(EXPECTED.y));
	CHECK(result.z == Approx(EXPECTED.z));
}

TEST_CASE("vector<3> / float", "[simd_vec][float][division][div]")
{
	float factor = 2.0f;
	Math::vector<3> vec(10.0f, 5.0f, -2.0f);

	Math::vector<3> result = vec / factor;
	Math::vector<3> EXPECTED (5.0f, 2.5f, -1.0f);

	CHECK(result.x == Approx(EXPECTED.x));
	CHECK(result.y == Approx(EXPECTED.y));
	CHECK(result.z == Approx(EXPECTED.z));
}

TEST_CASE("float / vector<3>", "[simd_vec][float][division][div]")
{
	float factor = 2.0f;
	Math::vector<3> vec(10.0f, 5.0f, -2.0f);

	Math::vector<3> result = factor / vec;
	Math::vector<3> EXPECTED (0.2f, 0.4f, -1.0f);

	CHECK(result.x == Approx(EXPECTED.x));
	CHECK(result.y == Approx(EXPECTED.y));
	CHECK(result.z == Approx(EXPECTED.z));
}

TEST_CASE("vector<3> . vector<3>", "[simd_vec][dot][product]")
{
	Math::vector<3> v1 (3.0f, 0.0f, 2.0f);
	Math::vector<3> v2 (4.0f, 1.0f, 8.0f);

	float dotprod = Math::dot(v1, v2);
	float EXPECTED = (3.0f * 4.0f) + (0.0f * 1.0f) + (2.0f * 8.0f);

	CHECK(dotprod == Approx(EXPECTED));

	v1 = Math::vector<3>(1.0f, 2.0f, 3.0f);
	v2 = Math::vector<3>(4.0f, -5.0f, 6.0f);

	dotprod = Math::dot(v1, v2);
	EXPECTED = (1.0f * 4.0f) + (2.0f * -5.0f) + (3.0f * 6.0f);

	CHECK(dotprod == Approx(EXPECTED));

	v1 = Math::vector<3>(9.0f, 2.0f, 7.0f);
	v2 = Math::vector<3>(4.0f, 8.0f, 10.0f);

	dotprod = Math::dot(v1, v2);
	EXPECTED = (9.0f * 4.0f) + (2.0f * 8.0f) + (7.0f * 10.0f);

	CHECK(dotprod == Approx(EXPECTED));
}

TEST_CASE("vector<3> x vector<3>", "[simd_vec][cross][multiplication]")
{
	Math::vector<3> v1 (3.0f, 0.0f, 2.0f);
	Math::vector<3> v2 (4.0f, 1.0f, 8.0f);

	Math::vector<3> cross = Math::cross(v1, v2);
	Math::vector<3> EXPECTED (-2.0f, -16.0f, 3.0f);

	CHECK(cross.x == Approx(EXPECTED.x));
	CHECK(cross.y == Approx(EXPECTED.y));
	CHECK(cross.z == Approx(EXPECTED.z));
}

TEST_CASE("vector<3> * vector<3>", "[simd_vec][element-wise][multiplication]")
{
	Math::vector<3> v1 (3.0f, 0.0f, 2.0f);
	Math::vector<3> v2 (4.0f, 1.0f, -8.0f);

	Math::vector<3> mult = v1 * v2;
	Math::vector<3> EXPECTED (12.0f, 0.0f, -16.0f);

	CHECK(mult.x == Approx(EXPECTED.x));
	CHECK(mult.y == Approx(EXPECTED.y));
	CHECK(mult.z == Approx(EXPECTED.z));
}

TEST_CASE("vector<3> *= vector<3>", "[simd_vec][element-wise][multiplication]")
{
	Math::vector<3> v1 (3.0f, 0.0f, 2.0f);
	Math::vector<3> v2 (4.0f, 1.0f, -8.0f);

	v1 *= v2;
	Math::vector<3> EXPECTED (12.0f, 0.0f, -16.0f);

	CHECK(v1.x == Approx(EXPECTED.x));
	CHECK(v1.y == Approx(EXPECTED.y));
	CHECK(v1.z == Approx(EXPECTED.z));
}

TEST_CASE("abs(vector<3>)", "[simd_vec][absolute][value]")
{
	Math::vector<3> vec (-3.0f, 0.0f, 2.0f);

	Math::vector<3> abs = Math::abs(vec);

	Math::vector<3> EXPECTED (3.0f, 0.0f, 2.0f);

	CHECK(abs.x == Approx(EXPECTED.x));
	CHECK(abs.y == Approx(EXPECTED.y));
	CHECK(abs.z == Approx(EXPECTED.z));
}

TEST_CASE("vector<3>.swap", "[simd_vec][swap][shuffle]")
{
	GIVEN("Swapping X and Y")
	{
		Math::vector<3> vec(-3.0f, 0.0f, 2.0f);
		Math::vector<3> swap = vec.swap(0, 1);

		Math::vector<3> EXPECTED(0.0f, -3.0f, 2.0f);

		CHECK(swap.x == Approx(EXPECTED.x));
		CHECK(swap.y == Approx(EXPECTED.y));
		CHECK(swap.z == Approx(EXPECTED.z));
	}

	GIVEN("Swapping Y and X")
	{
		Math::vector<3> vec(-3.0f, 0.0f, 2.0f);
		Math::vector<3> swap = vec.swap(1, 0);

		Math::vector<3> EXPECTED(0.0f, -3.0f, 2.0f);

		CHECK(swap.x == Approx(EXPECTED.x));
		CHECK(swap.y == Approx(EXPECTED.y));
		CHECK(swap.z == Approx(EXPECTED.z));
	}

	GIVEN("Swapping X and Z")
	{
		Math::vector<3> vec(-3.0f, 0.0f, 2.0f);
		Math::vector<3> swap = vec.swap(0, 2);

		Math::vector<3> EXPECTED(2.0f, 0.0f, -3.0f);

		CHECK(swap.x == Approx(EXPECTED.x));
		CHECK(swap.y == Approx(EXPECTED.y));
		CHECK(swap.z == Approx(EXPECTED.z));
	}

	GIVEN("Swapping Z and X")
	{
		Math::vector<3> vec(-3.0f, 0.0f, 2.0f);
		Math::vector<3> swap = vec.swap(2, 0);

		Math::vector<3> EXPECTED(2.0f, 0.0f, -3.0f);

		CHECK(swap.x == Approx(EXPECTED.x));
		CHECK(swap.y == Approx(EXPECTED.y));
		CHECK(swap.z == Approx(EXPECTED.z));
	}

	GIVEN("Swapping Y and Z")
	{
		Math::vector<3> vec(-3.0f, 0.0f, 2.0f);
		Math::vector<3> swap = vec.swap(1, 2);

		Math::vector<3> EXPECTED(-3.0f, 2.0f, 0.0f);

		CHECK(swap.x == Approx(EXPECTED.x));
		CHECK(swap.y == Approx(EXPECTED.y));
		CHECK(swap.z == Approx(EXPECTED.z));
	}

	GIVEN("Swapping Z and Y")
	{
		Math::vector<3> vec(-3.0f, 0.0f, 2.0f);
		Math::vector<3> swap = vec.swap(2, 1);

		Math::vector<3> EXPECTED(-3.0f, 2.0f, 0.0f);

		CHECK(swap.x == Approx(EXPECTED.x));
		CHECK(swap.y == Approx(EXPECTED.y));
		CHECK(swap.z == Approx(EXPECTED.z));
	}
}

TEST_CASE("vector<4>.swap", "[simd_vec][swap][shuffle]")
{
	GIVEN("Swapping X and Y")
	{
		Math::vector<4> vec(-3.0f, 0.0f, 2.0f, 10.0f);
		Math::vector<4> swap = vec.swap(0, 1);

		Math::vector<4> EXPECTED(0.0f, -3.0f, 2.0f, 10.0f);

		CHECK(swap.x == Approx(EXPECTED.x));
		CHECK(swap.y == Approx(EXPECTED.y));
		CHECK(swap.z == Approx(EXPECTED.z));
		CHECK(swap.w == Approx(EXPECTED.w));
	}

	GIVEN("Swapping Y and Z")
	{
		Math::vector<4> vec(-3.0f, 0.0f, 2.0f, 10.0f);
		Math::vector<4> swap = vec.swap(1, 0);

		Math::vector<4> EXPECTED(0.0f, -3.0f, 2.0f, 10.0f);

		CHECK(swap.x == Approx(EXPECTED.x));
		CHECK(swap.y == Approx(EXPECTED.y));
		CHECK(swap.z == Approx(EXPECTED.z));
		CHECK(swap.w == Approx(EXPECTED.w));
	}

	GIVEN("Swapping X and Z")
	{
		Math::vector<4> vec(-3.0f, 0.0f, 2.0f, 10.0f);
		Math::vector<4> swap = vec.swap(0, 2);

		Math::vector<4> EXPECTED(2.0f, 0.0f, -3.0f, 10.0f);

		CHECK(swap.x == Approx(EXPECTED.x));
		CHECK(swap.y == Approx(EXPECTED.y));
		CHECK(swap.z == Approx(EXPECTED.z));
		CHECK(swap.w == Approx(EXPECTED.w));
	}

	GIVEN("Swapping Z and X")
	{
		Math::vector<4> vec(-3.0f, 0.0f, 2.0f, 10.0f);
		Math::vector<4> swap = vec.swap(2, 0);

		Math::vector<4> EXPECTED(2.0f, 0.0f, -3.0f, 10.0f);

		CHECK(swap.x == Approx(EXPECTED.x));
		CHECK(swap.y == Approx(EXPECTED.y));
		CHECK(swap.z == Approx(EXPECTED.z));
		CHECK(swap.w == Approx(EXPECTED.w));
	}

	GIVEN("Swapping X and W")
	{
		Math::vector<4> vec(-3.0f, 0.0f, 2.0f, 10.0f);
		Math::vector<4> swap = vec.swap(0, 3);

		Math::vector<4> EXPECTED(10.0f, 0.0f, 2.0f, -3.0f);

		CHECK(swap.x == Approx(EXPECTED.x));
		CHECK(swap.y == Approx(EXPECTED.y));
		CHECK(swap.z == Approx(EXPECTED.z));
		CHECK(swap.w == Approx(EXPECTED.w));
	}

	GIVEN("Swapping W and X")
	{
		Math::vector<4> vec(-3.0f, 0.0f, 2.0f, 10.0f);
		Math::vector<4> swap = vec.swap(3, 0);

		Math::vector<4> EXPECTED(10.0f, 0.0f, 2.0f, -3.0f);

		CHECK(swap.x == Approx(EXPECTED.x));
		CHECK(swap.y == Approx(EXPECTED.y));
		CHECK(swap.z == Approx(EXPECTED.z));
		CHECK(swap.w == Approx(EXPECTED.w));
	}

	GIVEN("Swapping Y and Z")
	{
		Math::vector<4> vec(-3.0f, 0.0f, 2.0f, 10.0f);
		Math::vector<4> swap = vec.swap(1, 2);

		Math::vector<4> EXPECTED(-3.0f, 2.0f, 0.0f, 10.0f);

		CHECK(swap.x == Approx(EXPECTED.x));
		CHECK(swap.y == Approx(EXPECTED.y));
		CHECK(swap.z == Approx(EXPECTED.z));
		CHECK(swap.w == Approx(EXPECTED.w));
	}

	GIVEN("Swapping Z and Y")
	{
		Math::vector<4> vec(-3.0f, 0.0f, 2.0f, 10.0f);
		Math::vector<4> swap = vec.swap(1, 2);

		Math::vector<4> EXPECTED(-3.0f, 2.0f, 0.0f, 10.0f);

		CHECK(swap.x == Approx(EXPECTED.x));
		CHECK(swap.y == Approx(EXPECTED.y));
		CHECK(swap.z == Approx(EXPECTED.z));
		CHECK(swap.w == Approx(EXPECTED.w));
	}

	GIVEN("Swapping Y and W")
	{
		Math::vector<4> vec(-3.0f, 0.0f, 2.0f, 10.0f);
		Math::vector<4> swap = vec.swap(1, 3);

		Math::vector<4> EXPECTED(-3.0f, 10.0f, 2.0f, 0.0f);

		CHECK(swap.x == Approx(EXPECTED.x));
		CHECK(swap.y == Approx(EXPECTED.y));
		CHECK(swap.z == Approx(EXPECTED.z));
		CHECK(swap.w == Approx(EXPECTED.w));
	}

	GIVEN("Swapping W and Y")
	{
		Math::vector<4> vec(-3.0f, 0.0f, 2.0f, 10.0f);
		Math::vector<4> swap = vec.swap(1, 3);

		Math::vector<4> EXPECTED(-3.0f, 10.0f, 2.0f, 0.0f);

		CHECK(swap.x == Approx(EXPECTED.x));
		CHECK(swap.y == Approx(EXPECTED.y));
		CHECK(swap.z == Approx(EXPECTED.z));
		CHECK(swap.w == Approx(EXPECTED.w));
	}

	GIVEN("Swapping Z and W")
	{
		Math::vector<4> vec(-3.0f, 0.0f, 2.0f, 10.0f);
		Math::vector<4> swap = vec.swap(2, 3);

		Math::vector<4> EXPECTED(-3.0f, 0.0f, 10.0f, 2.0f);

		CHECK(swap.x == Approx(EXPECTED.x));
		CHECK(swap.y == Approx(EXPECTED.y));
		CHECK(swap.z == Approx(EXPECTED.z));
		CHECK(swap.w == Approx(EXPECTED.w));
	}

	GIVEN("Swapping W and Z")
	{
		Math::vector<4> vec(-3.0f, 0.0f, 2.0f, 10.0f);
		Math::vector<4> swap = vec.swap(3, 2);

		Math::vector<4> EXPECTED(-3.0f, 0.0f, 10.0f, 2.0f);

		CHECK(swap.x == Approx(EXPECTED.x));
		CHECK(swap.y == Approx(EXPECTED.y));
		CHECK(swap.z == Approx(EXPECTED.z));
		CHECK(swap.w == Approx(EXPECTED.w));
	}

	GIVEN("Swapping X and X")
	{
		Math::vector<4> vec(-3.0f, 0.0f, 2.0f, 10.0f);
		Math::vector<4> swap = vec.swap(0, 0);

		Math::vector<4> EXPECTED(-3.0f, 0.0f, 2.0f, 10.0f);

		CHECK(swap.x == Approx(EXPECTED.x));
		CHECK(swap.y == Approx(EXPECTED.y));
		CHECK(swap.z == Approx(EXPECTED.z));
		CHECK(swap.w == Approx(EXPECTED.w));
	}

	GIVEN("Swapping Y and Y")
	{
		Math::vector<4> vec(-3.0f, 0.0f, 2.0f, 10.0f);
		Math::vector<4> swap = vec.swap(1, 1);

		Math::vector<4> EXPECTED(-3.0f, 0.0f, 2.0f, 10.0f);

		CHECK(swap.x == Approx(EXPECTED.x));
		CHECK(swap.y == Approx(EXPECTED.y));
		CHECK(swap.z == Approx(EXPECTED.z));
		CHECK(swap.w == Approx(EXPECTED.w));
	}

	GIVEN("Swapping Z and Z")
	{
		Math::vector<4> vec(-3.0f, 0.0f, 2.0f, 10.0f);
		Math::vector<4> swap = vec.swap(2, 2);

		Math::vector<4> EXPECTED(-3.0f, 0.0f, 2.0f, 10.0f);

		CHECK(swap.x == Approx(EXPECTED.x));
		CHECK(swap.y == Approx(EXPECTED.y));
		CHECK(swap.z == Approx(EXPECTED.z));
		CHECK(swap.w == Approx(EXPECTED.w));
	}

	GIVEN("Swapping W and W")
	{
		Math::vector<4> vec(-3.0f, 0.0f, 2.0f, 10.0f);
		Math::vector<4> swap = vec.swap(3, 3);

		Math::vector<4> EXPECTED(-3.0f, 0.0f, 2.0f, 10.0f);

		CHECK(swap.x == Approx(EXPECTED.x));
		CHECK(swap.y == Approx(EXPECTED.y));
		CHECK(swap.z == Approx(EXPECTED.z));
		CHECK(swap.w == Approx(EXPECTED.w));
	}
}

TEST_CASE("max(vector<4>)", "[simd_vec][max][maximum]")
{
	Math::vector<4> v1 (3.0f, 0.0f, -2.0f, 5.0f);

	float EXPECTED = 5.0f;

	CHECK(max(v1) == EXPECTED);

	v1 = Math::vector<4>(3.0f, 3.0f, 3.0f, 3.0f);

	EXPECTED = 3.0f;

	CHECK(max(v1) == EXPECTED);

	v1 = Math::vector<4>(0.0f, 0.0f, 0.0f, 0.0f);

	EXPECTED = 0.0f;

	CHECK(max(v1) == EXPECTED);

	v1 = Math::vector<4>(-10.0f, -10.0f, -10.0f, -10.0f);

	EXPECTED = -10.0f;

	CHECK(max(v1) == EXPECTED);

	v1 = Math::vector<4>(-9.0f, -7.0f, -5.0f, -3.0f);

	EXPECTED = -3.0f;

	CHECK(max(v1) == EXPECTED);
}

TEST_CASE("min(vector<4>)", "[simd_vec][min][minimum]")
{
	Math::vector<4> v1 (3.0f, 0.0f, -2.0f, 5.0f);

	float EXPECTED = -2.0f;

	CHECK(min(v1) == EXPECTED);

	v1 = Math::vector<4>(3.0f, 3.0f, 3.0f, 3.0f);

	EXPECTED = 3.0f;

	CHECK(min(v1) == EXPECTED);

	v1 = Math::vector<4>(0.0f, 0.0f, 0.0f, 0.0f);

	EXPECTED = 0.0f;

	CHECK(min(v1) == EXPECTED);

	v1 = Math::vector<4>(-10.0f, -10.0f, -10.0f, -10.0f);

	EXPECTED = -10.0f;

	CHECK(min(v1) == EXPECTED);

	v1 = Math::vector<4>(-9.0f, -7.0f, -5.0f, -3.0f);

	EXPECTED = -9.0f;

	CHECK(min(v1) == EXPECTED);
}

TEST_CASE("reflect(vector<3>, vector<3>)", "[simd_vec][reflect][lighting]")
{
	Math::vector<3> incident1 (1.0f, -1.0f, 0.0f);
	Math::vector<3> normal1 (0.0f, 1.0f, 0.0f);

	Math::vector<3> EXPECTED_1 (1.0f, 1.0f, 0.0f);

	Math::vector<3> reflection1 = Math::reflect(incident1, normal1);

	CHECK(reflection1.x == Approx(EXPECTED_1.x));
	CHECK(reflection1.y == Approx(EXPECTED_1.y));
	CHECK(reflection1.z == Approx(EXPECTED_1.z));

	Math::vector<3> incident2 (0.0f, -1.0f, 1.0f);
	Math::vector<3> normal2 (0.0f, 1.0f, 0.0f);

	Math::vector<3> EXPECTED_2 (0.0f, 1.0f, 1.0f);

	Math::vector<3> reflection2 = Math::reflect(incident2, normal2);

	CHECK(reflection2.x == Approx(EXPECTED_2.x));
	CHECK(reflection2.y == Approx(EXPECTED_2.y));
	CHECK(reflection2.z == Approx(EXPECTED_2.z));

	Math::vector<3> incident3 (2.0f, -1.0f, 0.0f);
	Math::vector<3> normal3 (0.0f, 1.0f, 0.0f);

	Math::vector<3> EXPECTED_3 (2.0f, 1.0f, 0.0f);

	Math::vector<3> reflection3 = Math::reflect(incident3, normal3);

	CHECK(reflection3.x == Approx(EXPECTED_3.x));
	CHECK(reflection3.y == Approx(EXPECTED_3.y));
	CHECK(reflection3.z == Approx(EXPECTED_3.z));
}