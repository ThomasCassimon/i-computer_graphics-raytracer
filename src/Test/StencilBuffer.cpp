/*
 * @author thomas
 * @date   21/10/18
 * @file   StencilBuffer.cpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#include <Catch/Catch.hpp>

#include <Graphics/StencilBuffer.hpp>

using namespace RayTracer;

TEST_CASE("Setting bits in the stencil buffer", "[bit][stencil][set]")
{
	Graphics::StencilBuffer buffer = Graphics::StencilBuffer(8,8);

	const std::array<Math::ivec2, 4> INDICES_TO_SET = {
			Math::ivec2(3,3),
			Math::ivec2(4,7),
			Math::ivec2(0,5),
			Math::ivec2(1,2),
	};

	for (const Math::ivec2& index: INDICES_TO_SET)
	{
		buffer.set(index.x, index.y);
	}

	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			const Math::ivec2 index = Math::ivec2(i,j);

			if (std::find(INDICES_TO_SET.begin(), INDICES_TO_SET.end(), index) == INDICES_TO_SET.end())
			{
				CHECK(!buffer.get(index.x, index.y));
			}
			else
			{
				CHECK(buffer.get(index.x, index.y));
			}
		}
	}
}

TEST_CASE("Resetting bits in the stencil buffer", "[bit][stencil][reset]")
{
	Graphics::StencilBuffer buffer = Graphics::StencilBuffer(8,8);

	const std::array<Math::ivec2, 4> INDICES_TO_RESET = {
			Math::ivec2(7,5),
			Math::ivec2(3,1),
			Math::ivec2(2,4),
			Math::ivec2(0,0),
	};

	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			const Math::ivec2 index = Math::ivec2(i,j);
			buffer.set(index.x, index.y);
		}
	}

	for (const Math::ivec2& index: INDICES_TO_RESET)
	{
		buffer.reset(index.x, index.y);
	}

	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			const Math::ivec2 index = Math::ivec2(i,j);


			if (std::find(INDICES_TO_RESET.begin(), INDICES_TO_RESET.end(), index) == INDICES_TO_RESET.end())
			{
				CHECK(buffer.get(index.x, index.y));
			}
			else
			{
				CHECK(!buffer.get(index.x, index.y));
			}
		}
	}
}