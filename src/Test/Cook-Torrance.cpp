/*
 * @author  thomas
 * @date    07/12/18
 * Copyright (c) 2018 thomas
 */

#include <Catch/Catch.hpp>
#include <Shaders/CookTorrance.hpp>

TEST_CASE("Fresnel function of Gold at phi = 0°", "[fresnel][gold][CookTorrance]")
{
	RayTracer::Camera camera (RayTracer::Math::ivec2(1920, 1080), RayTracer::Math::vec3(1.0f, 1.0f, 1.0f), RayTracer::Math::vec3(-1.0f, -1.0f, -1.0f));
	RayTracer::RayTracer tracer (camera, RayTracer::Shaders::ShaderModel::COOK_TORRANCE, 0, RayTracer::Graphics::Color(0.0f, 0.0f, 0.0f, 1.0f));
	RayTracer::Shaders::CookTorrance shader (tracer, RayTracer::Graphics::Color(0.0f, 0.0f, 0.0f, 1.0f), 0);

	RayTracer::Math::vec3 eta_gold (0.47f);
	RayTracer::Math::vec3 EXPECTED (1.0f, 0.71f, 0.29);
	RayTracer::Math::vec3 F0 = shader.fresnel(0.0f, eta_gold);

	CHECK(F0.x == EXPECTED.x);
	CHECK(F0.y == EXPECTED.y);
	CHECK(F0.z == EXPECTED.z);
}
