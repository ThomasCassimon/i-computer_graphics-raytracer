/*
 * Created by thomas on 28/09/18.
 */

#include <Catch/Catch.hpp>

#include <Math/vec3.hpp>

using namespace RayTracer;

TEST_CASE("vec3 + vec3", "[vec3][sum][add]")
{
	Math::vec3 v1;
	Math::vec3 v2;

	v1.x = 3.0f;
	v1.y = 0.0f;
	v1.z = 2.0f;

	v2.x = 4.0f;
	v2.y = 1.0f;
	v2.z = 8.0f;

	Math::vec3 cross = v1 + v2;
	Math::vec3 EXPECTED = Math::vec3(7.0f, 1.0f, 10.0f);

	CHECK(cross.x == Approx(EXPECTED.x));
	CHECK(cross.y == Approx(EXPECTED.y));
	CHECK(cross.z == Approx(EXPECTED.z));
}

TEST_CASE("vec3 - vec3", "[vec3][difference][sub]")
{
	Math::vec3 v1;
	Math::vec3 v2;

	v1.x = 3.0f;
	v1.y = 0.0f;
	v1.z = 2.0f;

	v2.x = 4.0f;
	v2.y = 1.0f;
	v2.z = 8.0f;

	Math::vec3 cross = v1 - v2;
	Math::vec3 EXPECTED = Math::vec3(-1.0f, -1.0f, -6.0f);

	CHECK(cross.x == Approx(EXPECTED.x));
	CHECK(cross.y == Approx(EXPECTED.y));
	CHECK(cross.z == Approx(EXPECTED.z));
}

TEST_CASE("vec3 x vec3", "[vec3][cross][multiplication]")
{
	Math::vec3 v1;
	Math::vec3 v2;

	v1.x = 3.0f;
	v1.y = 0.0f;
	v1.z = 2.0f;

	v2.x = 4.0f;
	v2.y = 1.0f;
	v2.z = 8.0f;

	Math::vec3 cross = Math::cross(v1, v2);
	Math::vec3 EXPECTED = Math::vec3(-2.0f, -16.0f, 3.0f);

	CHECK(cross.x == Approx(EXPECTED.x));
	CHECK(cross.y == Approx(EXPECTED.y));
	CHECK(cross.z == Approx(EXPECTED.z));
}

TEST_CASE("vec3 . vec3", "[vec3][dot][multiplication]")
{
	Math::vec3 v1;
	Math::vec3 v2;

	v1.x = 3.0f;
	v1.y = 0.0f;
	v1.z = 2.0f;

	v2.x = 4.0f;
	v2.y = 1.0f;
	v2.z = 8.0f;

	float dotprod = Math::dot(v1, v2);
	float EXPECTED = (3.0f * 4.0f) + (0.0f * 1.0f) + (2.0f * 8.0f);

	CHECK(dotprod == Approx(EXPECTED));

	v1.x = 1.0f;
	v1.y = 2.0f;
	v1.z = 3.0f;

	v2.x = 4.0f;
	v2.y = -5.0f;
	v2.z = 6.0f;

	dotprod = Math::dot(v1, v2);
	EXPECTED = (1.0f * 4.0f) + (2.0f * -5.0f) + (3.0f * 6.0f);

	CHECK(dotprod == Approx(EXPECTED));

	v1.x = 9.0f;
	v1.y = 2.0f;
	v1.z = 7.0f;

	v2.x = 4.0f;
	v2.y = 8.0f;
	v2.z = 10.0f;

	dotprod = Math::dot(v1, v2);
	EXPECTED = (9.0f * 4.0f) + (2.0f * 8.0f) + (7.0f * 10.0f);

	CHECK(dotprod == Approx(EXPECTED));
}

TEST_CASE("vec3.length", "[vec3][length][norm]")
{
	Math::vec3 v;

	v.x = 3.0f;
	v.y = 0.0f;
	v.z = 2.0f;

	float length = v.length();
	float EXPECTED = std::sqrt(std::pow(3.0f, 2.0f) + std::pow(0.0f, 2.0f) + std::pow(2.0f, 2.0f));

	CHECK(length == Approx(EXPECTED));
}

TEST_CASE("normalize(vec3)", "[vec3][normalize][norm]")
{
	Math::vec3 v;

	v.x = 3.0f;
	v.y = 0.0f;
	v.z = 2.0f;

	Math::vec3 normalized = normalize(v);
	float length = std::sqrt(std::pow(3.0f, 2.0f) + std::pow(0.0f, 2.0f) + std::pow(2.0f, 2.0f));
	Math::vec3 EXPECTED = Math::vec3(3.0f / length, 0.0f / length, 2.0f / length);

	CHECK(normalized.x == Approx(EXPECTED.x));
	CHECK(normalized.y == Approx(EXPECTED.y));
	CHECK(normalized.z == Approx(EXPECTED.z));
}

TEST_CASE("vec3.normalize", "[vec3][normalize][norm]")
{
	Math::vec3 v;

	v.x = 3.0f;
	v.y = 0.0f;
	v.z = 2.0f;

	Math::vec3 normalized = v.normalize();
	float length = std::sqrt(std::pow(3.0f, 2.0f) + std::pow(0.0f, 2.0f) + std::pow(2.0f, 2.0f));
	Math::vec3 EXPECTED = Math::vec3(3.0f / length, 0.0f / length, 2.0f / length);

	CHECK(normalized.x == Approx(EXPECTED.x));
	CHECK(normalized.y == Approx(EXPECTED.y));
	CHECK(normalized.z == Approx(EXPECTED.z));
}

TEST_CASE("vec3 * float", "[vec3][scalar][product][multiplication]")
{
	Math::vec3 v;
	float coef = 2.0f;

	v.x = 3.0f;
	v.y = 0.0f;
	v.z = 2.0f;

	Math::vec3 product = v * coef;
	Math::vec3 EXPECTED = Math::vec3(6.0f, 0.0f, 4.0f);

	CHECK(product.x == Approx(EXPECTED.x));
	CHECK(product.y == Approx(EXPECTED.y));
	CHECK(product.z == Approx(EXPECTED.z));
}

TEST_CASE("float * vec3", "[vec3][scalar][product][multiplication]")
{
	Math::vec3 v;
	float coef = 2.0f;

	v.x = 3.0f;
	v.y = 0.0f;
	v.z = 2.0f;

	Math::vec3 product = coef * v;
	Math::vec3 EXPECTED = Math::vec3(6.0f, 0.0f, 4.0f);

	CHECK(product.x == Approx(EXPECTED.x));
	CHECK(product.y == Approx(EXPECTED.y));
	CHECK(product.z == Approx(EXPECTED.z));
}

TEST_CASE("vec3 / float", "[vec3][scalar][product][multiplication]")
{
	Math::vec3 v;
	float coef = 2.0f;

	v.x = 6.0f;
	v.y = 0.0f;
	v.z = 2.0f;

	Math::vec3 product = v / coef;
	Math::vec3 EXPECTED = Math::vec3(3.0f, 0.0f, 1.0f);

	CHECK(product.x == Approx(EXPECTED.x));
	CHECK(product.y == Approx(EXPECTED.y));
	CHECK(product.z == Approx(EXPECTED.z));
}

TEST_CASE("vec3(int)", "[vec3][access]")
{
	Math::vec3 v;

	v.x = 6.0f;
	v.y = 0.0f;
	v.z = 2.0f;

	CHECK(v(0) == Approx(6.0f));
	CHECK(v(1) == Approx(0.0f));
	CHECK(v(2) == Approx(2.0f));
}