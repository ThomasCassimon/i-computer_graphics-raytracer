/*
 * @author thomas
 * @date   21/10/18
 * @file   StencilBuffer.cpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#include "StencilBuffer.hpp"

namespace RayTracer::Graphics
{
	StencilBuffer::StencilBuffer(int rows, int cols) noexcept: mRows(rows), mCols(cols), buffer(static_cast<std::size_t>(rows * cols))
	{
	}

	void StencilBuffer::set(int row, int col) noexcept
	{
		this->buffer[this->flattenIndex(row, col)] = 1;
	}

	void StencilBuffer::reset(int row, int col) noexcept
	{
		this->buffer[this->flattenIndex(row, col)] = 0;
	}

	std::uint8_t StencilBuffer::get(int row, int col) const noexcept
	{
		return this->buffer[this->flattenIndex(row, col)];
	}

	const std::vector<std::uint8_t>& StencilBuffer::getBuffer() const noexcept
	{
		return this->buffer;
	}
}