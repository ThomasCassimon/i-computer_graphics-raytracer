/*
 * Created by thomas on 28/09/18.
 */

#include "Color.hpp"

namespace RayTracer::Graphics
{
	Color::Color(const Json::Value& jsonColor)
	{
		if ((jsonColor.size() != 3) && (jsonColor.size() != 4))
		{
			throw std::runtime_error("Json color had incorrect size.");
		}

		this->r = static_cast<std::uint8_t>(jsonColor[0].asInt());
		this->g = static_cast<std::uint8_t>(jsonColor[1].asInt());
		this->b = static_cast<std::uint8_t>(jsonColor[2].asInt());

		if (jsonColor.size() == 4)
		{
			this->a =  static_cast<std::uint8_t>(jsonColor[3].asInt());
		}
		else
		{
			this->a = 255;
		}
	}

	uint8_t& Color::red() noexcept
	{
		return this->r;
	}

	const uint8_t& Color::red() const noexcept
	{
		return this->r;
	}

	uint8_t& Color::green() noexcept
	{
		return this->g;
	}

	const uint8_t& Color::green() const noexcept
	{
		return this->g;
	}

	uint8_t& Color::blue() noexcept
	{
		return this->b;
	}

	const uint8_t& Color::blue() const noexcept
	{
		return this->b;
	}

	uint8_t& Color::alpha() noexcept
	{
		return this->a;
	}

	const uint8_t& Color::alpha() const noexcept
	{
		return this->a;
	}

	Color::operator Math::vec3() const noexcept
	{
		Math::vec3 vec (static_cast<float>(this->r) / 255.0f, static_cast<float>(this->g) / 255.0f, static_cast<float>(this->b) / 255.0f);
		return vec;
	}

	std::ostream& operator<< (std::ostream& stream, const Color& color) noexcept
	{
		stream << "(r: " << static_cast<int>(color.r) << ", g: " << static_cast<int>(color.g) << ", b: " << static_cast<int>(color.b) << ", a: " << static_cast<int>(color.a) << ")";
		return stream;
	}

	bool operator==(const Color& lhs, const Color& rhs) noexcept
	{
		return (lhs.r == rhs.r) && (lhs.g == rhs.g) && (lhs.b == rhs.b) && (lhs.a == rhs.a);
	}
}