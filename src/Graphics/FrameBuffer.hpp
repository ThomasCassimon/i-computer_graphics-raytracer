/*
 * @author thomas
 * @date   07/10/18
 * @file   FrameBuffer.hpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#ifndef RAY_TRACER_FRAMEBUFFER_HPP
#define RAY_TRACER_FRAMEBUFFER_HPP

#ifdef USE_SDL2
#include <SDL2/SDL.h>
#endif

#include "Graphics/PixelBuffer.hpp"
#include "Math/vec2.hpp"

namespace RayTracer::Graphics
{
	class FrameBuffer
	{
		private:
			PixelBuffer buffer;

		public:
			explicit FrameBuffer(const Math::ivec2& size);

			const Color& operator()(int row, int col) const noexcept;

			Color& operator()(int row, int col) noexcept;

			void clear (const Color& clearColor) noexcept;

			#ifdef USE_SDL2
			void blit (SDL_Renderer* renderer) const;
			#endif

			void save (const std::string& filename, PixelBuffer::FileType filetype = PixelBuffer::FileType::PNG) const;

			int rows() const noexcept;

			int cols() const noexcept;
	};
}

#endif //RAY_TRACER_FRAMEBUFFER_HPP
