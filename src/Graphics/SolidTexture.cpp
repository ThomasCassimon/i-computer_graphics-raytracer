/*
 * @author thomas
 * @date 12/30/18.
 */

#include "SolidTexture.hpp"

namespace RayTracer::Graphics
{
	SolidTexture::SolidTexture(const Json::Value& jsonSolidTexture)
	{
		const std::string functionName = jsonSolidTexture.asString();

		if (functionName == CHECKERBOARD_FUNCTION)
		{
			this->function = SolidTexture::checkerboard;
		}
		else if (functionName == DISCO_1_FUNCTION)
		{
			this->function = SolidTexture::disco_1;
		}
		else if (functionName == DISCO_2_FUNCTION)
		{
			this->function = SolidTexture::disco_2;
		}
		else if (functionName == DISCO_3_FUNCTION)
		{
			this->function = SolidTexture::disco_3;
		}
		else if (functionName == WAVE_1_FUNCTION)
		{
			this->function = SolidTexture::wave_1;
		}
		else if (functionName == WAVE_2_FUNCTION)
		{
			this->function = SolidTexture::wave_2;
		}
		else if (functionName == WAVE_3_FUNCTION)
		{
			this->function = SolidTexture::wave_3;
		}
		else
		{
			throw std::runtime_error("Unrecognized solid texture function \"" + functionName + "\".");
		}
	}

	SolidTexture::SolidTexture(const std::function<Math::vec3(Math::vec3)>& function): function(function)
	{
	}

	Color SolidTexture::sample(const Math::vec3& coordinate) const noexcept
	{
		return Color(this->function(coordinate));
	}

	Math::vec3 SolidTexture::checkerboard(const Math::vec3& coordinate) noexcept
	{
		constexpr static float SCALE = 0.05f;
		const int x = static_cast<int>(coordinate.x / SCALE);
		const int y = static_cast<int>(coordinate.y / SCALE);
		const int z = static_cast<int>(coordinate.z / SCALE);
		const int result = (x + y + z) == 0 ? 0 : (x + y + z) % 2;

		return Math::vec3(static_cast<float>(result));
	}

	Math::vec3 SolidTexture::disco_1(const Math::vec3& coordinate) noexcept
	{
		constexpr static float FREQ = 10.0f;
		const float r = std::sin(coordinate.x * FREQ) * std::sin(coordinate.z * FREQ);
		const float g = std::sin(coordinate.x * FREQ) * std::cos(coordinate.z * FREQ);
		const float b = std::cos(coordinate.x * FREQ) * std::cos(coordinate.z * FREQ);

		return Math::vec3(r, g, b);
	}

	Math::vec3 SolidTexture::disco_2(const Math::vec3& coordinate) noexcept
	{
		constexpr static float FREQ = 100.0f;
		const float r = (std::cos(coordinate.x * FREQ) + 1.0f) / 2.0f;
		const float g = (std::cos(coordinate.y * FREQ) + 1.0f) / 2.0f;
		const float b = (std::cos(coordinate.z * FREQ) + 1.0f) / 2.0f;

		return Math::vec3(r, r + g, r + g + b);
	}

	Math::vec3 SolidTexture::disco_3(const Math::vec3& coordinate) noexcept
	{
		constexpr static float FREQ = 10.0f;
		const float r = std::sin(coordinate.x * FREQ) * std::sin(coordinate.z * FREQ);
		const float g = std::sin(coordinate.x * FREQ) * std::cos(coordinate.z * FREQ);
		const float b = std::cos(coordinate.x * FREQ) * std::cos(coordinate.z * FREQ);

		return Math::vec3(r, g, b);
	}

	Math::vec3 SolidTexture::wave_1(const Math::vec3& coordinate) noexcept
	{
		constexpr static float FREQ = 100.0f;
		const float r = std::sin(coordinate.x * FREQ) * std::sin(coordinate.z * FREQ);

		return Math::vec3(r);
	}

	Math::vec3 SolidTexture::wave_2(const Math::vec3& coordinate) noexcept
	{
		constexpr static float FREQ = 100.0f;
		const float r = (std::cos(coordinate.x * FREQ) + 0.5f) * (std::cos(coordinate.z * FREQ) + 0.5f);

		return Math::vec3(r);
	}

	Math::vec3 SolidTexture::wave_3(const Math::vec3& coordinate) noexcept
	{
		constexpr static float FREQ = 100.0f;
		const float r = 1.0f - ((std::sin(coordinate.x * FREQ) + 1.0f) / 2.0f) * ((std::sin(coordinate.z * FREQ) + 1.0f) / 2.0f);

		return Math::vec3(r);
	}
}