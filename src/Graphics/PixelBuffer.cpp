/*
 * Created by thomas on 28/09/18.
 */

#include "PixelBuffer.hpp"

#include <stb_image/stb_image.h>
#include <stb_image_write/stb_image_write.h>

namespace RayTracer::Graphics
{
	void PixelBuffer::savePpm(const std::string& filename) const
	{
		std::ofstream filestream (filename, std::ios::binary);

		if (filestream.is_open())
		{
			filestream << "P6" << '\n';
			filestream << std::to_string(this->mCols) << " " << std::to_string(this->mRows) << '\n';
			filestream << "255" << '\n';

			for (int i = 0; i < this->mRows; i++)
			{
				for (int j = 0; j < this->mCols; j++)
				{
					const int index = this->flattenIndex(i, j);
					uint8_t red = this->mBuffer[index].red();
					uint8_t green = this->mBuffer[index].green();
					uint8_t blue = this->mBuffer[index].blue();

					filestream << red << green << blue;
				}
			}

			filestream.close();
		}
		else
		{
			throw std::runtime_error("Failed to open file '" + filename + "'");
		}
	}

	void PixelBuffer::savePng(const std::string& filename) const
	{
		std::vector<std::uint8_t> data (static_cast<std::size_t>(this->mRows * this->mCols * 4));

		for (long i = 0; i < (this->mRows * this->mCols); ++i)
		{
			const long dataIdx = (4 * i);

			data[dataIdx    ] = this->mBuffer[i].red();
			data[dataIdx + 1] = this->mBuffer[i].green();
			data[dataIdx + 2] = this->mBuffer[i].blue();
			data[dataIdx + 3] = this->mBuffer[i].alpha();
		}

		stbi_write_png(filename.c_str(), this->mCols, this->mRows, 4, data.data(), 0);
	}

	PixelBuffer::PixelBuffer(const Math::ivec2& size, const Graphics::Color& color) : mRows(size.y), mCols(size.x), mBuffer(static_cast<std::size_t>(size.x * size.y))
	{
		for (int i = 0; i < (size.x * size.y); i++)
		{
			this->mBuffer[i] = color;
		}
	}

	int PixelBuffer::rows() const noexcept
	{
		return this->mRows;
	}

	int PixelBuffer::cols() const noexcept
	{
		return this->mCols;
	}

	const Color& PixelBuffer::operator()(int row, int col) const noexcept
	{
		return this->mBuffer[this->flattenIndex(row, col)];
	}

	Color& PixelBuffer::operator()(int row, int col) noexcept
	{
		return this->mBuffer[this->flattenIndex(row, col)];
	}

	void PixelBuffer::save(const std::string& filename, PixelBuffer::FileType fileType) const
	{
		switch (fileType)
		{
			case FileType::PPM:
				this->savePpm(filename);
				break;

			case FileType::PNG:
				this->savePng(filename);
				break;
		}
	}

	Color PixelBuffer::sample(const Math::vec3& coordinate) const noexcept
	{
		if ((this->mRows == 1) && (this->mCols == 1))
		{
			return this->mBuffer[0];
		}

		float u = coordinate.x;
		float v = coordinate.z;

		u *= this->mCols;
		v *= this->mRows;

		u = 0 < u ? u : 0;
		v = 0 < v ? v : 0;

		int truncU = static_cast<int>(u) > (this->mCols - 1) ? (this->mCols -1) : static_cast<int>(u);
		int truncV = static_cast<int>(v) > (this->mRows - 1) ? (this->mRows -1) : static_cast<int>(v);

		return this->mBuffer[this->flattenIndex(truncV, truncU)];
	}

	PixelBuffer PixelBuffer::load(const std::string& filename)
	{
		Math::ivec2 size;
		int channels = 0;
		stbi_set_flip_vertically_on_load(true);
		unsigned char* pixelBuffer = stbi_load(filename.c_str(), &size.x, &size.y, &channels, 4);

		if (pixelBuffer == nullptr)
		{
			throw std::runtime_error("Failed to read texture from file \"" + filename + "\"");
		}

		PixelBuffer buffer (size);

		for (int i = 0; i < size.y; i++)
		{
			for (int j = 0; j < size.x; j++)
			{
				const int index = ((i * size.x) + j) * channels;

				const std::uint8_t r = channels > 0 ? pixelBuffer[index    ] : static_cast<std::uint8_t>(0);
				const std::uint8_t g = channels > 1 ? pixelBuffer[index + 1] : static_cast<std::uint8_t>(0);
				const std::uint8_t b = channels > 2 ? pixelBuffer[index + 2] : static_cast<std::uint8_t>(0);
				const std::uint8_t a = channels > 3 ? pixelBuffer[index + 3] : static_cast<std::uint8_t>(0xFF);


				buffer(i,j) = Graphics::Color(r, g, b, a);
			}
		}

		stbi_image_free(pixelBuffer);

		return buffer;
	}
}