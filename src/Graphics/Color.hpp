/*
 * Created by thomas on 28/09/18.
 */

#ifndef RAY_TRACER_COLOR_HPP
#define RAY_TRACER_COLOR_HPP

#include <cstdint>

#include <string>

#include <Math/vec3.hpp>
#include <Math/vec4.hpp>

namespace RayTracer::Graphics
{
	class Color
	{
		private:
			uint8_t r;
			uint8_t g;
			uint8_t b;
			uint8_t a;

		public:
			constexpr Color () noexcept: Color(static_cast<uint8_t>(0), static_cast<uint8_t>(0), static_cast<uint8_t>(0), static_cast<uint8_t>(255))
			{
			}

			constexpr Color (uint8_t r, uint8_t g, uint8_t b, uint8_t a = 0xFF) noexcept: r(r), g(g), b(b), a(a)
			{
			}

			constexpr Color (float r, float g, float b, float a = 1.0f) noexcept: r(static_cast<uint8_t>(r * 255)), g(static_cast<uint8_t>(g * 255)), b(static_cast<uint8_t>(b * 255)), a(static_cast<uint8_t>(a * 255))
			{
			}

			explicit constexpr Color (const Math::vec3& vec) noexcept: Color(vec.r, vec.g, vec.b)
			{
			}

			explicit constexpr Color (const Math::vec4& vec) noexcept: Color (vec.r, vec.g, vec.b, vec.a)
			{
			}

			explicit Color (const Json::Value& jsonColor);

			uint8_t& red() noexcept;

			const uint8_t& red() const noexcept;

			uint8_t& green() noexcept;

			const uint8_t& green() const noexcept;

			uint8_t& blue() noexcept;

			const uint8_t& blue() const noexcept;

			uint8_t& alpha() noexcept;

			const uint8_t& alpha() const noexcept;

			explicit operator Math::vec3() const noexcept;

			friend std::ostream& operator<< (std::ostream& stream, const Color& color) noexcept;

			friend bool operator== (const Color& lhs, const Color& rhs) noexcept;
	};
}

#endif //RAY_TRACER_COLOR_HPP
