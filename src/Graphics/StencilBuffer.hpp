/*
 * @author thomas
 * @date   21/10/18
 * @file   StencilBuffer.hpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#ifndef RAY_TRACER_STENCILBUFFER_HPP
#define RAY_TRACER_STENCILBUFFER_HPP

#include <iostream>
#include <vector>

#include <cmath>
#include <cstdint>

#include <Math/vec2.hpp>

namespace RayTracer::Graphics
{
	class StencilBuffer
	{
		private:
			int mRows;
			int mCols;
			std::vector<std::uint8_t> buffer;

			constexpr long flattenIndex(int row, int col) const noexcept
			{
				return (row * this->mCols) + col;
			}

		public:
			explicit StencilBuffer(int rows, int cols) noexcept;

			void set(int row, int col) noexcept;

			void reset(int row, int col) noexcept;

			std::uint8_t get(int row, int col) const noexcept;

			const std::vector<std::uint8_t>& getBuffer() const noexcept;
	};
}

#endif //RAY_TRACER_STENCILBUFFER_HPP
