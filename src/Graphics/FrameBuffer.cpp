/*
 * @author thomas
 * @date   07/10/18
 * @file   FrameBuffer.cpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#include "FrameBuffer.hpp"

namespace RayTracer::Graphics
{
	FrameBuffer::FrameBuffer(const Math::ivec2& size) : buffer(size)
	{
	}

	const Color& FrameBuffer::operator()(int row, int col) const noexcept
	{
		return this->buffer(row, col);
	}

	Color& FrameBuffer::operator()(int row, int col) noexcept
	{
		return this->buffer(row, col);
	}

	void FrameBuffer::clear(const Color& clearColor) noexcept
	{
		for (int i = 0; i < this->buffer.rows(); i++)
		{
			for (int j = 0; j < this->buffer.cols(); j++)
			{
				this->buffer(i, j) = clearColor;
			}
		}
	}

	#ifdef USE_SDL2
	void FrameBuffer::blit(SDL_Renderer* renderer) const
	{
		for (int i = (this->buffer.rows() - 1); i > 0; i--)
		{
			for (int j = 0; j < this->buffer.cols(); j++)
			{
				Color pixel = this->buffer(i,j);

				SDL_SetRenderDrawColor(renderer, pixel.red(), pixel.green(), pixel.blue(), pixel.alpha());
				SDL_RenderDrawPoint(renderer, j, (this->buffer.rows() - 1) - i);
			}
		}
	}
	#endif

	void FrameBuffer::save(const std::string& filename, PixelBuffer::FileType filetype) const
	{
		this->buffer.save(filename, filetype);
	}

	int FrameBuffer::rows() const noexcept
	{
		return this->buffer.rows();
	}

	int FrameBuffer::cols() const noexcept
	{
		return this->buffer.cols();
	}
}