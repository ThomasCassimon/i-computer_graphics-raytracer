/*
 * @author thomas
 * @date   05/10/18
 * @file   ISamplable.hpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#ifndef RAY_TRACER_SAMPLABLE_HPP
#define RAY_TRACER_SAMPLABLE_HPP

#include <Graphics/Color.hpp>

namespace RayTracer::Graphics
{
	class Samplable
	{
		public:
			virtual ~Samplable() = default;

			virtual Color sample(const Math::vec3& coordinate) const noexcept = 0;
	};
}

#endif //RAY_TRACER_SAMPLABLE_HPP
