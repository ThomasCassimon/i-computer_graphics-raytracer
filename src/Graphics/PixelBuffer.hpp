/*
 * Created by thomas on 28/09/18.
 */

#ifndef RAY_TRACER_PIXELBUFFER_HPP
#define RAY_TRACER_PIXELBUFFER_HPP

#include <iostream>
#include <fstream>
#include <stdexcept>
#include <vector>

#include <cstring>

#include "Graphics/Color.hpp"
#include "Graphics/Samplable.hpp"
#include "Math/vec2.hpp"

namespace RayTracer::Graphics
{
	class PixelBuffer: public Samplable
	{
		private:
			int mRows;
			int mCols;

			std::vector<Color> mBuffer;

			inline int flattenIndex (int row, int col) const noexcept
			{
				return (row * this->mCols) + col;
			}

			void savePpm (const std::string& filename) const;

			void savePng (const std::string& filename) const;

		public:
			enum class FileType
			{
				PPM,
				PNG
			};

			PixelBuffer() = delete;

			explicit PixelBuffer (const Math::ivec2& size, const Graphics::Color& color = Graphics::Color(0.0f, 0.0f, 0.0f, 0.0f));

			int rows() const noexcept;

			int cols() const noexcept;

			const Color& operator() (int row, int col) const noexcept;

			Color& operator() (int row, int col) noexcept;

			/**
			 * Save the image to a file
			 * @param filename
			 * @param fileType
			 */
			void save (const std::string& filename, FileType fileType) const;

			/**
			 * Load a pixelbuffer from an image.
			 * @param filename 	The filename of the image to load
			 * @return
			 */
			static PixelBuffer load (const std::string& filename);

			/**
			 * X = Columns, Y = Rows
			 * @param u
			 * @param v
			 * @return
			 */
			Color sample(const Math::vec3& coordinate) const noexcept override;
	};
}

#endif //RAY_TRACER_PIXELBUFFER_HPP
