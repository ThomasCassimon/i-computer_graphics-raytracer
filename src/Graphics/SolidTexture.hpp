/*
 * @author thomas
 * @date 12/30/18.
 */

#ifndef RAY_TRACER_SOLID_TEXTURE_HPP
#define RAY_TRACER_SOLID_TEXTURE_HPP

#include <functional>
#include <string_view>

#include "Math/vec3.hpp"
#include "Samplable.hpp"

namespace RayTracer::Graphics
{
	class SolidTexture: public Samplable
	{
		private:
			constexpr static std::string_view CHECKERBOARD_FUNCTION = "checkerboard";
			constexpr static std::string_view DISCO_1_FUNCTION = "disco_1";
			constexpr static std::string_view DISCO_2_FUNCTION = "disco_2";
			constexpr static std::string_view DISCO_3_FUNCTION = "disco_3";
			constexpr static std::string_view WAVE_1_FUNCTION = "wave_1";
			constexpr static std::string_view WAVE_2_FUNCTION = "wave_2";
			constexpr static std::string_view WAVE_3_FUNCTION = "wave_3";

			std::function<Math::vec3(Math::vec3)> function;

		public:
			explicit SolidTexture (const Json::Value& jsonSolidTexture);

			explicit SolidTexture (const std::function<Math::vec3(Math::vec3)>& function);

			Color sample(const Math::vec3& coordinate) const noexcept override;

			static Math::vec3 checkerboard (const Math::vec3& coordinate) noexcept;

			static Math::vec3 disco_1(const Math::vec3& coordinate) noexcept;

			static Math::vec3 disco_2(const Math::vec3& coordinate) noexcept;

			static Math::vec3 disco_3(const Math::vec3& coordinate) noexcept;

			static Math::vec3 wave_1(const Math::vec3& coordinate) noexcept;

			static Math::vec3 wave_2(const Math::vec3& coordinate) noexcept;

			static Math::vec3 wave_3(const Math::vec3& coordinate) noexcept;
	};
}

#endif //RAY_TRACER_SOLID_TEXTURE_HPP
