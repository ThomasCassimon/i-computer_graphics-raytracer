/*
 * @author  thomas
 * @date    30/11/18
 * Copyright (c) 2018 thomas
 */

#ifndef RAY_TRACER_FILTER_HPP
#define RAY_TRACER_FILTER_HPP

#include <Graphics/FrameBuffer.hpp>

namespace RayTracer::PostProcessing
{
	class Filter
	{
		public:
			virtual Graphics::FrameBuffer filter(const Graphics::FrameBuffer& frameBuffer) const noexcept = 0;

			virtual ~Filter() = default;
	};
}

#endif //RAY_TRACER_FILTER_HPP
