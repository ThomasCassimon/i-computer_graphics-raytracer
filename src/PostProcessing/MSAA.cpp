/*
 * @author  thomas
 * @date    30/11/18
 * Copyright (c) 2018 thomas
 */

#include "MSAA.hpp"

namespace RayTracer::PostProcessing
{

	MSAA::MSAA(int multiSampleFactor): offsets(static_cast<std::size_t>(multiSampleFactor + 1))
	{
		switch (multiSampleFactor)
		{
			case 2:
				this->offsets = {Math::ivec2(0, -1), Math::ivec2(-1, 0), Math::ivec2(0, 0)};
				break;

			case 4:
				this->offsets = {Math::ivec2(0, -1), Math::ivec2(-1, 0), Math::ivec2(0, 0), Math::ivec2(1, 0), Math::ivec2(0, 1)};
				break;

			case 8:
				this->offsets = {Math::ivec2(-1, -1), Math::ivec2(0, -1), Math::ivec2(1, -1), Math::ivec2(-1, 0), Math::ivec2(0, 0), Math::ivec2(1, 0), Math::ivec2(-1, 1), Math::ivec2(0, 1), Math::ivec2(1, 1)};
				break;

			default:
				throw std::runtime_error("Invalid subsample factor for MSAA. Possible values are 2, 4 and 8.");
		}
	}

	Graphics::FrameBuffer MSAA::filter(const Graphics::FrameBuffer& pixelBuffer) const noexcept
	{

		Graphics::FrameBuffer copy (pixelBuffer);

		for (int i = 0; i < pixelBuffer.rows(); ++i)
		{
			for (int j = 0; j < pixelBuffer.cols(); ++j)
			{
				Math::ivec2 index = Math::ivec2(i, j);
				Math::vec3 color;

				int numPixels = 0;
				for (const Math::ivec2& offset: this->offsets)
				{
					Math::ivec2 localIndex = index + offset;

					if ((0 <= localIndex.x) && (0 <= localIndex.y) && (localIndex.x < pixelBuffer.rows()) && (localIndex.y < pixelBuffer.cols()))
					{
						color += static_cast<Math::vec3>(pixelBuffer(localIndex.x, localIndex.y));
						++numPixels;
					}
				}

				if (numPixels > 0)
				{
					color /= static_cast<float>(numPixels);
				}
				else
				{
					color = static_cast<Math::vec3>(pixelBuffer(i, j));
				}

				copy(i,j) = Graphics::Color(color);
			}
		}

		return copy;
	}
}