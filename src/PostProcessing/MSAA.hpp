/*
 * @author  thomas
 * @date    30/11/18
 * Copyright (c) 2018 thomas
 */

#ifndef RAY_TRACER_MSAA_HPP
#define RAY_TRACER_MSAA_HPP

#include <PostProcessing/Filter.hpp>

namespace RayTracer::PostProcessing
{
	class MSAA: public Filter
	{
		private:
			std::vector<Math::ivec2> offsets;

		public:
			explicit MSAA(int multiSampleFactor = 4);

			Graphics::FrameBuffer filter(const Graphics::FrameBuffer& pixelBuffer) const noexcept override;
	};
}

#endif //RAY_TRACER_MSAA_HPP
