/*
 * @author  thomas
 * @date    07/12/18
 * Copyright (c) 2018 thomas
 */

#ifndef RAY_TRACER_MATERIAL_HPP
#define RAY_TRACER_MATERIAL_HPP

#include <Math/vec3.hpp>

namespace RayTracer::Shaders
{
	class Material
	{
		private:
			constexpr static std::string_view NAME_KEY = "name";
			constexpr static std::string_view ROUGHNESS_KEY = "roughness";
			constexpr static std::string_view REFRACTION_INDEX_KEY = "refractionIndex";
			constexpr static std::string_view SPECULAR_KEY = "kSpecular";
			constexpr static std::string_view REFLECTIVITY_KEY = "reflectivity";
			constexpr static std::string_view TRANSPARENCY_KEY = "transparency";

			float kSpecular;
			float roughness;
			float reflectivity;
			float transparency;
			Math::vec3 refractionIndex;

		public:
			Material() noexcept;

			Material(const Json::Value& jsonMaterial);

			Material(float specular, float roughness, float reflectivity, const Math::vec3& refractionIndex) noexcept;

			float getSpecular() const noexcept;

			float getRoughness() const noexcept;

			float getReflectivity() const noexcept;

			float getTransparency() const noexcept;

			const Math::vec3& getRefractionIndex() const noexcept;
	};
}

#endif //RAY_TRACER_MATERIAL_HPP
