/*
 * @author  thomas
 * @date    16/11/18
 * Copyright (c) 2018 thomas
 */

#ifndef RAY_TRACER_BLINNPHONG_HPP
#define RAY_TRACER_BLINNPHONG_HPP

#include <vector>
#include <Graphics/Color.hpp>
#include <Lights/GlobalLight.hpp>
#include <Lights/PointLight.hpp>
#include <Objects/Hit.hpp>
#include <RayTracer.hpp>

namespace RayTracer::Shaders
{
	class BlinnPhong
	{
		private:
			bool ambient;
			bool diffuse;
			bool specular;

			float specularPower;
			float specularStrength;

			RayTracer& rayTracer;

		public:
			explicit BlinnPhong(RayTracer& rayTracer, float specularPower = 16.0f, float specularStrength = 0.5f) noexcept;

			/**
			 * Calculate lighting for the given hits using the Blinn-Phong model as described here: https://learnopengl.com/Lighting/Basic-Lighting
			 * @param cameraPos	The position of the camera
			 * @param hits		A list of all hits that we want to calculate colors for
			 * @return			A list of all colors, they match up with their respective hits based on their indices.
			 */
			std::vector<Graphics::Color> shade(const std::vector<Lights::GlobalLight>& globalLights, const std::vector<Lights::PointLight>& pointLights, const Math::vec3& cameraPos, const std::vector<Objects::Hit>& hits) noexcept;

			void disableAmbient() noexcept;

			void enableAmbient() noexcept;

			void disableDiffuse() noexcept;

			void enableDiffuse() noexcept;

			void disableSpecular() noexcept;

			void enableSpecular() noexcept;
	};
}

#endif //RAY_TRACER_BLINNPHONG_HPP
