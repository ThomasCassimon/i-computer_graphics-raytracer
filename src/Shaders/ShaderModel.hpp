/*
 * @author  thomas
 * @date    16/11/18
 * Copyright (c) 2018 thomas
 */

#ifndef RAY_TRACER_SHADER_HPP
#define RAY_TRACER_SHADER_HPP

namespace RayTracer::Shaders
{
	enum class ShaderModel
	{
		BLINN_PHONG,
		COOK_TORRANCE,
	};
}

#endif //RAY_TRACER_SHADER_HPP
