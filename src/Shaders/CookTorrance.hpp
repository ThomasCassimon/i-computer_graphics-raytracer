/*
 * @author  thomas
 * @date    16/11/18
 * Copyright (c) 2018 thomas
 */

#ifndef RAY_TRACER_COOKTORRANCE_HPP
#define RAY_TRACER_COOKTORRANCE_HPP

#include <vector>
#include <Lights/GlobalLight.hpp>
#include <Lights/PointLight.hpp>
#include <Objects/Hit.hpp>
#include <RayTracer.hpp>

namespace RayTracer::Shaders
{
	class CookTorrance
	{
		private:
			int maxRecursionDepth;
			bool ambient;
			bool diffuse;
			bool specular;
			Graphics::Color backgroundColor;
			Material environment;   //todo: Add to scene parser
			RayTracer& rayTracer;

			bool isCriticalAngle(const Objects::Hit& hit, const Objects::Ray& ray) const noexcept;

			Math::vec3 calculateAmbient(const std::vector<Lights::GlobalLight>& globalLights, const Math::vec3& refractionIndex) const noexcept;

			Math::vec3 calculateDiffuse(const std::vector<Lights::PointLight>& pointLights, const Objects::Hit& hit, float kDiffuse, const Math::vec3& refractionIndex) const noexcept;

			Math::vec3 calculateSpecular(const std::vector<Lights::PointLight>& pointLights, const Objects::Hit& hit, const Math::vec3& toCamera, float surfaceRoughness, const Math::vec3& refractionIndex, float kSpecular) const noexcept;

			Math::vec3 calculateReflection(const std::vector<Lights::GlobalLight>& globalLights, const std::vector<Lights::PointLight>& pointLights, const Objects::Hit& hit, const Objects::Ray& ray, int recursionDepth) noexcept;

			Math::vec3 calculateRefraction(const std::vector<Lights::GlobalLight>& globalLights, const std::vector<Lights::PointLight>& pointLights, const Objects::Hit& hit, const Objects::Ray& ray, int recursionDepth) noexcept;

			Graphics::Color shade(const std::vector<Lights::GlobalLight>& globalLights, const std::vector<Lights::PointLight>& pointLights, const Math::vec3& cameraPos, const Objects::Hit& hit, const Objects::Ray& ray, int recursionDepth) noexcept;

		public:
			explicit CookTorrance(RayTracer& rayTracer, const Graphics::Color& backgroundColor, int maxRecursionDepth) noexcept;

			std::vector<Graphics::Color> shade(const std::vector<Lights::GlobalLight>& globalLights, const std::vector<Lights::PointLight>& pointLights, const Math::vec3& cameraPos, const std::vector<Objects::Hit>& hits, const std::vector<Objects::Ray>& rays) noexcept;

			Math::vec3 fresnel(float phi, const Math::vec3& refractionIndex) const noexcept;

			void disableAmbient() noexcept;

			void enableAmbient() noexcept;

			void disableDiffuse() noexcept;

			void enableDiffuse() noexcept;

			void disableSpecular() noexcept;

			void enableSpecular() noexcept;
	};
}

#endif //RAY_TRACER_COOKTORRANCE_HPP
