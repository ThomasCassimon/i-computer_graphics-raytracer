/*
 * @author  thomas
 * @date    16/11/18
 * Copyright (c) 2018 thomas
 */

#include "BlinnPhong.hpp"

#include <algorithm>

namespace RayTracer::Shaders
{
	BlinnPhong::BlinnPhong(RayTracer& rayTracer, float specularPower, float specularStrength) noexcept: ambient(true), diffuse(true), specular(true), specularPower(specularPower), specularStrength(specularStrength), rayTracer(rayTracer)
	{
	}

	std::vector<Graphics::Color> BlinnPhong::shade(const std::vector<Lights::GlobalLight>& globalLights, const std::vector<Lights::PointLight>& pointLights, const Math::vec3& cameraPos, const std::vector<Objects::Hit>& hits) noexcept
	{
		std::vector<Graphics::Color> colors (hits.size());

		for (std::size_t i = 0; i < hits.size(); i++)
		{
			Objects::Hit hit = hits[i];
			const Math::vec3 toCamera = Math::normalize(hit.getPosition() - cameraPos);

			Math::vec3 ambient;
			Math::vec3 diffuse;
			Math::vec3 specular;

			if (this->ambient)
			{
				// Ambient
				for (const Lights::GlobalLight& light: globalLights)
				{
					ambient += light.getIntensity() * static_cast<Math::vec3>(light.getColor());
				}
			}

			if (this->diffuse || this->ambient)
			{
				std::vector<Lights::PointLight> visibleLights;

				std::copy_if(std::begin(pointLights), std::end(pointLights), std::back_inserter(visibleLights),
				[hit, this] (const Lights::PointLight& light)
				{
					const Objects::Ray shadowRay (light.getPosition(), Math::normalize(hit.getPosition() - light.getPosition()));

					std::optional<Objects::Hit> shadowHit = this->rayTracer.hits(shadowRay, light.getPosition(), std::vector<std::shared_ptr<Objects::Object>>());

					if (shadowHit)
					{
						const Math::vec3 adjustedHitPoint = hit.getPosition() + (hit.getNormal() * std::numeric_limits<float>::epsilon());
						const float distLightHit = Math::dist(light.getPosition(), adjustedHitPoint);
						const float distLightShadowHit = Math::dist(light.getPosition(), shadowHit->getPosition());

						return std::fabs(distLightHit - distLightShadowHit) < 1.0e-4;
					}
					else
					{
						// NOTE: This should never happen! It means that the hit we're tracing isn't even possible!
						// Unfortunately, om occassion, this does happen, no idea why... (Probably numerical errors)
						//std::stringstream stream;
						//stream << "Shadowray failed to hit any object. Shadowray: " << shadowRay;
						//throw std::runtime_error(stream.str());
						return true;
					}
				});

				for (const Lights::PointLight& light: visibleLights)
				{
					const Math::vec3 toLight = Math::normalize(light.getPosition() - hit.getPosition());

					// Diffuse
					if (this->diffuse)
					{
						const float diffFactor = std::max(Math::dot(toLight, hit.getNormal()), 0.0f);
						diffuse += diffFactor * static_cast<Math::vec3>(light.getColor());
					}

					// Specular
					if (this->specular)
					{
						const Math::vec3 reflection = Math::reflect(-toLight, hit.getNormal());
						const float dot = Math::dot(toCamera, reflection);
						const float specFactor = std::max(dot, 0.0f);
						const float specularFactor = this->specularStrength * std::pow(specFactor, this->specularPower);
						specular += specularFactor * static_cast<Math::vec3>(light.getColor());
					}
				}
			}

			Math::vec3 finalColor = (ambient + diffuse + specular) * static_cast<Math::vec3>(hit.getColor());
			finalColor = Math::clamp(finalColor, 0.0f, 1.0f);

			colors[i] = Graphics::Color(finalColor);
		}

		return colors;
	}

	void BlinnPhong::disableAmbient() noexcept
	{
		this->ambient = false;
	}

	void BlinnPhong::enableAmbient() noexcept
	{
		this->ambient = true;
	}

	void BlinnPhong::disableDiffuse() noexcept
	{
		this->diffuse = false;
	}

	void BlinnPhong::enableDiffuse() noexcept
	{
		this->diffuse = true;
	}

	void BlinnPhong::disableSpecular() noexcept
	{
		this->specular = false;
	}

	void BlinnPhong::enableSpecular() noexcept
	{
		this->specular = true;
	}
}