/*
 * @author  thomas
 * @date    07/12/18
 * Copyright (c) 2018 thomas
 */

#include "Material.hpp"

namespace RayTracer::Shaders
{
	Material::Material() noexcept: Material(0.5f, 0.5f, 0, Math::vec3(16.0f, 16.0f, 1.0f))
	{
	}

	Material::Material(const Json::Value& jsonMaterial)
	{
		Json::Value jsonName = jsonMaterial[NAME_KEY.data()];
		Json::Value jsonRoughness = jsonMaterial[ROUGHNESS_KEY.data()];
		Json::Value jsonRefractionIndex = jsonMaterial[REFRACTION_INDEX_KEY.data()];
		Json::Value jsonSpecular = jsonMaterial[SPECULAR_KEY.data()];
		Json::Value jsonReflectivity = jsonMaterial[REFLECTIVITY_KEY.data()];
		Json::Value jsonTransparency = jsonMaterial[TRANSPARENCY_KEY.data()];

		if (jsonName.isNull())
		{
			throw std::runtime_error("Material didn't contain name.");
		}

		const std::string name = jsonName.asString();

		if (jsonRoughness.isNull())
		{
			std::stringstream stream;
			stream << "Material \"" << name << "\" didn't contain roughness.";
			throw std::runtime_error(stream.str());
		}

		if (jsonSpecular.isNull())
		{
			std::stringstream stream;
			stream << "Material \"" << name << "\" didn't contain specular fraction.";
			throw std::runtime_error(stream.str());
		}

		if (jsonReflectivity.isNull())
		{
			std::stringstream stream;
			stream << "Material \"" << name << "\" didn't contain reflectivity.";
			throw std::runtime_error(stream.str());
		}

		if (jsonTransparency.isNull())
		{
			std::stringstream stream;
			stream << "Material \"" << name << "\" didn't contain transparency.";
			throw std::runtime_error(stream.str());
		}

		if (jsonRefractionIndex.isNull())
		{
			std::stringstream stream;
			stream << "Material \"" << name << "\" didn't contain refraction indices.";
			throw std::runtime_error(stream.str());
		}

		if (jsonRefractionIndex.size() != 3)
		{
			std::stringstream stream;
			stream << "Material \"" << name << "\" contained extra or is missing refraction index components. Got " << jsonRefractionIndex.size() << " components, but expected 3.";
			throw std::runtime_error(stream.str());
		}

		this->roughness = jsonRoughness.asFloat();
		this->kSpecular = jsonSpecular.asFloat();
		this->reflectivity = jsonReflectivity.asFloat();
		this->transparency = jsonTransparency.asFloat();

		float refractionIndices [3];

		for (int i = 0; i < jsonRefractionIndex.size(); ++i)
		{
			refractionIndices[i] = jsonRefractionIndex[i].asFloat();
		}

		this->refractionIndex = Math::vec3(refractionIndices[0], refractionIndices[1], refractionIndices[2]);
	}

	Material::Material(float specular, float roughness, float reflectivity, const Math::vec3& refractionIndex) noexcept: kSpecular(specular), roughness(roughness), reflectivity(reflectivity), refractionIndex(refractionIndex)
	{
	}

	float Material::getRoughness() const noexcept
	{
		return this->roughness;
	}

	float Material::getSpecular() const noexcept
	{
		return this->kSpecular;
	}

	float Material::getReflectivity() const noexcept
	{
		return this->reflectivity;
	}

	float Material::getTransparency() const noexcept
	{
		return this->transparency;
	}

	const Math::vec3& Material::getRefractionIndex() const noexcept
	{
		return this->refractionIndex;
	}
}