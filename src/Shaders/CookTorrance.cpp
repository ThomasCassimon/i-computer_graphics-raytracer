/*
 * @author  thomas
 * @date    16/11/18
 * Copyright (c) 2018 thomas
 */

#include "CookTorrance.hpp"

#include <algorithm>
#include <Math/Util.hpp>

namespace RayTracer::Shaders
{
	bool CookTorrance::isCriticalAngle(const Objects::Hit& hit, const Objects::Ray& ray) const noexcept
	{
		// m1 is the material we're coming from, m2 is the material we're entering
		Material m1;
		Material m2;

		// Even number of hits to the same object, we're coming from outside
		if ((ray.getSameObjectHitCount() % 2) == 0)
		{
			m1 = this->environment;
			m2 = hit.getMaterial();
		}
			// Odd number of hits to the same object, we're coming from inside
		else
		{
			m1 = hit.getMaterial();
			m2 = this->environment;
		}

		// Calculate average eta for both materials
		const float eta1 = (m1.getRefractionIndex().r + m1.getRefractionIndex().g + m1.getRefractionIndex().b) / 3.0f;
		const float eta2 = (m2.getRefractionIndex().r + m2.getRefractionIndex().g + m2.getRefractionIndex().b) / 3.0f;

		const float eta = eta1 / eta2;
		const float c1 =  Math::dot(hit.getNormal(), ray.getDirection());
		const float arg = 1.0f - (std::pow(eta, 2.0f) * (1.0f - std::pow(c1, 2.0f)));

		return arg < 0.0f;
	}

	Math::vec3 CookTorrance::calculateAmbient(const std::vector<Lights::GlobalLight>& globalLights, const Math::vec3& refractionIndex) const noexcept
	{
		Math::vec3 ambient;

		// Ambient
		for (const Lights::GlobalLight& light: globalLights)
		{
			// Light.getIntensity = kAmbient
			ambient += light.getIntensity() * static_cast<Math::vec3>(light.getColor()) * fresnel(0.0f, refractionIndex);
		}

		return ambient;
	}

	Math::vec3 CookTorrance::calculateDiffuse(const std::vector<Lights::PointLight>& pointLights, const Objects::Hit& hit, float kDiffuse, const Math::vec3& refractionIndex) const noexcept
	{
		Math::vec3 diffuse;
		const Math::vec3 normal = hit.getNormal();

		// Diffuse
		for (const Lights::PointLight& light : pointLights)
		{
			const Math::vec3 toLight = Math::normalize(light.getPosition() - hit.getPosition());
			const float sdotm   = Math::dot(toLight, normal);

			const float frac = sdotm / (toLight.length() * normal.length());

			const float lambert = std::max(0.0f, frac);

			diffuse += static_cast<Math::vec3>(light.getColor()) * light.getIntensity() * kDiffuse * lambert * fresnel(0.0f, refractionIndex);
		}

		return diffuse;
	}

	Math::vec3 CookTorrance::calculateSpecular(const std::vector<Lights::PointLight>& pointLights, const Objects::Hit& hit, const Math::vec3& toCamera, float surfaceRoughness, const Math::vec3& refractionIndex, float kSpecular) const noexcept
	{
		Math::vec3 specular;
		const Math::vec3 position = hit.getPosition();
		const Math::vec3 normal = hit.getNormal();

		// Specular
		for (const Lights::PointLight& light : pointLights)
		{
			// Setting up some vectors
			const Math::vec3 toLight = Math::normalize(light.getPosition() - position);
			const Math::vec3 halfway = Math::normalize(toCamera + normal);

			// Angle between toLight and halfway vector
			const float delta = Math::angle(toLight, halfway);

			// Microfacet Distribution
			const float D = Math::beckmann(delta, surfaceRoughness);

			// Geometry
			const float halfwayDottoLight = Math::dot(halfway, toLight);
			const float halfwayDotNormal  = Math::dot(normal, halfway);
			const float Gm = (2.0f * halfwayDotNormal * Math::dot(normal, toLight)) / halfwayDottoLight;
			const float Gs = (2.0f * halfwayDotNormal * Math::dot(normal, toCamera)) / halfwayDottoLight;
			const float G = std::min(1.0f, std::min(Gm, Gs));

			// Fresnel
			const float      phi = std::acos(Math::dot(normal, toLight));
			const Math::vec3 F   = fresnel(phi, refractionIndex);

			specular += (static_cast<Math::vec3>(light.getColor()) * light.getIntensity() * kSpecular * D * G * F) / Math::dot(normal, toCamera);
		}

		return specular;
	}

	Math::vec3 CookTorrance::fresnel(float phi, const Math::vec3& refractionIndex) const noexcept
	{
		const float cScalar = std::cos(phi);
		const Math::vec3 c = Math::vec3(cScalar);
		const Math::vec3 g = Math::sqrt((refractionIndex * refractionIndex) + Math::vec3((std::pow(cScalar, 2.0f) - 1.0f)));

		const Math::vec3 g_plus_c = g + c;
		const Math::vec3 g_minus_c = g - c;
		const Math::vec3 one = Math::vec3(1.0f);
		const Math::vec3 second_fraction = ((c * g_plus_c) - one) / ((c * g_minus_c) + one);

		return (1.0f / 2.0f) * ((g_minus_c * g_minus_c) / (g_plus_c * g_plus_c)) * (one + (second_fraction * second_fraction));
	}

	Math::vec3 CookTorrance::calculateReflection(const std::vector<Lights::GlobalLight>& globalLights, const std::vector<Lights::PointLight>& pointLights, const Objects::Hit& hit, const Objects::Ray& ray, int recursionDepth) noexcept
	{
		Math::vec3 reflectionColor (0.0f);
		const Material material = hit.getMaterial();

		// Reflect
		// Check if we're still allowed to recurse
		if ((0 < recursionDepth) && (0.01f < material.getReflectivity()))
		{
			// The reflected ray starts in the hitpoint
			const Math::vec3 reflectionRayOrigin = hit.getPosition() + (1.0e-4 * hit.getNormal());

			// And it travels along the reflection vector
			const Math::vec3 reflectionRayDirection = Math::reflect(ray.getDirection(), hit.getNormal());

			// Construct ray object
			const Objects::Ray reflectionRay (reflectionRayOrigin, reflectionRayDirection);

			const std::optional<Objects::Hit> reflectionHitOpt = this->rayTracer.hits(reflectionRay, reflectionRayOrigin, std::vector<std::shared_ptr<Objects::Object>>());

			if (reflectionHitOpt)
			{
				const Objects::Hit& reflectionHit = reflectionHitOpt.value();
				const float distance = Math::dist(hit.getPosition(), reflectionHit.getPosition());

				if (1.0e-4 < distance)
				{
					Graphics::Color color = this->shade(globalLights, pointLights, reflectionRayOrigin, reflectionHit, reflectionRay, recursionDepth - 1);
					reflectionColor = material.getReflectivity() * static_cast<Math::vec3>(color);
				}
			}
			else
			{
				reflectionColor = material.getReflectivity() * static_cast<Math::vec3>(this->backgroundColor);
			}
		}

		return reflectionColor;
	}

	Math::vec3 CookTorrance::calculateRefraction(const std::vector<Lights::GlobalLight>& globalLights, const std::vector<Lights::PointLight>& pointLights, const Objects::Hit& hit, const Objects::Ray& ray, int recursionDepth) noexcept
	{
		Math::vec3 refractionColor;

		const Material material = hit.getMaterial();

		// Refract
		// See https://www.scratchapixel.com/lessons/3d-basic-rendering/introduction-to-shading/reflection-refraction-fresnel
		// Check if we're still allowed to recurse
		if ((0 < recursionDepth) && (0.01f < hit.getMaterial().getTransparency()))
		{
			// m1 is the material we're coming from, m2 is the material we're entering
			Material m1;
			Material m2;

			// Even number of hits to the same object, we're coming from outside
			if ((ray.getSameObjectHitCount() % 2) == 0)
			{
				m1 = this->environment;
				m2 = hit.getMaterial();
			}
				// Odd number of hits to the same object, we're coming from inside
			else
			{
				m1 = hit.getMaterial();
				m2 = this->environment;
			}

			// Calculate average eta for both materials
			const float eta1 = (m1.getRefractionIndex().r + m1.getRefractionIndex().g + m1.getRefractionIndex().b) / 3.0f;
			const float eta2 = (m2.getRefractionIndex().r + m2.getRefractionIndex().g + m2.getRefractionIndex().b) / 3.0f;

			const float eta = eta1 / eta2;
			const float c1 =  Math::dot(hit.getNormal(), ray.getDirection());

			const float c2 = std::sqrt(1.0f - (std::pow(eta, 2.0f) * (1.0f - std::pow(c1, 2.0f))));

			const Math::vec3 refractionRayOrigin = hit.getPosition() + (1.0e-4 * -hit.getNormal());
			const Math::vec3 refractionRayDirection = (eta * ray.getDirection()) + (((eta * c1) - c2) * hit.getNormal());

			int sameObjectHitCount = ray.getSameObjectHitCount();

			if (ray.getLastObjectId() == hit.getObjectId())
			{
				++sameObjectHitCount;
			}
			else
			{
				sameObjectHitCount = 0;
			}

			const Objects::Ray refractionRay (hit.getObjectId(), sameObjectHitCount, refractionRayOrigin, refractionRayDirection);

			const std::optional<Objects::Hit> refractionHitOpt = this->rayTracer.hits(refractionRay, refractionRayOrigin, std::vector<std::shared_ptr<Objects::Object>>());

			refractionColor = material.getTransparency() * static_cast<Math::vec3>(this->backgroundColor);

			if (refractionHitOpt)
			{
				const Objects::Hit& refractionHit = refractionHitOpt.value();
				const float distance = Math::dist(hit.getPosition(), refractionHit.getPosition());

				if (1.0e-4 < distance)
				{
					Graphics::Color color = this->shade(globalLights, pointLights, refractionRayOrigin, refractionHit, refractionRay, recursionDepth - 1);
					refractionColor = material.getTransparency() * static_cast<Math::vec3>(color);
				}
			}
		}

		return refractionColor;
	}

	CookTorrance::CookTorrance(RayTracer& rayTracer, const Graphics::Color& backgroundColor, int maxRecursionDepth) noexcept:
	maxRecursionDepth(maxRecursionDepth),
	ambient(true),
	diffuse(true),
	specular(true),
	backgroundColor(backgroundColor),
	environment(0.0f, 0.0f, 0.0f, Math::vec3(1.0f)),
	rayTracer(rayTracer)
	{
	}

	Graphics::Color CookTorrance::shade(const std::vector<Lights::GlobalLight>& globalLights, const std::vector<Lights::PointLight>& pointLights, const Math::vec3& cameraPos, const Objects::Hit& hit, const Objects::Ray& ray, int recursionDepth) noexcept
	{
		const Material material = hit.getMaterial();
		const Math::vec3 toCamera = -ray.getDirection();

		std::vector<Lights::PointLight> visibleLights;

		std::copy_if(std::begin(pointLights), std::end(pointLights), std::back_inserter(visibleLights),
			[hit, this]
			(const Lights::PointLight& light)
			{
				const Objects::Ray shadowRay (light.getPosition(), Math::normalize(hit.getPosition() - light.getPosition()));

				std::optional<Objects::Hit> shadowHit = this->rayTracer.hits(shadowRay, light.getPosition(), {});

				if (shadowHit)
				{
					//const Math::vec3 adjustedHitPoint = hit.getPosition() + (hit.getNormal() * std::numeric_limits<float>::epsilon());
					const float distLightShadowHit = Math::dist(shadowHit->getPosition(), hit.getPosition());
					const float distLightHit = Math::dist(light.getPosition(), hit.getPosition());

					return (std::fabs(distLightShadowHit) < 1.0e-4) && (distLightHit < Math::dist(light.getPosition(), hit.getPosition()));
				}
				else
				{
					// NOTE: This should never happen! It means that the hit we're tracing isn't even possible!
					// Unfortunately, om occassion, this does happen, no idea why... (Probably numerical errors)
					//std::stringstream stream;
					//stream << "Shadowray failed to hit any object. Shadowray: " << shadowRay;
					//throw std::runtime_error(stream.str());
					return true;
				}
			});

		Math::vec3 ambient;
		Math::vec3 diffuse;
		Math::vec3 specular;
		Math::vec3 reflectionColor;
		Math::vec3 refractionColor;

		if (this->ambient)
		{
			ambient = this->calculateAmbient(globalLights, material.getRefractionIndex());
		}

		if (this->diffuse)
		{
			diffuse = this->calculateDiffuse(visibleLights, hit, 1.0f - material.getSpecular(), material.getRefractionIndex());
		}

		if (this->specular)
		{
			specular = this->calculateSpecular(visibleLights, hit, toCamera, material.getRoughness(), material.getRefractionIndex(), material.getSpecular());
		}

		reflectionColor = this->calculateReflection(globalLights, pointLights, hit, ray, recursionDepth);

		if (0.01f < hit.getMaterial().getTransparency())
		{
			if (!this->isCriticalAngle(hit, ray))
			{
				refractionColor = this->calculateRefraction(globalLights, pointLights, hit, ray, recursionDepth);
			}
			else
			{
				refractionColor = hit.getMaterial().getTransparency() * (1.0f / hit.getMaterial().getReflectivity()) * reflectionColor;
			}
		}

		Math::vec3 finalColor = Math::clamp(ambient + diffuse + specular + reflectionColor + refractionColor, 0.0f, 1.0f);

		return Graphics::Color(finalColor * (static_cast<Math::vec3>(hit.getColor())));
	}

	std::vector<Graphics::Color> CookTorrance::shade(const std::vector<Lights::GlobalLight>& globalLights, const std::vector<Lights::PointLight>& pointLights, const Math::vec3& cameraPos, const std::vector<Objects::Hit>& hits, const std::vector<Objects::Ray>& rays) noexcept
	{
		std::vector<Graphics::Color> colors (hits.size());

		for (std::size_t i = 0; i < hits.size(); ++i)
		{
			colors[i] = this->shade(globalLights, pointLights, cameraPos, hits[i], rays[i], this->maxRecursionDepth);
		}

		return colors;
	}

	void CookTorrance::disableAmbient() noexcept
	{
		this->ambient = false;
	}

	void CookTorrance::enableAmbient() noexcept
	{
		this->ambient = true;
	}

	void CookTorrance::disableDiffuse() noexcept
	{
		this->diffuse = false;
	}

	void CookTorrance::enableDiffuse() noexcept
	{
		this->diffuse = true;
	}

	void CookTorrance::disableSpecular() noexcept
	{
		this->specular = false;
	}

	void CookTorrance::enableSpecular() noexcept
	{
		this->specular = true;
	}
}