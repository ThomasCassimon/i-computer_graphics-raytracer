/*
 * @author thomas
 * @date   20/10/18
 * @file   Time.cpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#include "Time.hpp"

namespace RayTracer
{
	Time::Time(long milliseconds) noexcept: totalMilliseconds(milliseconds), ms(0), s(0), min(0)
	{
		long totalSeconds = (totalMilliseconds - (totalMilliseconds % 1000)) / 1000;
		long remainingMilliseconds = totalMilliseconds - (totalSeconds * 1000);

		long totalMinutes = (totalSeconds - (totalSeconds % 60)) / 60;
		long remainingSeconds = totalSeconds - (totalMinutes * 60);

		this->min = totalMinutes;
		this->s = remainingSeconds;
		this->ms = remainingMilliseconds;
	}

	long Time::getTotalMilliseconds() const noexcept
	{
		return this->totalMilliseconds;
	}

	std::ostream& operator<<(std::ostream& stream, const Time& time) noexcept
	{
		stream << std::setw(2) << std::setfill('0') << time.min << ":" << std::setw(2) << std::setfill('0') << time.s << "." << std::setw(3) << std::setfill('0') << time.ms;
		return stream;
	}
}