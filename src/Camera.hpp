/*
 * @author  thomas
 * @date    09/11/18
 * Copyright (c) 2018 thomas
 */

#ifndef RAY_TRACER_CAMERA_HPP
#define RAY_TRACER_CAMERA_HPP

#include <array>
#include <thread>
#include <vector>

#include <Math/vec3.hpp>
#include <Math/vec2.hpp>
#include <Objects/Ray.hpp>
#include <PixelOrder.hpp>

namespace RayTracer
{
	class Camera
	{
		private:
			constexpr static std::string_view IMAGE_SIZE_KEY = "image_size";
			constexpr static std::string_view POSITION_KEY = "position";
			constexpr static std::string_view DIRECTION_KEY = "direction";

			Math::ivec2 screenSize;
			Math::vec2 virtualScreenSize;
			Math::vec3 position;
			Math::vec3 direction;

		public:
			explicit Camera (const Json::Value& jsonCamera);

			Camera (const Math::ivec2& screenSize, const Math::vec3& position, const Math::vec3& direction) noexcept;

			std::vector<std::vector<Math::ivec2>> generatePixelList(PixelOrder order, long numThreads = std::thread::hardware_concurrency()) const;

			std::vector<std::vector<Objects::Ray>> generateRays(const std::vector<std::vector<Math::ivec2>>& pixels);

			const Math::ivec2& getScreenSize() const noexcept;

			long getNumPixels() const noexcept;

			const Math::vec3& getPosition() const noexcept;
	};
}

#endif //RAY_TRACER_CAMERA_HPP
