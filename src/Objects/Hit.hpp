/*
 * @author thomas
 * @date   05/10/18
 * @file   Hit.hpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#ifndef RAY_TRACER_HIT_HPP
#define RAY_TRACER_HIT_HPP

#include <memory>
#include <Graphics/Color.hpp>
#include <Math/vec3.hpp>
#include <Shaders/Material.hpp>

namespace RayTracer::Objects
{
	class Object;

	class Hit
	{
		private:
			long objectId;
			Math::vec3 pos;
			Math::vec3 normal;
			Graphics::Color color;
			Shaders::Material material;

		public:
			Hit() noexcept;

			Hit(long objectId, const Math::vec3& pos, const Math::vec3& normal, const Graphics::Color& color, const Shaders::Material& material) noexcept;

			Hit(long objectId, Math::vec3&& pos, Math::vec3&& normal, Graphics::Color&& color, Shaders::Material&& material) noexcept;

			long getObjectId() const noexcept;

			const Math::vec3& getPosition() const noexcept;

			const Math::vec3& getNormal() const noexcept;

			const Graphics::Color& getColor() const noexcept;

			const Shaders::Material& getMaterial() const noexcept;

			friend std::ostream& operator<< (std::ostream& stream, const Hit& hit) noexcept;
	};
}

#endif //RAY_TRACER_HIT_HPP
