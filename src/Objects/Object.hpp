/*
 * @author thomas
 * @date   05/10/18
 * @file   AbstractObject.hpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#ifndef RAY_TRACER_ABSTRACTOBJECT_HPP
#define RAY_TRACER_ABSTRACTOBJECT_HPP

#include <optional>

#include <json/json.h>

#include "Graphics/Samplable.hpp"
#include "Graphics/PixelBuffer.hpp"
#include "Math/Transformation.hpp"
#include "Objects/Hittable.hpp"

//todo: add material to object
namespace RayTracer::Objects
{
	class Object: public Hittable
	{
		private:
			constexpr static std::string_view ID_KEY = "id";
			constexpr static std::string_view MATERIAL_KEY = "material";
			constexpr static std::string_view COLOR_KEY = "color";
			constexpr static std::string_view TEXTURE_KEY = "texture";
			constexpr static std::string_view SOLID_TEXTURE_KEY = "solid_texture";

			Shaders::Material parseMaterial(const std::string& materialName) const;

			std::shared_ptr<Graphics::Samplable> parseColor (const Json::Value& colorValue) const;

			std::shared_ptr<Graphics::Samplable> parseTexture (const Json::Value& jsonTexture) const;

			std::shared_ptr<Graphics::Samplable> parseSolidTexture (const Json::Value& jsonSolidTexture) const;

			long id;

		protected:
			Math::Transformation transformation;
			Shaders::Material material;
			std::shared_ptr<Graphics::Samplable> color;

		public:
			Object() = delete;

			Object(long id) noexcept;

			explicit Object(const Json::Value& jsonObject);

			virtual ~Object() = default;

			long getId() const noexcept;

			virtual long getNumVertices() const noexcept;

			virtual void transform (const Math::Transformation& transform) noexcept;

			friend bool operator== (const Object& obj1, const Object& obj2) noexcept;

			friend bool operator!= (const Object& obj1, const Object& obj2) noexcept;
	};


}

#endif //RAY_TRACER_ABSTRACTOBJECT_HPP
