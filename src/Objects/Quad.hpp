/*
 * @author  thomas
 * @date    25/11/18
 * Copyright (c) 2018 thomas
 */

#ifndef RAY_TRACER_QUAD_HPP
#define RAY_TRACER_QUAD_HPP

#include <array>
#include <memory>

#include <Graphics/Samplable.hpp>
#include <Math/vec3.hpp>
#include <Objects/Object.hpp>
#include <Objects/Triangle.hpp>
#include <Math/vec2.hpp>

namespace RayTracer::Objects
{
	class Quad: public Object
	{
		private:
			constexpr static std::string_view ALLOW_BACKHITS_KEY = "allow_back_hits";
			constexpr static std::string_view VERTICES_KEY = "vertices";

			bool allowBackHits;
			std::array<Math::vec3, 4> vertices;

			// Lagae-Durnez Ray-Quad intersection
			Math::vec3 edge01;
			Math::vec3 edge02;
			Math::vec3 edge03;
			Math::vec3 edge21;
			Math::vec3 edge23;

			Math::vec2 bary11;

			Math::vec3 N;

		public:
			Quad() = delete;

			explicit Quad(const Json::Value& jsonQuad);

			std::optional<Hit> hit(Ray ray) const noexcept override;

			std::optional<Hit> hit(Ray ray, const Math::vec3& pixel) const noexcept override;

			long getNumVertices() const noexcept override;

			friend std::ostream& operator<< (std::ostream& stream, const Quad& quad) noexcept;
	};
}

#endif //RAY_TRACER_QUAD_HPP
