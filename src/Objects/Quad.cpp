/*
 * @author  thomas
 * @date    25/11/18
 * Copyright (c) 2018 thomas
 */

#include "Quad.hpp"

#include <sstream>

#include "Graphics/PixelBuffer.hpp"

namespace RayTracer::Objects
{
	Quad::Quad(const Json::Value &jsonQuad) : Object(jsonQuad), allowBackHits(),
																vertices(),
																edge01(),
																edge02(),
																edge03(),
																edge21(),
																edge23(),
																bary11(),
																N()
	{
		const Json::Value& jsonAllowBackHits = jsonQuad[ALLOW_BACKHITS_KEY.data()];
		const Json::Value& jsonVertices = jsonQuad[VERTICES_KEY.data()];

		if (jsonAllowBackHits.isNull())
		{
			this->allowBackHits = false;    // We use a default setting rather than throwing an exception, because it makes sense here
			//throw std::runtime_error("Quad didn't contain back hits preference.");
		}
		else
		{
			this->allowBackHits = jsonAllowBackHits.asBool();
		}

		if (jsonVertices.isNull())
		{
			throw std::runtime_error("Quad didn't contain vertices.");
		}

		if (jsonVertices.size() != 4)
		{
			std::stringstream stream;
			stream << "Quad vertices had incorrect size of ";
			stream << jsonVertices.size();
			stream << ", should be 4.";
			throw std::runtime_error(stream.str());
		}

		for (int i = 0; i < jsonVertices.size(); ++i)
		{
			this->vertices[i] = Math::vec3(jsonVertices[i]);
		}

		this->edge01 = vertices[1] - vertices[0];
		this->edge02 = vertices[2] - vertices[0];
		this->edge03 = vertices[3] - vertices[0];
		this->edge21 = vertices[1] - vertices[2];
		this->edge23 = vertices[3] - vertices[2];
		this->N = Math::cross(this->edge01, this->edge03);

		if ((std::fabs(this->N.x) >= std::fabs(this->N.y)) && (std::fabs(this->N.x) >= std::fabs(this->N.z)))
		{
			this->bary11.x = ((this->edge02.y * this->edge03.z) - (this->edge02.z * this->edge03.y)) / this->N.x;
			this->bary11.y = ((this->edge01.y * this->edge02.z) - (this->edge01.z * this->edge02.y)) / this->N.x;
		}
		else if ((std::fabs(this->N.y) >= std::fabs(this->N.x)) && (std::fabs(this->N.y) >= std::fabs(this->N.z)))
		{
			this->bary11.x = ((this->edge02.z * this->edge03.x) - (this->edge02.x * this->edge03.z)) / this->N.y;
			this->bary11.y = ((this->edge01.z * this->edge02.x) - (this->edge01.x * this->edge02.z)) / this->N.y;
		}
		else
		{
			this->bary11.x = ((this->edge02.x * this->edge03.y) - (this->edge02.y * this->edge03.x)) / this->N.z;
			this->bary11.y = ((this->edge01.x * this->edge02.y) - (this->edge01.y * this->edge02.x)) / this->N.z;
		}
	}

	std::optional<Hit> Quad::hit(Ray ray) const noexcept
	{
		const Math::vec3 P = Math::cross(ray.getDirection(), this->edge03);
		const float det = Math::dot(this->edge01, P);

		if (std::fabs(det) < std::numeric_limits<float>::epsilon())
		{
			return std::nullopt;
		}

		const Math::vec3 T = ray.getOffset() - this->vertices[0];
		const float alpha = Math::dot(T, P) / det;

		if (alpha < 0.0f)
		{
			return std::nullopt;
		}

		const Math::vec3 Q = Math::cross(T, this->edge01);
		const float beta = Math::dot(ray.getDirection(), Q) / det;

		if (beta < 0.0f)
		{
			return std::nullopt;
		}

		if ((alpha + beta) > 1.0f)
		{
			const Math::vec3 P_tick = Math::cross(ray.getDirection(), this->edge21);
			const float det_tick = Math::dot(this->edge23, P_tick);

			if (std::fabs(det_tick) < std::numeric_limits<float>::epsilon())
			{
				return std::nullopt;
			}

			const Math::vec3 T_tick = ray.getOffset() - this->vertices[2];
			const float alpha_tick = Math::dot(T_tick, P_tick) / det_tick;

			if (alpha_tick < 0)
			{
				return std::nullopt;
			}

			const Math::vec3 Q_tick = Math::cross(T_tick, this->edge23);
			const float beta_tick = Math::dot(ray.getDirection(), Q_tick);

			if (beta_tick < 0.0f)
			{
				return std::nullopt;
			}
		}

		Math::vec4 transformedNormal = this->transformation.forward(Math::vec4(this->N, 0.0f));
		Math::vec3 normal = Math::normalize(Math::vec3(transformedNormal.x, transformedNormal.y, transformedNormal.z));

		const float t = Math::dot(this->edge03, Q) / det;

		if ((!allowBackHits) && (t < 0.0f))
		{
			return std::nullopt;
		}
		else if (allowBackHits && (t < 0.0f))
		{
			normal = normal * -1.0f;
		}

		float u;
		float v;

		if (std::fabs(this->bary11.x - 1.0f) < std::numeric_limits<float>::epsilon())
		{
			u = alpha;

			if (std::fabs(this->bary11.y - 1.0f) < std::numeric_limits<float>::epsilon())
			{
				v = beta;
			}
			else
			{
				v = beta / (u * (this->bary11.y - 1.0f) + 1.0f);
			}
		}
		else if (std::fabs(this->bary11.y - 1.0f) < std::numeric_limits<float>::epsilon())
		{
			v = beta;
			u = alpha / (v * (this->bary11.x - 1.0f) + 1.0f);
		}
		else
		{
			const float A = -(this->bary11.y - 1.0f);
			const float B = (alpha * (this->bary11.y - 1.0f)) - (beta * (this->bary11.x - 1.0f)) - 1.0f;
			const float C = alpha;

			auto sign = [](float f)
				{
					if (f < -std::numeric_limits<float>::epsilon())
					{
						return -1;
					}
					else if (std::fabs(f) < std::numeric_limits<float>::epsilon())
					{
						return 0;
					}
					else
					{
						return 1;
					}
				};

			const float delta = std::pow(B, 2.0f) - (4.0f * A * C);

			const float Q_scal = -0.5f * (B + (sign(B) * std::sqrt(delta)));

			u = Q_scal / A;

			if ((u < 0.0f) || (u > 1.0f))
			{
				u = C / Q_scal;
			}

			v = this->bary11.y / ((u * (this->bary11.x - 1.0f)) + 1.0f);
		}

		const Math::vec3 position = ray.getOffset() + (t * ray.getDirection());

		return std::make_optional<Hit>(this->getId(), position, normal, this->color->sample(Math::vec3(u, 0.0f, v)), this->material);
	}

	std::optional<Hit> Quad::hit(Ray ray, const Math::vec3& pixel) const noexcept
	{
		return this->hit(ray);
	}

	long Quad::getNumVertices() const noexcept
	{
		return 4;
	}

	std::ostream& operator<<(std::ostream& stream, const Quad& quad) noexcept
	{
		stream << '{';

		for (std::size_t i = 0; i < quad.vertices.size(); ++i)
		{
			stream << quad.vertices[i];

			if ((i + 1) != quad.vertices.size())
			{
				stream << ", ";
			}
		}

		stream << '}';

		return stream;
	}
}