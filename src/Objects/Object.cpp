/*
 * @author  thomas
 * @date    07/12/18
 * Copyright (c) 2018 thomas
 */
#include "Object.hpp"
#include <fstream>
#include <sstream>
#include <Graphics/SolidTexture.hpp>
#include "Math/vec2.hpp"

namespace RayTracer::Objects
{
	Shaders::Material Object::parseMaterial(const std::string& materialName) const
	{
		const std::string materialFile ("res/materials/" + materialName + ".json");

		Json::Value root;

		std::ifstream filestream(materialFile, std::ios::binary);

		if (!filestream.is_open())
		{
			throw std::runtime_error("Failed to open material file \"" + materialFile + "\".");
		}

		filestream >> root;

		return Shaders::Material(root);
	}

	std::shared_ptr<Graphics::Samplable> Object::parseColor(const Json::Value& jsonColour) const
	{
		if (jsonColour.isNull())
		{
			throw std::runtime_error("Object didn't contain color.");
		}

		Graphics::Color color (jsonColour);

		return std::make_shared<Graphics::PixelBuffer>(Math::ivec2(1,1), color);
	}

	std::shared_ptr<Graphics::Samplable> Object::parseTexture(const Json::Value& jsonTexture) const
	{
		if (jsonTexture.isNull())
		{
			throw std::runtime_error("Object didn't contain texture.");
		}

		const std::string textureFile = jsonTexture.asString();
		return std::make_shared<Graphics::PixelBuffer>(Graphics::PixelBuffer::load(textureFile));
	}

	std::shared_ptr<Graphics::Samplable> Object::parseSolidTexture(const Json::Value& jsonSolidTexture) const
	{
		if (jsonSolidTexture.isNull())
		{
			throw std::runtime_error("Object didn't contain solid texture.");
		}

		Graphics::SolidTexture solidTexture (jsonSolidTexture);
		return std::make_shared<Graphics::SolidTexture>(solidTexture);
	}

	Object::Object(long id) noexcept: id(id), transformation(), material(), color(nullptr)
	{
	}

	Object::Object(const Json::Value& jsonObject): id(-1), transformation(), material(), color(nullptr)
	{
		const Json::Value& jsonId = jsonObject[ID_KEY.data()];
		const Json::Value& jsonMaterialName = jsonObject[MATERIAL_KEY.data()];
		const Json::Value& jsonTexture = jsonObject[TEXTURE_KEY.data()];
		const Json::Value& jsonSolidTexture = jsonObject[SOLID_TEXTURE_KEY.data()];
		const Json::Value& jsonColor = jsonObject[COLOR_KEY.data()];

		if (jsonId.isNull())
		{
			throw std::runtime_error("Object didn't contain ID.");
		}

		this->id = jsonId.asLargestInt();

		if (jsonMaterialName.isNull())
		{
			std::stringstream stream;
			stream << "Object " << this->id << " didn't contain material.";
			throw std::runtime_error(stream.str());
		}

		if (!jsonTexture.isNull())
		{
			this->color = this->parseTexture(jsonTexture);
		}
		else if (!jsonSolidTexture.isNull())
		{
			this->color = this->parseSolidTexture(jsonSolidTexture);
		}
		else if (!jsonColor.isNull())
		{
			this->color = this->parseColor(jsonColor);
		}
		else
		{
			Graphics::Color color (static_cast<std::uint8_t>(255), static_cast<std::uint8_t>(255), static_cast<std::uint8_t>(255));
			this->color = std::make_shared<Graphics::PixelBuffer>(Math::ivec2(1,1), color);
		}

		/*
		if (this->getId() == 3)
		{
			Graphics::PixelBuffer* buf = dynamic_cast<Graphics::PixelBuffer*>(this->color.get());
			std::cout << "buffer size: " << buf->rows() << " x " << buf->cols() << std::endl;
		}*/

		this->material = this->parseMaterial(jsonMaterialName.asString());
	}

	long Object::getId() const noexcept
	{
		return this->id;
	}

	long Object::getNumVertices() const noexcept
	{
		return 0;
	}

	void Object::transform(const Math::Transformation& transform) noexcept
	{
		this->transformation *= transform;
	}

	bool operator==(const Object& obj1, const Object& obj2) noexcept
	{
		return obj1.id == obj2.id;
	}

	bool operator!=(const Object& obj1, const Object& obj2) noexcept
	{
		return !(obj1 == obj2);
	}
}