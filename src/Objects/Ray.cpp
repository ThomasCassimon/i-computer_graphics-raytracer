/*
 * @author thomas
 * @date   05/10/18
 * @file   Ray.cpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#include "Ray.hpp"

namespace RayTracer::Objects
{
	long Ray::getLastObjectId() const noexcept
	{
		return this->lastObjectId;
	}

	int Ray::getSameObjectHitCount() const noexcept
	{
		return this->sameObjectHitCount;
	}

	Math::vec3 Ray::getOffset() const noexcept
	{
		return this->off;
	}

	Math::vec3 Ray::getDirection() const noexcept
	{
		return this->dir;
	}

	std::ostream& operator<< (std::ostream& stream, const Ray& ray) noexcept
	{
		stream << "Offset: " << ray.off << ", Direction: " << ray.dir;
		return stream;
	}
}