/*
 * @author thomas
 * @date   05/10/18
 * @file   Plane.cpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#include "Plane.hpp"
#include "Math/Util.hpp"

namespace RayTracer::Objects
{
	Plane::Plane(const Json::Value &jsonPlane) : Object(jsonPlane), p0(), n(), ndotp0()
	{
		const Json::Value& jsonPoint = jsonPlane[POINT_KEY.data()];
		const Json::Value& jsonNormal = jsonPlane[NORMAL_KEY.data()];

		if (jsonPoint.isNull())
		{
			throw std::runtime_error("Plane didn't contain point.");
		}

		if (jsonNormal.isNull())
		{
			throw std::runtime_error("Plane didn't contain normal.");
		}

		this->p0 = Math::vec3(jsonPoint);
		this->n = Math::vec3(jsonNormal);
		this->ndotp0 = Math::dot(this->n, this->p0);
	}

	Plane::Plane(long id) noexcept : Object(id), p0(0.0f, 0.0f, 0.0f), n(0.0f, 1.0f, 0.0f), ndotp0(Math::dot(n, p0))
	{
		// Precompute ndotp0
	}

	Plane::Plane(long id, const Math::vec3& p0, const Math::vec3& n, std::shared_ptr<Graphics::Samplable> color, Shaders::Material material) noexcept: Object(id), p0(p0), n(Math::normalize(n)), ndotp0(Math::dot(n, p0))
	{
		// Precompute ndotp0
		this->color = color;
		this->material = material;
	}

	std::optional<Hit> Plane::hit(Ray ray) const noexcept
	{
		const Math::vec4 offset4 = this->transformation.inverse(Math::vec4(ray.getOffset(), 1.0f));
		const Math::vec4 direction4 = this->transformation.inverse(Math::vec4(ray.getDirection(), 0.0f));

		const Math::vec3 offset (offset4.x, offset4.y, offset4.z);
		const Math::vec3 direction (direction4.x, direction4.y, direction4.z);

		const float ndotOff = Math::dot(this->n, offset);
		const float denominator = Math::dot(this->n, direction);

		if (std::fabs(denominator) < std::numeric_limits<float>::epsilon())
		{
			return std::nullopt;
		}

		const float numerator = this->ndotp0 - ndotOff;
		const float t = numerator / denominator;

		if (std::fabs(t) < std::numeric_limits<float>::epsilon())
		{
			return std::nullopt;

		}
		else
		{
			const Math::vec3 hitpoint = ray.findPoint(t);

			return std::make_optional<Hit>(this->getId(), hitpoint, this->n, this->color->sample(hitpoint), this->material);
		}
	}

	std::optional<Hit> Plane::hit(Ray ray, const Math::vec3& pixel) const noexcept
	{
		return this->hit(ray);
	}

	long Plane::getNumVertices() const noexcept
	{
		return 2;
	}
}