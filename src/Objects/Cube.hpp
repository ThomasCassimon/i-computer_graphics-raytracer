/*
 * @author thomas
 * @date 12/25/18.
 */

#ifndef RAY_TRACER_CUBE_HPP
#define RAY_TRACER_CUBE_HPP

#include "Objects/Object.hpp"
#include "Math/vec3.hpp"

namespace RayTracer::Objects
{
	class Cube: public Object
	{
		private:
			constexpr static std::string_view CENTER_KEY = "center";
			constexpr static std::string_view SIZE_KEY = "size";

			Math::vec3 center;
			Math::vec3 size;
			Math::vec3 min;
			Math::vec3 max;

		public:
			Cube (const Json::Value& jsonCube);

			std::optional<Hit> hit(Ray ray) const  noexcept override;

			std::optional<Hit> hit(Ray ray, const Math::vec3& pixel) const noexcept override;

			long getNumVertices() const noexcept override;
	};
}

#endif //RAY_TRACER_CUBE_HPP
