/*
 * @author  thomas
 * @date    23/11/18
 * Copyright (c) 2018 thomas
 */

#ifndef RAY_TRACER_TRIANGLE_HPP
#define RAY_TRACER_TRIANGLE_HPP

#include <array>
#include <memory>
#include <vector>

#include <Graphics/Samplable.hpp>
#include <Math/vec3.hpp>
#include <Math/Transformation.hpp>
#include <Objects/Object.hpp>

namespace RayTracer::Objects
{
	/**
	 * Triangle class that uses Möller-Trumbore intersection algorithm to quickly check if a ray intersects a triangle
	 */
	class Triangle: public Objects::Object
	{
		private:
			constexpr static std::string_view ALLOW_BACK_HITS_KEY = "allow_back_hits";
			constexpr static std::string_view VERTICES_KEY = "vertices";

			bool allowBackHits;
			Math::vec3 edge1;
			Math::vec3 edge2;
			Math::vec3 normal;
			std::array<Math::vec3, 3> vertices;

			constexpr inline bool inside(float b1, float b2) const noexcept
			{
				return (
					(0.0f <= b1) &&
					(0.0f <= b2) &&
					((b1 + b2) <= 1.0f));
			}

		public:
			explicit Triangle(const Json::Value& jsonTriangle);

			explicit Triangle(long id, const std::array<Math::vec3, 3>& vertices, bool allowBackHit = false) noexcept;

			std::optional<Hit> hit(Ray ray) const noexcept override;

			std::optional<Hit> hit(Ray ray, const Math::vec3& pixel) const noexcept override;

			long getNumVertices() const noexcept override;
	};
}

#endif //RAY_TRACER_TRIANGLE_HPP
