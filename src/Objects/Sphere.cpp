/*
 * @author thomas
 * @date   25/10/18
 * @file   Sphere.cpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#include "Sphere.hpp"

#include "Math/Util.hpp"
#include "Math/vec2.hpp"
#include "Math/TransformFactory.hpp"
#include "Graphics/PixelBuffer.hpp"

namespace RayTracer::Objects
{
	Sphere::Sphere(const Json::Value& jsonSphere) : Object(jsonSphere)
	{
		const Json::Value& jsonCenter = jsonSphere[CENTER_KEY.data()];
		const Json::Value& jsonRadius = jsonSphere[RADIUS_KEY.data()];

		if (jsonRadius.isNull())
		{
			throw std::runtime_error("Sphere didn't contain radius.");
		}

		const float radius = jsonRadius.asFloat();

		if (jsonCenter.isNull())
		{
			throw std::runtime_error("Sphere didn't contain center.");
		}

		const Math::vec3 center (jsonCenter);

		Math::TransformFactory factory;
		this->transform(factory.scale(Math::vec3(radius)));
		this->transform(factory.translate(center));
	}

	Sphere::Sphere(long id, float radius) noexcept: Object(id)
	{
		Math::TransformFactory factory;
		this->transform(factory.scale(Math::vec3(radius)));
	}

	std::optional<Hit> Sphere::hit(Ray ray) const noexcept
	{
		Math::vec4 off = Math::vec4(ray.getOffset(), 1.0f);
		Math::vec4 dir = Math::vec4(ray.getDirection(), 0.0f);

		off = this->transformation.inverse(off);
		dir = this->transformation.inverse(dir);

		ray = Ray(Math::vec3(off.x, off.y, off.z), Math::normalize(Math::vec3(dir.x, dir.y, dir.z)));

		const float cdotc = Math::dot(ray.getDirection(), ray.getDirection());	// A
		const float sdotc = Math::dot(ray.getDirection(), ray.getOffset());		// B
		const float sdots = Math::dot(ray.getOffset(), ray.getOffset());		// C + R²
		const float disc = std::pow(sdotc, 2.0f) - (cdotc * (sdots - 1.0f));

		if (disc < 0.0f)
		{
			return std::nullopt;
		}
		else
		{
			// We had a proper hit, determine k
			float k;
			float normalMultiplier = 1.0f;

			if (std::fabs(disc) < std::numeric_limits<float>::epsilon())
			{
				k = -sdotc / cdotc;

				if (k < 0)
				{
					return std::nullopt;
				}
			}
			else
			{
				const float k1 = (-sdotc - std::sqrt(disc)) / (cdotc);
				const float k2 = (-sdotc + std::sqrt(disc)) / (cdotc);
				//k = ((k1 < 0) && (k2 > 0)) ? k2 : ((k2 < 0) && (k1 > 0)) ? k1 : ((k1 > 0) && (k2 > 0)) && (k1 < k2) ? k1 : k2;    // TODO: This breaks reflection calculations, find out why?
																																	// This was changed in this commit: https://bitbucket.org/ThomasCassimon/i-computer_graphics-raytracer/commits/1876645ee449a3aa0cf047d113f9f68387a77d10

				if ((k1 < 0.0f) && (k2 < 0.0f))
				{
					return std::nullopt;
				}

				if ((k1 < 0.0f) && (0.0f < k2))
				{
					k = k2;
					normalMultiplier = -1.0f;
				}
				else
				{
					k = k1 < k2 ? k1 : k2;
				}
			}

			// Determine hitpoint
			const Math::vec3 hitPoint = ray.getOffset() + (k * ray.getDirection());

			// Determine UV-coordinate of the hit
			const Math::vec2 uv = Math::vec2((std::asin(hitPoint.x) / Math::PI) + 0.5f, (std::asin(hitPoint.y) / Math::PI) + 0.5f);

			// Transform the hitpoint into worldspace and make sure coordinates are homogeneous
			Math::vec4 hitPointWorldspace = this->transformation.forward(Math::vec4(hitPoint, 1.0f));

			// Transform the normal into worldspace
			const Math::vec4 normalWorldSpace = this->transformation.forward(Math::vec4(hitPoint, 1.0f));

			// Normalize normal and extract first 3 coordinates
			const Math::vec3 hitNormal = normalMultiplier * Math::normalize(Math::vec3(normalWorldSpace.x, normalWorldSpace.y, normalWorldSpace.z));

			return std::make_optional<Hit>(this->getId(), Math::vec3(hitPointWorldspace.x, hitPointWorldspace.y, hitPointWorldspace.z), hitNormal, this->color->sample(Math::vec3(uv, 0.0f)), this->material);
		}
	}

	std::optional<Hit> Sphere::hit(Ray ray, const Math::vec3& pixel) const noexcept
	{
		return this->hit(ray);
	}

	long Sphere::getNumVertices() const noexcept
	{
		return 1L;
	}
}