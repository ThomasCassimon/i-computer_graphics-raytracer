/*
 * @author thomas
 * @date   12/10/18
 * @file   Mesh.cpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#include "Mesh.hpp"

#include "Graphics/PixelBuffer.hpp"
#include "Math/Util.hpp"
#include "Objects/COLLADA/Parser.hpp"

namespace RayTracer::Objects
{
	Mesh::Mesh(const Json::Value& jsonMesh): Object(jsonMesh), boundingSphere(-1), triangles()
	{
		const Json::Value& jsonModelFile = jsonMesh[MODEL_FILE_KEY.data()];

		if (jsonModelFile.isNull())
		{
			throw std::runtime_error("Mesh didn't have filename.");
		}

		const std::string filename = jsonModelFile.asString();

		Objects::COLLADA::Parser parser;
		std::vector<RayTracer::Objects::Vertex> vertices = parser.parse(filename);

		float maxLen = std::numeric_limits<float>::min();
		std::size_t triangleIdx = 0;
		for (std::size_t i = 0; i < vertices.size(); i += 3)
		{
			this->triangles[triangleIdx] = Objects::Triangle(this->getId(), std::array<Math::vec3, 3>({vertices[i].position, vertices[i + 1].position, vertices[i + 2].position}));

			++triangleIdx;

			if (vertices[i].position.length() > maxLen)
			{
				maxLen = vertices[i].position.length();
			}
		}

		this->boundingSphere = Sphere(this->getId(), maxLen);
	}

	std::optional<Hit> Mesh::hit(Ray ray) const noexcept
	{
		throw std::runtime_error("Calling hit(const Ray& ray) on Mesh is not supported because it prevents the mesh from doing Z-testing.");
	}

	std::optional<Hit> Mesh::hit(Ray ray, const Math::vec3& pixel) const noexcept
	{
		std::optional<Hit> boundingSphereHit = this->boundingSphere.hit(ray);

		if (!boundingSphereHit)
		{
			return std::nullopt;
		}

		std::optional<Hit> shortestHit = std::nullopt;
		float dist = -1.0f;
		float finalDist = std::numeric_limits<float>::max();

		std::optional<Hit> hit = std::nullopt;
		for (const Triangle& triangle : this->triangles)
		{
			 hit = triangle.hit(ray);

			if (hit)
			{
				dist = Math::dist(hit->getPosition(), pixel);

				if (dist < finalDist)
				{
					finalDist = dist;
					shortestHit = hit;
				}
			}
		}

		if (!shortestHit)
		{
			return std::nullopt;
		}

		return std::make_optional<Objects::Hit>(this->getId(), shortestHit->getPosition(), shortestHit->getNormal(), this->color->sample(shortestHit->getPosition()), this->material);
	}

	long Mesh::getNumVertices() const noexcept
	{
		return this->triangles.size() * 3;
	}
}