/*
 * @author thomas
 * @date   25/10/18
 * @file   Sphere.hpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#ifndef RAY_TRACER_SPHERE_HPP
#define RAY_TRACER_SPHERE_HPP

#include <memory>

#include <Graphics/Samplable.hpp>
#include <Math/vec3.hpp>
#include <Math/Transformation.hpp>
#include <Objects/Object.hpp>
#include <Objects/Hittable.hpp>
#include <Shaders/Material.hpp>

namespace RayTracer::Objects
{
	class Sphere: public Object
	{
		private:
			constexpr static std::string_view CENTER_KEY = "center";
			constexpr static std::string_view RADIUS_KEY = "radius";

		public:
			explicit Sphere(long id, float radius = 1.0f) noexcept;

			explicit Sphere(const Json::Value& jsonSphere);

			std::optional<Hit> hit(Ray ray) const noexcept override;

			[[deprecated]]
			std::optional<Hit> hit(Ray ray, const Math::vec3& pixel) const noexcept override;

			long getNumVertices() const noexcept override;
	};
}

#endif //RAY_TRACER_SPHERE_HPP
