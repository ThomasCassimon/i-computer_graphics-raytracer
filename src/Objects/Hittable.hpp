//
// Created by thomas on 01/11/18.
//

#ifndef RAY_TRACER_HITTABLE_HPP
#define RAY_TRACER_HITTABLE_HPP

#include <optional>
#include <Objects/Hit.hpp>
#include <Objects/Ray.hpp>
#include <Math/vec3.hpp>

namespace RayTracer::Objects
{
	class Hittable
	{
		public:
			virtual std::optional <Hit> hit(Ray ray) const noexcept = 0;

			virtual std::optional <Hit> hit(Ray ray, const Math::vec3 &pixel) const noexcept = 0;
	};
}
#endif //RAY_TRACER_HITTABLE_HPP
