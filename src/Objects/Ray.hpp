/*
 * @author thomas
 * @date   05/10/18
 * @file   Ray.hpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#ifndef RAY_TRACER_RAY_HPP
#define RAY_TRACER_RAY_HPP

#include "Math/vec3.hpp"
#include "Shaders/Material.hpp"


namespace RayTracer::Objects
{
	class Ray
	{
		private:
			long lastObjectId;
			int sameObjectHitCount;
			Math::vec3 off;
			Math::vec3 dir;

		public:
			Ray() noexcept: lastObjectId(-1), sameObjectHitCount(0), off(0.0), dir(1.0)
			{}

			Ray(Math::vec3 offset, Math::vec3 dir) noexcept: lastObjectId(-1), sameObjectHitCount(0), off(std::move(offset)), dir(std::move(dir))
			{}

			Ray(long lastObjectId, int sameObjectHitCount, Math::vec3 offset, Math::vec3 dir) noexcept: lastObjectId(lastObjectId), sameObjectHitCount(sameObjectHitCount), off(std::move(offset)), dir(std::move(dir))
			{}

			inline Math::vec3 findPoint (float k) const noexcept
			{
				return (this->off) + (k * this->dir);
			}

			long getLastObjectId() const noexcept;

			int getSameObjectHitCount() const noexcept;

			Math::vec3 getOffset() const noexcept;

			Math::vec3 getDirection() const noexcept;

			friend std::ostream& operator<< (std::ostream& stream, const Ray& ray) noexcept;
	};
}

#endif //RAY_TRACER_RAY_HPP
