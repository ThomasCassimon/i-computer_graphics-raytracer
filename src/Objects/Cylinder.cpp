/*
 * @author thomas
 * @date 12/25/18.
 */

#include "Cylinder.hpp"
#include "Math/TransformFactory.hpp"
#include "Math/Util.hpp"

namespace RayTracer::Objects
{
	std::optional<Hit> Cylinder::topHit(Ray ray) const noexcept
	{
		std::optional<Hit> topHit = this->top.hit(ray);

		if (!topHit)
		{
			return std::nullopt;
		}

		const float radius2 = std::pow(this->s, 2.0f);
		Math::vec4 topHitInv = this->transformation.inverse(Math::vec4(topHit->getPosition(), 1.0f));

		if (radius2 < (std::pow(topHitInv.x, 2.0f) + std::pow(topHitInv.z, 2.0f)))
		{
			return std::nullopt;
		}

		return std::make_optional<Objects::Hit>(this->getId(), topHit->getPosition(), topHit->getNormal(), this->color->sample(topHit->getPosition()), this->material);
	}

	std::optional<Hit> Cylinder::bottomHit(Ray ray) const noexcept
	{
		std::optional<Hit> bottomHit = this->bottom.hit(ray);

		if (!bottomHit)
		{
			return std::nullopt;
		}

		Math::vec4 bottomHitInv = this->transformation.inverse(Math::vec4(bottomHit->getPosition(), 1.0f));

		if (1.0f < (std::pow(bottomHitInv.x, 2.0f) + std::pow(bottomHitInv.z, 2.0f)))
		{
			return std::nullopt;
		}

		return std::make_optional<Objects::Hit>(this->getId(), bottomHit->getPosition(), bottomHit->getNormal(), this->color->sample(bottomHit->getPosition()), this->material);
	}

	std::optional<Hit> Cylinder::mantleHit(Ray ray) const noexcept
	{
		const Math::vec4 offset4 = this->transformation.inverse(Math::vec4(ray.getOffset(), 1.0f));
		const Math::vec4 direction4 = this->transformation.inverse(Math::vec4(ray.getDirection(), 0.0f));

		const Math::vec3 offset (offset4.x, offset4.y, offset4.z);
		const Math::vec3 direction (direction4.x, direction4.y, direction4.z);

		const float d = (this->s - 1.0f) * direction.y;
		const float F = 1.0f + ((this->s - 1.0f) * offset.y);

		const float a = (direction.x * direction.x) + (direction.z * direction.z) - (d * d);
		const float b = 2.0f * ((offset.x * direction.x) + (offset.z * direction.z) - (F * d));
		const float c = (offset.x * offset.x) + (offset.z * offset.z) - (F * F);

		const float disc = (b * b) - (4.0f * a * c);
		float t = 0.0f;

		if (disc < -std::numeric_limits<float>::epsilon())
		{
			return std::nullopt;
		}
		else if (std::fabs(disc) < std::numeric_limits<float>::epsilon())
		{
			t = -b / (2.0f * a);
		}
		else
		{
			// We had a proper hit, determine t
			const float t1 = (-b + std::sqrt(disc)) / (2.0f * a);
			const float t2 = (-b - std::sqrt(disc)) / (2.0f * a);

			if ((t1 < 0.0f) && (t2 < 0.0f))
			{
				return std::nullopt;
			}
			else if ((t1 < 0.0f) && (0.0f < t2))
			{
				t = t2;
			}
			else if ((t2 < 0.0f) && (0.0f < t1))
			{
				t = t1;
			}
			else
			{
				t = t1 < t2 ? t1 : t2;
			}
		}

		// Worldspace hitpoint
		const Math::vec3 hitPoint = ray.findPoint(t);

		// Hit-space hitpoint
		const Math::vec3 hitSpaceHitPoint = offset + (t * direction);

		if ((0.0f < hitSpaceHitPoint.y) && (hitSpaceHitPoint.y < 1.0f))
		{
			Math::vec3 normal (hitSpaceHitPoint);
			normal.y = -(this->s - 1.0f) * (1 + ((this->s - 1.0f) * hitSpaceHitPoint.y));

			return std::make_optional<Hit>(this->getId(), hitPoint, Math::normalize(normal), this->color->sample(hitPoint), this->material);
		}
		else
		{
			return std::nullopt;
		}
	}

	Cylinder::Cylinder(const Json::Value& jsonCylinder): Object(jsonCylinder), top(-1), bottom(-1)
	{
		const Json::Value jsonBase = jsonCylinder[BASE_KEY.data()];
		const Json::Value jsonS = jsonCylinder[S_KEY.data()];

		if (jsonBase.isNull())
		{
			throw std::runtime_error("Cylinder didn't contain base.");
		}

		if (jsonS.isNull())
		{
			throw std::runtime_error("Cylinder didn't contain s.");
		}

		this->s = jsonS.asFloat();
		this->top = Plane(this->getId(), Math::vec3(0.0f, 1.0f, 0.0f), Math::vec3(0.0f, 1.0f, 0.0f), this->color, this->material);
		this->bottom = Plane(this->getId(), Math::vec3(0.0f, 0.0f, 0.0f), Math::vec3(0.0f, -1.0f, 0.0f), this->color, this->material);

		Math::TransformFactory factory;
		const Math::Transformation baseTranslation = factory.translate(Math::vec3(jsonBase));

		this->transform(baseTranslation);
	}

	//https://stackoverflow.com/questions/35164707/ray-cylinder-intersection
	std::optional<Hit> Cylinder::hit(Ray ray) const noexcept
	{
		std::optional<Hit> topHit = this->topHit(ray);
		std::optional<Hit> bottomHit = this->bottomHit(ray);
		std::optional<Hit> mantleHit = this->mantleHit(ray);

		if (!topHit && !bottomHit && !mantleHit)
		{
			return std::nullopt;
		}

		float topDist = topHit ? Math::dist(topHit->getPosition(), ray.getOffset()) : std::numeric_limits<float>::max();
		float bottomDist = bottomHit ? Math::dist(bottomHit->getPosition(), ray.getOffset()) : std::numeric_limits<float>::max();
		float mantleDist = mantleHit ? Math::dist(mantleHit->getPosition(), ray.getOffset()) : std::numeric_limits<float>::max();

		if ((topDist < bottomDist) && (topDist < mantleDist))
		{
			return topHit;
		}
		else if ((bottomDist < topDist) && (bottomDist < mantleDist))
		{
			return bottomHit;
		}
		else if ((mantleDist < bottomDist) && (mantleDist < topDist))
		{
			return mantleHit;
		}
		else
		{
			return std::nullopt;
		}
	}

	std::optional<Hit> Cylinder::hit(Ray ray, const Math::vec3& pixel) const noexcept
	{
		return this->hit(ray);
	}

	long Cylinder::getNumVertices() const noexcept
	{
		return 2;
	}

	void Cylinder::transform(const Math::Transformation& transform) noexcept
	{
		Object::transform(transform);
		this->top.transform(transform);
		this->bottom.transform(transform);
	}
}