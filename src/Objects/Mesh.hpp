/*
 * @author thomas
 * @date   12/10/18
 * @file   Mesh.hpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#ifndef RAY_TRACER_MESH_HPP
#define RAY_TRACER_MESH_HPP

#include <vector>

#include <Math/Transformation.hpp>
#include <Math/mat4.hpp>

#include <Objects/Hittable.hpp>
#include <Objects/Sphere.hpp>
#include <Objects/Triangle.hpp>
#include <Objects/Vertex.hpp>

namespace RayTracer::Objects
{
	class Mesh : public Object
	{
		private:
			constexpr static std::string_view MODEL_FILE_KEY = "filename";

			Sphere boundingSphere;
			std::vector<Triangle> triangles;

		public:
			explicit Mesh (const Json::Value& jsonMesh);

			[[deprecated]]
			std::optional<Hit> hit(Ray ray) const noexcept override;

			std::optional<Hit> hit(Ray ray, const Math::vec3& pixel) const noexcept override;

			long getNumVertices() const noexcept override;
	};
}

#endif //RAY_TRACER_MESH_HPP
