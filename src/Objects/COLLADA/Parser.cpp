/*
 * Created by thomas on 8/15/18.
 *
 * Copyright 2018 Thomas Cassimon
 */

#include "Parser.hpp"

#include <algorithm>
#include <utility>

#include <rapidxml/rapidxml_utils.hpp>

#include <Utils/String.hpp>

namespace RayTracer::Objects::COLLADA
{
	const std::string Parser::COLLADA = "COLLADA";
	const std::string Parser::ASSET = "asset";
	const std::string Parser::UP_AXIS = "up_axis";
	const std::string Parser::GEOMETRY_LIBRARY = "library_geometries";
	const std::string Parser::GEOMETRY = "geometry";
	const std::string Parser::GEOMETRY_NAME = "name";
	const std::string Parser::GEOMETRY_ID = "id";
	const std::string Parser::MESH = "mesh";
	const std::string Parser::SOURCE = "source";
	const std::string Parser::SOURCE_ID = "id";
	const std::string Parser::FLOAT_ARRAY = "float_array";
	const std::string Parser::COUNT = "count";
	const std::string Parser::POSITION_SUFFIX = "-positions";
	const std::string Parser::NORMALS_SUFFIX = "-normals";
	const std::string Parser::TEXTURE_SUFFIX = "-map-0";
	const std::string Parser::TRIANGLES = "triangles";
	const std::string Parser::P = "p";

	const std::string Parser::X_UP = "x_up";
	const std::string Parser::Y_UP = "y_up";
	const std::string Parser::Z_UP = "z_up";

	Parser::UpAxis Parser::getUpAxis(const rapidxml::xml_node<>* rootNode) const
	{
		const rapidxml::xml_node<>* assetNode = rootNode->first_node(Parser::ASSET.c_str());

		if (assetNode == nullptr)
		{
			throw std::runtime_error("Corrupt COLLADA file, missing <" + Parser::ASSET + ">.");
		}

		const rapidxml::xml_node<>* upAxisNode = assetNode->first_node(UP_AXIS.c_str());

		if (upAxisNode == nullptr)
		{
			throw std::runtime_error("Corrupt COLLADA file, missing <" + Parser::UP_AXIS + ">.");
		}

		std::string upAxis = Utils::String::toLowerCase(std::string(upAxisNode->value()));

		if (upAxis == Parser::X_UP)
		{
			return UpAxis::X_UP;
		}
		else if (upAxis == Parser::Y_UP)
		{
			return UpAxis::Y_UP;
		}
		else if (upAxis == Parser::Z_UP)
		{
			return UpAxis::Z_UP;
		}
		else
		{
			throw std::runtime_error("<" + Parser::UP_AXIS + "> contained invalid value: " + std::string(upAxisNode->value()) + " (" + upAxis + ").");
		}
	}

	rapidxml::xml_node<>* Parser::getGeometry(const std::string& id, const rapidxml::xml_node<>* rootNode) const
	{
		const rapidxml::xml_node<>* geometryLibrary = rootNode->first_node(GEOMETRY_LIBRARY.c_str());

		if (geometryLibrary == nullptr)
		{
			throw std::runtime_error("Corrupt COLLADA file, missing <" + GEOMETRY_LIBRARY + ">.");
		}

		for (rapidxml::xml_node<>* geometryNode = geometryLibrary->first_node(GEOMETRY.c_str()); geometryNode; geometryNode = geometryNode->next_sibling(GEOMETRY.c_str()))
		{
			const rapidxml::xml_attribute<>* idAttribute = geometryNode->first_attribute(GEOMETRY_ID.c_str());

			if (idAttribute == nullptr)
			{
				throw std::runtime_error("Corrupt COLLADA file, <" + GEOMETRY + "> tag missing \"" + GEOMETRY_ID + "\" attribute.");
			}

			if (std::string(idAttribute->value()) == id)
			{
				return geometryNode;
			}
		}

		throw std::out_of_range("Geometry with id \"" + id + "\" doesn't exist");
	}

	rapidxml::xml_node<>* Parser::getMesh(const std::string& geometryId, const rapidxml::xml_node<>* rootNode) const
	{
		const rapidxml::xml_node<>* geometryNode = this->getGeometry(geometryId, rootNode);

		rapidxml::xml_node<>* meshNode = geometryNode->first_node(MESH.c_str());

		if (meshNode == nullptr)
		{
			throw std::runtime_error("Geometry with id \"" + geometryId + "\" doesn't have a mesh.");
		}

		return meshNode;
	}

	std::vector<std::pair<std::string, std::string>> Parser::getGeometries(const rapidxml::xml_node<>* rootNode) const
	{
		const rapidxml::xml_node<>* geometryLibrary = rootNode->first_node(GEOMETRY_LIBRARY.c_str());

		if (geometryLibrary == nullptr)
		{
			throw std::runtime_error("Corrupt COLLADA file, missing <" + std::string(GEOMETRY_LIBRARY) + ">.");
		}

		std::vector <std::pair<std::string, std::string>> geometries = std::vector<std::pair<std::string, std::string>>();

		for (rapidxml::xml_node<>* geometryNode = geometryLibrary->first_node(GEOMETRY.c_str()); geometryNode; geometryNode = geometryNode->next_sibling(GEOMETRY.c_str()))
		{
			const rapidxml::xml_attribute<>* nameAttribute = geometryNode->first_attribute(GEOMETRY_NAME.c_str());
			const rapidxml::xml_attribute<>* idAttribute = geometryNode->first_attribute(GEOMETRY_ID.c_str());

			if (nameAttribute == nullptr)
			{
				throw std::runtime_error("Corrupt COLLADA file, <" + GEOMETRY + "> tag missing \"" + GEOMETRY_NAME + "\" attribute.");
			}

			if (idAttribute == nullptr)
			{
				throw std::runtime_error("Corrupt COLLADA file, <" + GEOMETRY + "> tag missing \"" + GEOMETRY_ID + "\" attribute.");
			}

			geometries.emplace_back(idAttribute->value(), nameAttribute->value());
		}

		return geometries;
	}

	template <typename T>
	std::vector<T> Parser::getIndices(const std::string& geometryId, const rapidxml::xml_node<>* rootNode) const
	{
		const rapidxml::xml_node<>* meshNode = this->getMesh(geometryId, rootNode);

		const rapidxml::xml_node<>* triangleNode = meshNode->first_node(TRIANGLES.c_str());

		if (triangleNode == nullptr)
		{
			throw std::runtime_error("Mesh from geometry with id \"" + geometryId + "\" doesn't have a triangle list.");
		}

		const rapidxml::xml_attribute<>* countAttribute = triangleNode->first_attribute(COUNT.c_str());

		if (countAttribute == nullptr)
		{
			throw std::runtime_error("Triangle list from geometry with id \"" + geometryId + "\" is missing the \"" + COUNT + "\" attribute.");
		}

		long numVertices = std::stol(countAttribute->value()) * 3;

		const rapidxml::xml_node<>* dataNode = triangleNode->first_node(P.c_str());

		if (dataNode == nullptr)
		{
			throw std::runtime_error("Data from geometry with id \"" + geometryId + "\" doesn't have any content.");
		}

		std::string dataString = std::string(dataNode->value());

		std::vector<std::string> indexStrings = Utils::String::split(dataString, " ");
		std::vector<T> vertices = std::vector<T>(static_cast<unsigned long>(numVertices));

		long vectorSize = 0;

		if constexpr (std::is_same<T, Math::ivec3>::value)
		{
			vectorSize = 3;
		}
		else if constexpr (std::is_same<T, Math::ivec4>::value)
		{
			vectorSize = 4;
		}

		int vertexIndices [vectorSize];

		for (std::size_t i = 0; i < indexStrings.size(); i++)
		{
			vertexIndices[i % vectorSize] = std::stoi(indexStrings[i]);

			if ((i % vectorSize) == (vectorSize - 1))
			{
				if constexpr (std::is_same<T, Math::ivec3>::value)
				{
					vertices[i / vectorSize] = Math::ivec3(vertexIndices[0], vertexIndices[1], vertexIndices[2]);
				}
				else if constexpr (std::is_same<T, Math::ivec4>::value)
				{
					vertices[i / vectorSize] = Math::ivec4(vertexIndices[0], vertexIndices[1], vertexIndices[2], vertexIndices[3]);
				}
			}
		}

		return vertices;
	}

	template <typename T>
	std::vector<T> Parser::getVertices(const std::string& geometryId, const rapidxml::xml_node<>* rootNode, const std::string& suffix) const
	{
		const std::vector<std::string> expectedIds = {};
		const std::string expectedId = geometryId + suffix;

		const rapidxml::xml_node<>* meshNode = this->getMesh(geometryId, rootNode);

		rapidxml::xml_node<>* sourceNode = nullptr;

		for (sourceNode = meshNode->first_node(SOURCE.c_str()); sourceNode; sourceNode = sourceNode->next_sibling(SOURCE.c_str()))
		{
			const rapidxml::xml_attribute<>* idAttribute = sourceNode->first_attribute(SOURCE_ID.c_str());

			if (idAttribute == nullptr)
			{
				throw std::runtime_error("Corrupt COLLADA file, <" + SOURCE + "> tag missing \"" + SOURCE_ID + "\" attribute.");
			}

			std::string nodeId = std::string(idAttribute->value());

			if (nodeId == expectedId)
			{
				break;
			}
		}

		if (sourceNode == nullptr)
		{
			throw std::runtime_error("Corrupt COLLADA file, <" + MESH + "> tag is missing <" + SOURCE + "> tag.");
		}

		const rapidxml::xml_node<>* floatArrayNode = sourceNode->first_node(FLOAT_ARRAY.c_str());

		if (floatArrayNode == nullptr)
		{
			throw std::runtime_error("Corrupt COLLADA file, <" + SOURCE + "> tag missing <" + FLOAT_ARRAY + "> tag.");
		}

		const rapidxml::xml_attribute<>* numFloatsAttribute = floatArrayNode->first_attribute(COUNT.c_str());

		if (numFloatsAttribute == nullptr)
		{
			throw std::runtime_error("Corrupt COLLADA file, <" + FLOAT_ARRAY + "> tag missing \"" + COUNT + "\" attribute.");
		}

		long numFloats = numFloats = std::stol(numFloatsAttribute->value());
		long numVertices = 0;

		std::vector<T> vertices;

		if constexpr (std::is_same<T, Math::vec3>::value)
		{
			numVertices = numFloats / 3;
		}
		else if constexpr (std::is_same<T, Math::vec2>::value)
		{
			numVertices = numFloats / 2;
		}

		vertices = std::vector<T>(static_cast<std::size_t>(numVertices));

		std::string dataString = std::string(floatArrayNode->value());

		std::vector<std::string> floatStrings = Utils::String::split(dataString, " ");

		for (long i = 0; i < numVertices; i++)
		{
			if constexpr (std::is_same<T, Math::vec3>::value)
			{
				float vertex [3];

				for (long j = (i * 3); j < ((i + 1) * 3); j++)
				{
					vertex[j - (i * 3)] = std::stof(floatStrings[j]);
				}

				vertices[i] = Math::vec3(vertex[0], vertex[1], vertex[2]);
			}
			else if constexpr (std::is_same<T, Math::vec2>::value)
			{
				float vertex [2];

				for (long j = (i * 2); j < ((i + 1) * 2); j++)
				{
					vertex[j - (i * 2)] = std::stof(floatStrings[j]);
				}

				vertices[i] = Math::vec2(vertex[0], vertex[1]);
			}
		}

		return vertices;
	}

	std::vector<Math::vec3> Parser::getPositions(const std::string& id, const rapidxml::xml_node<>* rootNode) const
	{
		return this->getVertices<Math::vec3>(id, rootNode, POSITION_SUFFIX);
	}

	std::vector<Math::vec3> Parser::getNormals (const std::string& id, const rapidxml::xml_node<>* rootNode) const
	{
		return this->getVertices<Math::vec3>(id, rootNode, NORMALS_SUFFIX);
	}

	std::vector<Math::vec2> Parser::getTextureCoordinates (const std::string& id, const rapidxml::xml_node<>* rootNode) const
	{
		return this->getVertices<Math::vec2>(id, rootNode, TEXTURE_SUFFIX);
	}

	std::vector<Objects::Vertex> Parser::mergeVertices(const std::vector<Math::vec3>& positions, const std::vector<Math::vec3>& normals, const std::vector<Math::vec2>& textures, const std::vector<Math::ivec3>& indices) const
	{
		std::vector<Objects::Vertex> result = std::vector<Objects::Vertex>(indices.size());

		for (std::size_t i = 0; i < indices.size(); i++)
		{
			result[i] = {.position = positions[indices[i].x], .texture = textures[indices[i].z], .normal = normals[indices[i].y]};
		}

		return result;
	}

	std::vector<Objects::Vertex> Parser::convertUpAxis(const std::vector<Objects::Vertex>& vertices) const noexcept
	{
		RayTracer::Math::TransformFactory factory;
		Math::Transformation transform = factory.rotate(RayTracer::Math::vec3(1.0f, 0.0f, 0.0f), Math::PI / 2.0f);

		std::vector<Objects::Vertex> result = std::vector<Objects::Vertex>(vertices);

		for (Objects::Vertex& vertex : result)
		{
			Math::vec4 postition4 = Math::vec4(vertex.position, 1.0f);
			Math::vec4 normal4 = Math::vec4(vertex.normal, 1.0f);

			postition4 = transform.forward(postition4);
			normal4  = Math::normalize(transform.forward(normal4));

			vertex.position = Math::vec3(postition4.x, postition4.y, postition4.z);
			vertex.normal = Math::vec3(normal4.x, normal4.y, normal4.z);
		}

		return result;
	}

	std::vector<unsigned int> Parser::expandTriangles(std::size_t triangleSize) const
	{
		std::vector<unsigned int> indices = std::vector<unsigned int>(triangleSize);

		for (std::size_t i = 0; i < triangleSize; i++)
		{
			indices[i] = static_cast<unsigned int>(i);
		}

		return indices;
	}

	std::vector<Vertex> Parser::parseGeometry (const rapidxml::xml_node<>* rootNode) const
	{
		std::vector<Objects::Vertex> vertices;

		std::vector<std::pair<std::string, std::string>> geometries = this->getGeometries(rootNode);

		for (const std::pair<std::string, std::string>& geometry : geometries)
		{
			const std::string& meshId = geometry.first;

			std::vector<Math::vec3> positions = this->getPositions(meshId, rootNode);
			std::vector<Math::vec3> normals = this->getNormals(meshId, rootNode);
			std::vector<Math::vec2> textureCoordinates = this->getTextureCoordinates(meshId, rootNode);
			std::vector<Math::ivec3> indices = this->getIndices<Math::ivec3>(meshId, rootNode);

			std::vector<Objects::Vertex> geometryVertices = this->mergeVertices(positions, normals, textureCoordinates, indices);

			vertices = Utils::merge(vertices, geometryVertices);
		}

		vertices = this->convertUpAxis(vertices);

		return vertices;
	}

	std::vector<Vertex> Parser::parse(const std::string& filename) const
	{
		rapidxml::file<> file (filename.c_str());
		rapidxml::xml_document<> doc;

		doc.parse<0>(file.data());

		rapidxml::xml_node<>* rootNode = doc.first_node(COLLADA.c_str());

		if (rootNode == nullptr)
		{
			throw std::runtime_error("Corrupt COLLADA file, missing <" + COLLADA + ">.");
		}

		return this->parseGeometry(rootNode);
	}

	template
	std::vector<Math::vec2> Parser::getVertices (const std::string& id, const rapidxml::xml_node<>* rootNode, const std::string& suffix) const;

	template
	std::vector<Math::vec3> Parser::getVertices (const std::string& id, const rapidxml::xml_node<>* rootNode, const std::string& suffix) const;

	template
	std::vector<Math::vec3> Parser::getIndices (const std::string& id, const rapidxml::xml_node<>* rootNode) const;

	template
	std::vector<Math::vec4> Parser::getIndices (const std::string& id, const rapidxml::xml_node<>* rootNode) const;
}