/*
 * Created by thomas on 8/15/18.
 *
 * Copyright 2018 Thomas Cassimon
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef WATSON_COLLADA_PARSER_HPP
#define WATSON_COLLADA_PARSER_HPP

#include <Objects/Vertex.hpp>
#include <Objects/Mesh.hpp>

#include <Math/vec2.hpp>
#include <Math/vec3.hpp>
#include <Math/vec4.hpp>
#include <Math/Util.hpp>
#include <Math/TransformFactory.hpp>

#include <Utils/Vector.hpp>
#include <Utils/String.hpp>

#include <rapidxml/rapidxml.hpp>

#include <string>
#include <filesystem>

namespace RayTracer::Objects::COLLADA
{
	class Parser
	{
		private:
			const static std::string COLLADA;
			const static std::string ASSET;
			const static std::string UP_AXIS;
			const static std::string GEOMETRY_LIBRARY;
			const static std::string GEOMETRY;
			const static std::string GEOMETRY_NAME;
			const static std::string GEOMETRY_ID;
			const static std::string MESH;
			const static std::string SOURCE;
			const static std::string SOURCE_ID;
			const static std::string FLOAT_ARRAY;
			const static std::string COUNT;
			const static std::string POSITION_SUFFIX;
			const static std::string NORMALS_SUFFIX;
			const static std::string TEXTURE_SUFFIX;
			const static std::string TRIANGLES;
			const static std::string P;

			const static std::string X_UP;
			const static std::string Y_UP;
			const static std::string Z_UP;

			enum class UpAxis
			{
				X_UP,
				Y_UP,
				Z_UP
			};

			UpAxis getUpAxis (const rapidxml::xml_node<>* rootNode) const;

			/**
			 * Returns the <geometry> tag for the geometry with the specified id.
			 * @param id
			 * @param rootNode
			 * @return
			 */
			rapidxml::xml_node<>* getGeometry (const std::string& id, const rapidxml::xml_node<>* rootNode) const;

			rapidxml::xml_node<>* getMesh(const std::string& geometryId, const rapidxml::xml_node<>* rootNode) const;

			/**
			 * Returns a vector of id-name pairs of each piece of geometry in the model.
			 * @param rootNode
			 * @return
			 */
			std::vector <std::pair<std::string, std::string>> getGeometries(const rapidxml::xml_node<>* rootNode) const;


			/**
			 *
			 * @param geometryId
			 * @param rootNode
			 * @return
			 */
			template <typename T>
			 std::vector<T> getIndices (const std::string& geometryId, const rapidxml::xml_node<>* rootNode) const;

			/**
			 * Return a vector of vertices, which are a part of the geometry identified by "id", whose <source> tag has its "id" attribute set to the geometry id + suffix.
			 * @tparam T 		The type of vertex to return, this can either be glm::vec3 or glm::vec2
			 * @param geometryId
			 * @param rootNode
			 * @param suffix
			 * @return
			 */
			template <typename T>
			std::vector<T> getVertices (const std::string& geometryId, const rapidxml::xml_node<>* rootNode, const std::string& suffix) const;

			/**
			 * Get a vector of position vertices for the specified ID.
			 * @param id
			 * @return
			 */
			std::vector<Math::vec3> getPositions (const std::string& id, const rapidxml::xml_node<>* rootNode) const;

			/**
			 * Get a vector of normal vertices for the specified ID.
			 * @param id
			 * @param rootNode
			 * @return
			 */
			std::vector<Math::vec3> getNormals (const std::string& id, const rapidxml::xml_node<>* rootNode) const;

			/**
			 *
			 * @param id
			 * @param rootNode
			 * @return
			 */
			std::vector<Math::vec2> getTextureCoordinates (const std::string& id, const rapidxml::xml_node<>* rootNode) const;

			std::vector<Objects::Vertex> parseGeometry (const rapidxml::xml_node<>* rootNode) const;

			//TODO: Somehow prevent allocation of a new, 4th buffer and reuse the memory from the 3 other buffers.
			// -> glSubBufferData?
			/**
			 *
			 * @see https://learnopengl.com/img/getting-started/vertex_attribute_pointer_interleaved_textures.png
			 * @param positions
			 * @param normals
			 * @param textures
			 * @param indices
			 * @return
			 */
			std::vector<Objects::Vertex> mergeVertices (const std::vector<Math::vec3>& positions, const std::vector<Math::vec3>& normals, const std::vector<Math::vec2>& textures, const std::vector<Math::ivec3>& indices) const;

			std::vector<Objects::Vertex> convertUpAxis (const std::vector<Objects::Vertex>& vertices) const noexcept;

			std::vector<unsigned int> expandTriangles (std::size_t triangleSize) const;

		public:
			Parser() noexcept = default;

			std::vector<Objects::Vertex> parse(const std::string& filename) const;
	};
}

#endif //WATSON_COLLADA_PARSER_HPP
