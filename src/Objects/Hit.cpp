/*
 * @author thomas
 * @date   05/10/18
 * @file   Hit.cpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#include "Hit.hpp"
#include "Object.hpp"

namespace RayTracer::Objects
{
	Hit::Hit() noexcept : objectId(-1), pos(), normal(), color(), material()
	{
	}

	Hit::Hit(long objectId, const Math::vec3& pos, const Math::vec3& normal, const Graphics::Color& color, const Shaders::Material& material) noexcept: objectId(objectId), pos(pos), normal(normal), color(color), material(material)
	{
	}

	Hit::Hit(long objectId, Math::vec3&& pos, Math::vec3&& normal, Graphics::Color&& color, Shaders::Material&& material) noexcept: objectId(objectId), pos(std::move(pos)), normal(std::move(normal)), color(color), material(material)
	{
	}

	long Hit::getObjectId() const noexcept
	{
		return this->objectId;
	}

	const Math::vec3& Hit::getPosition() const noexcept
	{
		return this->pos;
	}

	const Math::vec3& Hit::getNormal() const noexcept
	{
		return this->normal;
	}

	const Graphics::Color& Hit::getColor() const noexcept
	{
		return this->color;
	}

	const Shaders::Material& Hit::getMaterial() const noexcept
	{
		return this->material;
	}

	std::ostream& operator<<(std::ostream& stream, const Hit& hit) noexcept
	{
		stream << "Position: " << hit.pos << "\nNormal: " << hit.normal;
		return stream;
	}
}