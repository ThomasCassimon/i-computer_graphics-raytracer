/*
 * @author thomas
 * @date 12/25/18.
 */

#include "Cube.hpp"
#include <cstdint>
#include "Math/TransformFactory.hpp"

namespace RayTracer
{
	Objects::Cube::Cube(const Json::Value& jsonCube) : Object(jsonCube)
	{
		const Json::Value jsonCenter = jsonCube[CENTER_KEY.data()];
		const Json::Value jsonSize = jsonCube[SIZE_KEY.data()];

		if (jsonCenter.isNull())
		{
			throw std::runtime_error("Cube didn't contain center.");
		}

		if (jsonSize.isNull())
		{
			throw std::runtime_error("Cube didn't contain size.");
		}

		this->size = Math::vec3(jsonSize);
		this->center = Math::vec3(jsonCenter);
		this->min = this->center - (this->size / 2.0f);
		this->max = this->center + (this->size / 2.0f);

		Math::TransformFactory factory;
		this->transform(factory.scale(this->size));
		this->transform(factory.translate(this->center));
	}

	// See: https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-box-intersection
	std::optional<Objects::Hit> Objects::Cube::hit(Objects::Ray ray) const noexcept
	{
		Math::vec4 offset4 = this->transformation.inverse(Math::vec4(ray.getOffset(), 1.0f));
		Math::vec4 direction4 = this->transformation.inverse(Math::vec4(ray.getDirection(), 0.0f));

		Math::vec3 offset (offset4.x, offset4.y, offset4.z);
		Math::vec3 direction (direction4.x, direction4.y, direction4.z);

		/*
		float tmin = Math::min((this->min - offset) / direction);
		float tmax = Math::max((this->max - offset) / direction);

		if (tmin < tmax)
		{
			std::swap(tmin, tmax);
		}
		*/

		float tmin = (this->min.x - offset.x) / direction.x;
		float tmax = (this->max.x - offset.x) / direction.x;

		// If the values are the wrong way around, we swap them.
		if (tmax < tmin)
		{
			std::swap(tmin, tmax);
		}

		float tymin = (this->min.y - offset.y) / direction.y;
		float tymax = (this->max.y - offset.y) / direction.y;

		// If the values are the wrong way around, we swap them.
		if (tymax < tymin)
		{
			std::swap(tymin, tymax);
		}

		if ((tymax < tmin) || (tmax < tymin))
		{
			return std::nullopt;
		}

		if (tmin < tymin)
		{
			tmin = tymin;
		}

		if (tymax < tmax)
		{
			tmax = tymax;
		}

		float tzmin = (min.z - offset.z) / direction.z;
		float tzmax = (max.z - offset.z) / direction.z;

		if (tzmax < tzmin)
		{
			std::swap(tzmin, tzmax);
		}

		if ((tzmax < tmin) || (tmax < tzmin))
		{
			return std::nullopt;
		}

		if (tmin < tzmin)
		{
			tmin = tzmin;
		}

		if (tzmax < tmax)
		{
			tmax = tzmax;
		}

		const Math::vec3 position = ray.findPoint(tmin);

		Math::vec3 normal (this->size / 2.0f);

		normal.x = position.x == this->min.x ? -normal.x : normal.x;
		normal.y = position.y == this->min.y ? -normal.y : normal.y;
		normal.z = position.z == this->min.z ? -normal.z : normal.z;

		return std::make_optional<Hit>(this->getId(), position, normal, this->color->sample(position), this->material);
	}

	std::optional<Objects::Hit> Objects::Cube::hit(Objects::Ray ray, const Math::vec3& pixel) const noexcept
	{
		return this->hit(ray);
	}

	long Objects::Cube::getNumVertices() const noexcept
	{
		return 8;
	}
}