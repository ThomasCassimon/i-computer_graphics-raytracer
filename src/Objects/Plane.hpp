/*
 * @author thomas
 * @date   05/10/18
 * @file   Plane.hpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#ifndef RAY_TRACER_PLANE_HPP
#define RAY_TRACER_PLANE_HPP

#include "Math/vec3.hpp"
#include "Objects/Object.hpp"

namespace RayTracer::Objects
{
	class [[deprecated]] Plane : public Object
	{
		private:
			constexpr static std::string_view POINT_KEY = "point";
			constexpr static std::string_view NORMAL_KEY = "normal";

			Math::vec3 p0;
			Math::vec3 n;
			float ndotp0;

		public:
			Plane () = delete;

			Plane (long id) noexcept;

			Plane (long id, const Math::vec3& p0, const Math::vec3& n, std::shared_ptr<Graphics::Samplable> color, Shaders::Material material) noexcept;

			Plane (const Json::Value& jsonPlane);

			std::optional<Hit> hit(Ray ray) const noexcept override;

			[[deprecated]]
			std::optional<Hit> hit(Ray ray, const Math::vec3& pixel) const noexcept override;

			long getNumVertices() const noexcept override;
	};
}

#endif //RAY_TRACER_PLANE_HPP
