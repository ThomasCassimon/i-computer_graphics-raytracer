/*
 * @author thomas
 * @date   12/10/18
 * @file   Vertex.hpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#ifndef RAY_TRACER_VERTEX_HPP
#define RAY_TRACER_VERTEX_HPP

#include <Math/vec2.hpp>
#include <Math/vec3.hpp>

namespace RayTracer::Objects
{
	struct Vertex
	{
		Math::vec3 position;
		Math::vec2 texture;
		Math::vec3 normal;
	};
}

#endif //RAY_TRACER_VERTEX_HPP
