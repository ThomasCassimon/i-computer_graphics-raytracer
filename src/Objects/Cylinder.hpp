/*
 * @author thomas
 * @date 12/25/18.
 */

#ifndef RAY_TRACER_CONE_HPP
#define RAY_TRACER_CONE_HPP

#include "Math/vec3.hpp"
#include "Objects/Object.hpp"
#include "Objects/Plane.hpp"

namespace RayTracer::Objects
{
	class Cylinder: public Object
	{
		private:
			constexpr static std::string_view BASE_KEY = "base";
			constexpr static std::string_view S_KEY = "s";

			Plane top;
			Plane bottom;
			float s;

			std::optional<Hit> topHit (Ray ray) const noexcept;

			std::optional<Hit> bottomHit (Ray ray) const noexcept;

			std::optional<Hit> mantleHit (Ray ray) const noexcept;

		public:
			explicit Cylinder (const Json::Value& jsonCylinder);

			std::optional<Hit> hit(Ray ray) const noexcept override;

			std::optional<Hit> hit(Ray ray, const Math::vec3& pixel) const noexcept override;

			long getNumVertices() const noexcept override;

			void transform(const Math::Transformation& transform) noexcept override;
	};
}



#endif //RAY_TRACER_CONE_HPP