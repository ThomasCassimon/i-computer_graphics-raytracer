/*
 * @author  thomas
 * @date    23/11/18
 * Copyright (c) 2018 thomas
 */

#include "Triangle.hpp"

#include <sstream>

//#include "Graphics/PixelBuffer.hpp"

namespace RayTracer::Objects
{
	Triangle::Triangle(const Json::Value& jsonTriangle) : Object(jsonTriangle)
	{
		const Json::Value& jsonBackHits = jsonTriangle[ALLOW_BACK_HITS_KEY.data()];
		const Json::Value& jsonVertices = jsonTriangle[VERTICES_KEY.data()];

		if (jsonBackHits.isNull())
		{
			this->allowBackHits = false;
		}
		else
		{
			this->allowBackHits = jsonBackHits.asBool();
		}

		if (jsonVertices.isNull())
		{
			throw std::runtime_error("Triangle didn't contain any vertices.");
		}

		if (jsonVertices.size() != 3)
		{
			std::stringstream stream;
			stream << "Triangle vertices had size of ";
			stream << jsonVertices.size();
			stream << ", should be 3.";
			throw std::runtime_error(stream.str());
		}

		for (int i = 0; i < jsonVertices.size(); ++i)
		{
			this->vertices[i] = Math::vec3(jsonVertices[i]);
		}

		this->edge1 = vertices[1] - vertices[0];
		this->edge2 = vertices[2] - vertices[0];
		this->normal = Math::normalize(Math::cross(this->edge1, this->edge2));
	}

	Triangle::Triangle(long id, const std::array<RayTracer::Math::vec3, 3>& vertices, bool allowBackHit) noexcept:   Object(id),
																											allowBackHits(allowBackHit),
																											edge1(vertices[1] - vertices[0]),
																											edge2(vertices[2] - vertices[0]),
																											normal(Math::normalize(Math::cross(this->edge1, this->edge2))),
																											vertices(vertices)
	{
	}

	std::optional<Hit> Triangle::hit(Ray ray) const noexcept
	{
		const Math::vec3 P = Math::cross(ray.getDirection(), this->edge2);
		const float det = Math::dot(this->edge1, P);

		if (this->allowBackHits)
		{
			if (std::fabs(det) < std::numeric_limits<float>::epsilon())
			{
				return std::nullopt;
			}

			const float inv_det = 1.0f / det;
			const Math::vec3 T = ray.getOffset() - this->vertices[0];
			const float u = inv_det * Math::dot(T, P);

			if ((u < 0.0f) || (u > 1.0f))
			{
				return std::nullopt;
			}

			const Math::vec3 Q = Math::cross(T, this->edge1);
			const float v = inv_det * Math::dot(ray.getDirection(), Q);

			if ((v < 0.0f) || ((u + v) > 1.0f))
			{
				return std::nullopt;
			}

			const float t = inv_det * Math::dot(this->edge2, Q);
			const Math::vec3 hitpoint = ray.getOffset() + (t * ray.getDirection());

			return std::make_optional<Objects::Hit>(this->getId(), hitpoint, this->normal, this->color->sample(Math::vec3(u, v, 0.0f)), this->material);
		}
		else
		{
			if (det < std::numeric_limits<float>::epsilon())
			{
				return std::nullopt;
			}

			const Math::vec3 T = ray.getOffset() - this->vertices[0];
			float u = Math::dot(T, P);

			if ((u < 0.0f) || (u > det))
			{
				return std::nullopt;
			}

			const Math::vec3 Q = Math::cross(T, this->edge1);
			float v = Math::dot(ray.getDirection(), Q);

			if ((v < 0.0f) || ((u + v) > det))
			{
				return std::nullopt;
			}

			float t = Math::dot(this->edge2, Q);
			const float inv_det = 1.0f / det;
			t *= inv_det;
			u *= inv_det;
			v *= inv_det;

			const Math::vec3 hitpoint = ray.getOffset() + (t * ray.getDirection());

			return std::make_optional<Objects::Hit>(this->getId(), hitpoint, this->normal, this->color->sample(Math::vec3(u, 0.0f, v)), this->material);
		}
	}

	std::optional<Hit> Triangle::hit(Ray ray, const Math::vec3& pixel) const noexcept
	{
		return this->hit(ray);
	}

	long Triangle::getNumVertices() const noexcept
	{
		return 3;
	}
}