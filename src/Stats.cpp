/*
 * @author thomas
 * @date   20/10/18
 * @file   Stats.cpp
 *
 * Copyright 2018 Thomas Cassimon
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Stats.hpp"

#include <Utils/String.hpp>
#include <Utils/Vector.hpp>

namespace RayTracer
{
	Stats::Stats(): SIMD(false), resolution(-1, -1), totalVertices(-1), totalRays(-1), traceTime(-1), perThreadStats()
	{
	}

	Stats::Stats(bool SIMD, long totalMilliseconds, double totalVertices, double totalRays, const Math::ivec2& resolution, const std::optional<std::vector<Stats>>& perThreadStats) noexcept: SIMD(SIMD), resolution(resolution), totalVertices(totalVertices), totalRays(totalRays), traceTime(totalMilliseconds), perThreadStats(perThreadStats)
	{
	}

	Stats& Stats::operator=(Stats other) noexcept
	{
		std::swap(this->resolution, other.resolution);
		std::swap(this->totalVertices, other.totalVertices);
		std::swap(this->totalRays, other.totalRays);
		std::swap(this->traceTime, other.traceTime);
		return *this;
	}

	std::ostream& operator<<(std::ostream& stream, const Stats& stats) noexcept
	{
		stream << "Total trace time: " << stats.getFormattedTracetime() << std::endl;
		stream << "Traced " << stats.getFormattedTotalRays() << std::endl;
		stream << "Average speed of " << stats.getFormattedRaySpeed() << std::endl;
		return stream;
	}

	std::string Stats::getFormattedTracetime() const noexcept
	{
		std::stringstream stream;
		stream << this->traceTime;
		return stream.str();
	}

	std::string Stats::getFormattedRaySpeed() const noexcept
	{
		double speed = static_cast<double>(this->totalRays) / (static_cast<double>(this->traceTime.getTotalMilliseconds()) / 1.0E3);

		std::pair<double, std::string> formattedSpeed = Utils::String::getOrderOfMagnitude(speed);

		std::stringstream stream;
		stream << formattedSpeed.first << " " << formattedSpeed.second << "Rays / s";
		return stream.str();
	}

	std::string Stats::getFormattedTotalRays() const noexcept
	{
		std::pair<double, std::string> formattedNumRays = Utils::String::getOrderOfMagnitude(this->totalRays);

		std::stringstream stream;
		stream << formattedNumRays.first << " " << formattedNumRays.second << "Rays";
		return stream.str();
	}

	std::string Stats::getFormattedVertexSpeed() const noexcept
	{
		double speed = (this->totalVertices * this->totalRays) / (static_cast<double>(this->traceTime.getTotalMilliseconds()) / 1.0E3);

		std::pair<double, std::string> formattedSpeed = Utils::String::getOrderOfMagnitude(speed);

		std::stringstream stream;
		stream << formattedSpeed.first << " " << formattedSpeed.second << "Vertices / s";
		return stream.str();
	}

	std::string Stats::getFormattedTotalVertices() const noexcept
	{
		std::pair<double, std::string> formattedNumVertices = Utils::String::getOrderOfMagnitude(this->totalVertices);

		std::stringstream stream;
		stream << formattedNumVertices.first << " " << formattedNumVertices.second << "Vertices";
		return stream.str();
	}

	std::string Stats::getFormattedResolution() const noexcept
	{
		std::stringstream stream;
		stream << this->resolution.x << " x " << this->resolution.y << " pixels";
		return stream.str();
	}

	std::optional<std::vector<Stats>> Stats::getPerThreadStats() const noexcept
	{
		return this->perThreadStats;
	}

	std::string Stats::getFormattedSIMDFlag() const noexcept
	{
		if (this->SIMD)
		{
			return "Enabled";
		}
		else
		{
			return "Disabled";
		}
	}

	std::string Stats::getFormattedPixelSpeed() const noexcept
	{
		std::pair<double, std::string> formattedNumVertices = Utils::String::getOrderOfMagnitude((static_cast<double>(this->resolution.x) * static_cast<double>(this->resolution.y)) / (static_cast<double>(this->traceTime.getTotalMilliseconds()) / 1000.0));

		std::stringstream stream;
		stream << formattedNumVertices.first << " " << formattedNumVertices.second << "Pixels/s";
		return stream.str();
	}
}