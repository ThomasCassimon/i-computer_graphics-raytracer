/*
 * @author thomas
 * @date   20/10/18
 * @file   Stats.hpp
 *
 * Copyright 2018 Thomas Cassimon
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef RAY_TRACER_STATS_HPP
#define RAY_TRACER_STATS_HPP

#include <optional>
#include <vector>

#include <Time.hpp>
#include <Math/vec2.hpp>

namespace RayTracer
{
	class Stats
	{
		private:
			bool SIMD;
			Math::ivec2 resolution;
			double totalVertices;
			double totalRays;
			Time traceTime;

			std::optional<std::vector<Stats>> perThreadStats;

		public:
			Stats();

			Stats (bool simdEnabled, long totalMilliseconds, double totalVertices, double totalRays, const Math::ivec2& resolution, const std::optional<std::vector<Stats>>& threadStats) noexcept;

			Stats(const Stats& other) = default;

			Stats& operator= (Stats other) noexcept;

			friend std::ostream& operator<< (std::ostream& stream, const Stats& stats) noexcept;

			std::string getFormattedTracetime() const noexcept;

			std::string getFormattedRaySpeed() const noexcept;

			std::string getFormattedTotalRays() const noexcept;

			std::string getFormattedVertexSpeed() const noexcept;

			std::string getFormattedTotalVertices() const noexcept;

			std::string getFormattedResolution() const noexcept;

			std::string getFormattedSIMDFlag() const noexcept;

			std::string getFormattedPixelSpeed() const noexcept;

			std::optional<std::vector<Stats>> getPerThreadStats() const noexcept;
	};
}

#endif //RAY_TRACER_STATS_HPP
