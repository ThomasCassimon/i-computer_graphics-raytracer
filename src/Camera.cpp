/*
 * @author  thomas
 * @date    09/11/18
 * Copyright (c) 2018 thomas
 */

#include "Camera.hpp"

#include <sstream>

#include "Utils/Vector.hpp"
#include "Shaders/Material.hpp"

namespace RayTracer
{
	Camera::Camera(const Json::Value& jsonCamera)
	{
		// todo: Add further data validation
		const Json::Value MINUS_ONE = -1;
		const std::string IMAGE_SIZE_KEY = "img_size";
		const std::string POSITION_KEY = "position";
		const std::string DIRECTION_KEY = "direction";

		Json::Value jsonImageSize = jsonCamera[IMAGE_SIZE_KEY];
		Json::Value jsonPosition = jsonCamera[POSITION_KEY];
		Json::Value jsonDirection = jsonCamera[DIRECTION_KEY];

		if (jsonImageSize.isNull())
		{
			throw std::runtime_error("Camera didn't contain image size.");
		}

		if (jsonImageSize.size() != 2)
		{
			std::stringstream stream;
			stream << "Image size for camera was ";
			stream << jsonImageSize.size();
			stream << ", should be 2";
			throw std::runtime_error(stream.str());
		}

		this->screenSize = Math::ivec2(jsonImageSize[0].asInt(), jsonImageSize[1].asInt());
		this->virtualScreenSize = Math::vec2(2.0f, (2.0f * static_cast<float>(screenSize.y)) / static_cast<float>(screenSize.x));

		if (jsonPosition.isNull())
		{
			throw std::runtime_error("Camera didn't have position.");
		}

		if (jsonPosition.size() != 3)
		{
			std::stringstream stream;
			stream << "Position size for camera was ";
			stream << jsonPosition.size();
			stream << ", should be 3";
			throw std::runtime_error(stream.str());
		}

		this->position = Math::vec3(jsonPosition);

		if (jsonDirection.isNull())
		{
			throw std::runtime_error("Camera didn't contain direction.");
		}

		if (jsonDirection.size() != 3)
		{
			std::stringstream stream;
			stream << "Direction size for camera was ";
			stream << jsonDirection.size();
			stream << ", should be 3";
			throw std::runtime_error(stream.str());
		}

		this->direction = Math::normalize(Math::vec3(jsonDirection));
	}

	Camera::Camera(const Math::ivec2& screenSize, const Math::vec3& position, const Math::vec3& direction) noexcept: screenSize(screenSize), virtualScreenSize(2.0f, (2.0f * static_cast<float>(screenSize.y)) / static_cast<float>(screenSize.x)), position(position), direction(Math::normalize(direction))
	{
	}

	std::vector<std::vector<Math::ivec2>> Camera::generatePixelList(PixelOrder order, long numThreads) const
	{
		std::vector<Math::ivec2> pixels (static_cast<std::size_t>(this->screenSize.x * this->screenSize.y));

		switch (order)
		{
			case PixelOrder::LEFT_TO_RIGHT_TOP_TO_BOTTOM:
				for (int i = 0; i < this->screenSize.y; i++)
				{
					for (int j = 0; j < this->screenSize.x; j++)
					{
						const int index = (i * this->screenSize.x) + j;
						pixels[index] = Math::ivec2(j, i);
					}
				}

				return Utils::split(pixels, static_cast<std::size_t>(numThreads));

			case PixelOrder::TOP_TO_BOTTOM_LEFT_TO_RIGHT:
				for (int i = 0; i < this->screenSize.x; i++)
				{
					for (int j = 0; j < this->screenSize.y; j++)
					{
						const int index = (i * this->screenSize.y) + j;
						pixels[index] = Math::ivec2(i, j);
					}
				}

				return Utils::split(pixels, static_cast<std::size_t>(numThreads));
		}
	}

	std::vector<std::vector<Objects::Ray>> Camera::generateRays(const std::vector<std::vector<Math::ivec2>>& pixels)
	{
		std::vector<std::vector<Objects::Ray>> rays = std::vector<std::vector<Objects::Ray>>(pixels.size());

		const Math::vec3 screenCenter = this->position + this->direction;
		const Math::vec3 screenSpaceXAxis = Math::normalize(Math::cross(this->direction, Math::vec3(0.0f, 1.0f, 0.0f)));
		const Math::vec3 screenSpaceYAxis = Math::normalize(Math::cross(screenSpaceXAxis, this->direction));

		for (std::size_t i = 0; i < pixels.size(); i++)
		{
			const std::vector<Math::ivec2>& pixelList = pixels[i];
			rays[i] = std::vector<Objects::Ray>(pixelList.size());

			for (std::size_t j = 0; j < pixelList.size(); j++)
			{
				const Math::ivec2& pixel = pixelList[j];
				const float x = ((static_cast<float>(pixel.x) - (static_cast<float>(this->screenSize.x) / 2.0f)) / static_cast<float>(this->screenSize.x)) * this->virtualScreenSize.x;
				const float y = ((static_cast<float>(pixel.y) - (static_cast<float>(this->screenSize.y) / 2.0f)) / static_cast<float>(this->screenSize.y)) * this->virtualScreenSize.y;

				const Math::vec3 pixelPos = (y * screenSpaceYAxis) + (x * screenSpaceXAxis) + screenCenter;

				rays[i][j] = Objects::Ray(this->position, pixelPos - this->position);
			}
		}

		return rays;
	}

	const Math::ivec2& Camera::getScreenSize() const noexcept
	{
		return this->screenSize;
	}

	long Camera::getNumPixels() const noexcept
	{
		return this->screenSize.x * this->screenSize.y;
	}

	const Math::vec3& Camera::getPosition() const noexcept
	{
		return this->position;
	}
}