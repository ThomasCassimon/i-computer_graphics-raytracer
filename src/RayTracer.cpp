/*
 * @author thomas
 * @date   09/10/18
 * @file   RayTracer.cpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#include "RayTracer.hpp"
#include <algorithm>
#include <Shaders/BlinnPhong.hpp>
#include <Shaders/CookTorrance.hpp>

#ifdef USE_SIMD
#define SIMD 1
#else
#define SIMD 0
#endif

namespace RayTracer
{
	RayTracer::RayTracer(const Camera& camera, Shaders::ShaderModel model, int maxReflections, const Graphics::Color& clearColor, long numThreads) noexcept: enableAmbient(true), enableDiffuse(true), enableSpecular(true), maxReflections(maxReflections), numThreads(numThreads), clearColor(clearColor), rayCounter(0), camera(camera), model(model), stencilBuffer(camera.getScreenSize().x, camera.getScreenSize().y), frameBuffer(camera.getScreenSize()), objects(), pointLights(), globalLights()
	{
	}

	RayTracer::RayTracer(const RayTracer& other): enableAmbient(other.enableAmbient), enableDiffuse(other.enableDiffuse), enableSpecular(other.enableSpecular), maxReflections(other.maxReflections), numThreads(other.numThreads), clearColor(other.clearColor), rayCounter(other.rayCounter.load()), camera(other.camera), model(other.model), stencilBuffer(other.stencilBuffer), frameBuffer(other.frameBuffer), objects(other.objects), pointLights(other.pointLights), globalLights(other.globalLights)
	{
	}

	RayTracer::RayTracer(RayTracer&& other) noexcept: enableAmbient(other.enableAmbient), enableDiffuse(other.enableDiffuse), enableSpecular(other.enableSpecular), maxReflections(other.maxReflections), numThreads(other.numThreads), clearColor(other.clearColor), rayCounter(other.rayCounter.load()), camera(other.camera), model(other.model), stencilBuffer(other.stencilBuffer), frameBuffer(other.frameBuffer), objects(other.objects), pointLights(other.pointLights), globalLights(other.globalLights)
	{
	}

	void RayTracer::addObject(std::shared_ptr<Objects::Object> object) noexcept
	{
		this->objects.push_back(object);
	}

	void RayTracer::addPointLight(Lights::PointLight light) noexcept
	{
		this->pointLights.push_back(light);
	}

	void RayTracer::addGlobalLight(Lights::GlobalLight light) noexcept
	{
		this->globalLights.push_back(light);
	}

	std::optional<Objects::Hit> RayTracer::hits(const Objects::Ray& ray, const Math::vec3& pixelPos, const std::vector<std::shared_ptr<Objects::Object>>& exclude) noexcept
	{
		std::vector<std::shared_ptr<Objects::Object>> allowedObjects;

		std::copy_if(this->objects.begin(), this->objects.end(), std::back_inserter(allowedObjects),
			[exclude]
			(const std::shared_ptr<Objects::Object>& object)
			{return std::find(exclude.begin(), exclude.end(), object) == exclude.end();});

		return this->checkRay(ray, pixelPos, allowedObjects);
	}

	std::vector<std::pair<std::size_t, std::optional<Objects::Hit>>> RayTracer::hits(const std::vector<Objects::Ray>& rays, const std::vector<Math::vec3>& pixelPositions, const std::vector<std::shared_ptr<Objects::Object>>& excludes) noexcept
	{
		std::vector<std::pair<std::size_t, std::optional<Objects::Hit>>> hits;
		hits.reserve(rays.size() / 2);

		for (std::size_t i = 0; i < rays.size(); ++i)
		{
			++this->rayCounter;

			const Objects::Ray& ray = rays[i];
			const Math::vec3& pixelPos = pixelPositions[i];
			const std::shared_ptr<Objects::Object>& exclude = excludes[i];

			for (const std::shared_ptr<Objects::Object>& object : this->objects)
			{
				if ((*object) != (*exclude))
				{
					std::optional<Objects::Hit> hit = object->hit(ray, pixelPos);

					if (hit)
					{
						hits.emplace_back(i, hit);
					}
				}
			}
		}

		return hits;
	}

	std::optional<Objects::Hit> RayTracer::checkRay(const Objects::Ray& ray, const Math::vec3& pixelPos, const std::vector<std::shared_ptr<Objects::Object>>& objects) noexcept
	{
		++this->rayCounter;

		std::optional<Objects::Hit> minimumHit = std::nullopt;
		float minDist = std::numeric_limits<float>::max();  // Per-pixel Z-buffer

		for (const std::shared_ptr<Objects::Object>& object : objects)
		{
			std::optional<Objects::Hit> hit = object->hit(ray, pixelPos);

			if (hit)
			{
				float dist = Math::dist(hit->getPosition(), pixelPos);

				if (dist < minDist)
				{
					//std::lock_guard lock (this->frameBufferMutex);
					minDist = dist;
					minimumHit = hit;
				}
			}
		}

		return minimumHit;
	}

	Stats RayTracer::run(const std::vector<Objects::Ray>& rays, const std::vector<Math::ivec2>& pixels) noexcept
	{
		std::chrono::time_point start = std::chrono::high_resolution_clock::now();

		std::vector<Objects::Hit> hits;
		std::vector<Objects::Ray> hitRays;
		std::map<std::size_t, Math::ivec2> pixelIdcs;

		for (std::size_t i = 0; i < rays.size(); i++)
		{
			const Math::vec3 pixelPos = rays[i].getOffset();
			const Math::ivec2 pixel = pixels[i];

			std::optional<Objects::Hit> hit = this->checkRay(rays[i], pixelPos, this->objects);

			if (hit)
			{
				this->stencilBuffer.set(pixel.x, pixel.y);

				hits.push_back(hit.value());
				hitRays.push_back(rays[i]);
				pixelIdcs.insert(std::make_pair(hits.size() - 1, pixel));
			}
		}

		std::vector<Graphics::Color> colors;

		switch (this->model)
		{
			case Shaders::ShaderModel::BLINN_PHONG:
			{
				Shaders::BlinnPhong shader (*this);

				if(!this->enableAmbient)
				{
					shader.disableAmbient();
				}

				if(!this->enableDiffuse)
				{
					shader.disableDiffuse();
				}

				if(!this->enableSpecular)
				{
					shader.disableSpecular();
				}

				colors = shader.shade(this->globalLights, this->pointLights, this->camera.getPosition(), hits);

				break;
			}

			case Shaders::ShaderModel::COOK_TORRANCE:
			{
				Shaders::CookTorrance shader (*this, this->clearColor, this->maxReflections);

				if(!this->enableAmbient)
				{
					shader.disableAmbient();
				}

				if(!this->enableDiffuse)
				{
					shader.disableDiffuse();
				}

				if(!this->enableSpecular)
				{
					shader.disableSpecular();
				}

				colors = shader.shade(this->globalLights, this->pointLights, this->camera.getPosition(), hits, hitRays);

				break;
			}
		}

		for (std::size_t i = 0; i < colors.size(); i++)
		{
			this->frameBuffer(this->frameBuffer.rows() - pixelIdcs[i].y - 1, pixelIdcs[i].x) = colors[i];
		}

		long totalVertices = 0;
		for (const std::shared_ptr<Objects::Object>& object : this->objects)
		{
			totalVertices += object->getNumVertices();
		}

		std::chrono::time_point end = std::chrono::high_resolution_clock::now();
		long totalMilliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

		return Stats(SIMD, totalMilliseconds, totalVertices, static_cast<double>(this->rayCounter), this->camera.getScreenSize(), std::nullopt);
	}

	Stats RayTracer::run() noexcept
	{
		this->frameBuffer.clear(this->clearColor);

		std::vector<std::vector<Math::ivec2>> splitPixels = this->camera.generatePixelList(PixelOrder::LEFT_TO_RIGHT_TOP_TO_BOTTOM, this->numThreads);
		std::vector<std::vector<Objects::Ray>> splitRays = this->camera.generateRays(splitPixels);

		std::vector<std::thread> threads;
		threads.reserve(static_cast<std::size_t>(this->numThreads));

		std::chrono::time_point start = std::chrono::high_resolution_clock::now();
		for (int i = 0; i < this->numThreads; i++)
		{
			threads.emplace_back([=](){this->run(splitRays[i], splitPixels[i]);});
		}

		for (std::thread& thread : threads)
		{
			thread.join();
		}

		std::chrono::time_point end = std::chrono::high_resolution_clock::now();

		long totalVertices = 0;
		for (const std::shared_ptr<Objects::Object>& object : this->objects)
		{
			totalVertices += object->getNumVertices();
		}

		long totalMilliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();


		return Stats(SIMD, totalMilliseconds, totalVertices, static_cast<double>(this->rayCounter), this->camera.getScreenSize(), std::nullopt);
	}

	const Graphics::FrameBuffer& RayTracer::getFramebuffer() const noexcept
	{
		return this->frameBuffer;
	}

	Graphics::FrameBuffer& RayTracer::getFramebuffer() noexcept
	{
		return this->frameBuffer;
	}

	long RayTracer::getNumThreads() const noexcept
	{
		return this->numThreads;
	}

	void RayTracer::disableAmbient() noexcept
	{
		this->enableAmbient = false;
	}

	void RayTracer::disableDiffuse() noexcept
	{
		this->enableDiffuse = false;
	}

	void RayTracer::disableSpecular() noexcept
	{
		this->enableSpecular = false;
	}
}