/*
 * @author  thomas
 * @date    23/11/18
 * Copyright (c) 2018 thomas
 */

#ifndef RAY_TRACER_SCENE_PARSER_HPP
#define RAY_TRACER_SCENE_PARSER_HPP

#include <json/json.h>
#include <Objects/Sphere.hpp>
#include <Objects/Quad.hpp>
#include <Objects/Triangle.hpp>
#include <Objects/Mesh.hpp>
#include <RayTracer.hpp>
#include <Shaders/Material.hpp>

namespace RayTracer
{
	class SceneParser
	{
		private:
			const std::string GENERAL_HEADER;
			const std::string CAMERA_HEADER;
			const std::string GLOBAL_LIGHT_HEADER;
			const std::string POINT_LIGHT_HEADER;
			const std::string OBJECT_HEADER;

			bool isAmbientEnabled (const Json::Value& jsonGeneral) const;

			bool isDiffuseEnabled (const Json::Value& jsonGeneral) const;

			bool isSpecularEnabled (const Json::Value& jsonGeneral) const;

			long getNumThreads (const Json::Value& jsonGeneral) const;

			int getReflectionDepth (const Json::Value& jsonGeneral) const;

			Shaders::ShaderModel getShaderModel (const Json::Value& generalHeader) const;

			Camera getCamera(const Json::Value& cameraHeader) const;

			void parseGlobalLights(RayTracer& rayTracer, Json::Value& globalLightList) const;

			void parsePointLights(RayTracer& rayTracer, Json::Value& pointLightList) const;

			void parseObjectList(RayTracer& rayTracer, Json::Value& objectList);

		public:
			SceneParser();

			RayTracer parse (const std::string& scenefile);
	};
}

#endif //RAY_TRACER_SCENE_PARSER_HPP
