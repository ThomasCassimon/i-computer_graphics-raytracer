/*
 * @author thomas
 * @date   09/10/18
 * @file   RayTracer.hpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#ifndef RAY_TRACER_RAYTRACER_HPP
#define RAY_TRACER_RAYTRACER_HPP

#include <atomic>
#include <map>
#include <optional>
#include <thread>
#include <utility>
#include <vector>


#include <Graphics/FrameBuffer.hpp>
#include <Graphics/StencilBuffer.hpp>
#include <Lights/PointLight.hpp>
#include <Lights/GlobalLight.hpp>
#include <Math/vec2.hpp>
#include <Math/vec3.hpp>
#include <Math/Util.hpp>
#include <Objects/Object.hpp>
#include <Objects/Ray.hpp>
#include <Utils/Vector.hpp>
#include <Camera.hpp>
#include <Stats.hpp>
#include <PixelOrder.hpp>
#include <Shaders/ShaderModel.hpp>

namespace RayTracer
{
	class RayTracer
	{
		private:
			bool enableAmbient;
			bool enableDiffuse;
			bool enableSpecular;
			int maxReflections;
			long numThreads;

			Graphics::Color clearColor;

			std::atomic<long> rayCounter;
			Camera camera;
			Shaders::ShaderModel model;
			Graphics::StencilBuffer stencilBuffer;
			Graphics::FrameBuffer frameBuffer;

			std::vector<std::shared_ptr<Objects::Object>> objects;
			std::vector<Lights::PointLight> pointLights;
			std::vector<Lights::GlobalLight> globalLights;

			std::optional<Objects::Hit> checkRay (const Objects::Ray& ray, const Math::vec3& pixelPos, const std::vector<std::shared_ptr<Objects::Object>>& objects) noexcept;

			/**
			 * Pixels are given in <X, Y> form, where X is a screenspace position, with (0,0) in the top left and (resultSize.x, resultSize.y) in the lower right.
			 * @param pixelList
			 * @param eyePos
			 * @param screenCenter
			 * @return
			 */
			Stats run (const std::vector<Objects::Ray>& rays, const std::vector<Math::ivec2>& pixels) noexcept;

		public:
			explicit RayTracer(const Camera& camera, Shaders::ShaderModel model, int maxReflections, const Graphics::Color& clearColor, long numThreads = std::thread::hardware_concurrency()) noexcept;

			RayTracer(const RayTracer& other);

			RayTracer(RayTracer&& other) noexcept;

			void addObject(std::shared_ptr<Objects::Object> object) noexcept;   //todo: Change argument to be reference and create shared pointer in method?

			void addPointLight(Lights::PointLight light) noexcept;

			void addGlobalLight(Lights::GlobalLight light) noexcept;

			Stats run() noexcept;

			std::optional<Objects::Hit> hits(const Objects::Ray& ray, const Math::vec3& pixelPos, const std::vector<std::shared_ptr<Objects::Object>>& exclude) noexcept;

			std::vector<std::pair<std::size_t, std::optional<Objects::Hit>>> hits (const std::vector<Objects::Ray>& rays, const std::vector<Math::vec3>& pixelPos, const std::vector<std::shared_ptr<Objects::Object>>& exclude) noexcept;

			const Graphics::FrameBuffer& getFramebuffer() const noexcept;

			Graphics::FrameBuffer& getFramebuffer() noexcept;

			long getNumThreads() const noexcept;

			void disableAmbient() noexcept;

			void disableDiffuse() noexcept;

			void disableSpecular() noexcept;
	};
}

#endif //RAY_TRACER_RAYTRACER_HPP
