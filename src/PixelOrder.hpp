/*
 * @author thomas
 * @date   09/10/18
 * @file   PixelOrder.hpp
 *
 * Copyright 2018 Thomas Cassimon
 */

#ifndef RAY_TRACER_PIXELORDER_HPP
#define RAY_TRACER_PIXELORDER_HPP

namespace RayTracer
{
	enum class PixelOrder
	{
		LEFT_TO_RIGHT_TOP_TO_BOTTOM,
		TOP_TO_BOTTOM_LEFT_TO_RIGHT,
	};
}

#endif //RAY_TRACER_PIXELORDER_HPP
