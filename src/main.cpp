#include <iostream>
#include <sstream>
#include <thread>


#include <SceneParser.hpp>
#include <Stats.hpp>
#include <Utils/String.hpp>
#include <RayTracer.hpp>
#include <PostProcessing/MSAA.hpp>

std::ostream& usage(std::ostream& stream, const std::string& executable) noexcept
{
	stream << "USAGE: " << executable << " [SCENE DESCRIPTION] [OUTPUT FOLDER]";
	return stream;
}

int main(int argc, char* argv[])
{
	if (argc < 3)
	{
		if (argc < 1)
		{
			usage(std::cerr, "Unknown Executable");
		}
		else
		{
			usage(std::cerr, std::string(argv[0]));
		}
		return -1;
	}

	const std::string sceneDescription (argv[1]);
	const std::string outputDirectory (argv[2]);

	RayTracer::SceneParser sceneParser;

	RayTracer::RayTracer rayTracer = sceneParser.parse(sceneDescription);

	RayTracer::Stats stats = rayTracer.run();

	RayTracer::Graphics::FrameBuffer& frameBuffer = rayTracer.getFramebuffer();

	//RayTracer::PostProcessing::MSAA msaa;

	//frameBuffer = msaa.filter(frameBuffer);

	std::time_t timestamp = std::time(nullptr);

	const static std::string TIME_FORMAT = "%Y-%m-%d_%H%M%S";

	char timeBuffer [18];
	timeBuffer[17] = '\0';

	strftime(timeBuffer, sizeof(timeBuffer), TIME_FORMAT.c_str(), std::localtime(&timestamp));

	const std::string imgName = std::string(timeBuffer) + ".png";
	const std::string resultFilename = outputDirectory + "/" + imgName;

	frameBuffer.save(resultFilename, RayTracer::Graphics::PixelBuffer::FileType::PNG);

	std::ifstream htmlStream ("res/html/Template.html");

	std::string templateString ((   std::istreambuf_iterator<char>(htmlStream)),
									std::istreambuf_iterator<char>());

	templateString = RayTracer::Utils::String::replace(templateString, "$MODEL", sceneDescription);
	templateString = RayTracer::Utils::String::replace(templateString, "$RESULT", imgName);
	templateString = RayTracer::Utils::String::replace(templateString, "$RESOLUTION", stats.getFormattedResolution());
	templateString = RayTracer::Utils::String::replace(templateString, "$TRACE_TIME", stats.getFormattedTracetime());
	templateString = RayTracer::Utils::String::replace(templateString, "$SIMD_FLAG", stats.getFormattedSIMDFlag());
	templateString = RayTracer::Utils::String::replace(templateString, "$NUM_RAYS", stats.getFormattedTotalRays());
	templateString = RayTracer::Utils::String::replace(templateString, "$RAY_SPEED", stats.getFormattedRaySpeed());
	templateString = RayTracer::Utils::String::replace(templateString, "$NUM_VERTICES", stats.getFormattedTotalVertices());
	templateString = RayTracer::Utils::String::replace(templateString, "$VERTEX_SPEED", stats.getFormattedVertexSpeed());
	templateString = RayTracer::Utils::String::replace(templateString, "$PIXEL_SPEED", stats.getFormattedPixelSpeed());
	templateString = RayTracer::Utils::String::replace(templateString, "$NUM_THREADS", std::to_string(rayTracer.getNumThreads()));

	std::ofstream outputstream (outputDirectory + "/" + std::string(timeBuffer) + ".html");

	outputstream << templateString;

	return 0;
}